"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactTyped = _interopRequireDefault(require("react-typed"));

var _logoIzee = _interopRequireDefault(require("static/media/logo-izee.png"));

var _parsePropsAsBulmaClasses = _interopRequireDefault(require("../lib/_utils/parsePropsAsBulmaClasses"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Assistant = function Assistant(props) {
  var assistantCSSClass = "assistant ".concat((0, _parsePropsAsBulmaClasses.default)(props));
  var bubbleCSSClass = 'assistant-speech-bubble is-bottom';
  return _react.default.createElement("div", {
    className: assistantCSSClass,
    style: props.style
  }, _react.default.createElement("div", {
    className: "assistant-icon"
  }, _react.default.createElement("img", {
    src: _logoIzee.default,
    alt: props.words
  })), _react.default.createElement("div", {
    className: bubbleCSSClass
  }, _react.default.createElement(_reactTyped.default, {
    strings: [props.words],
    typeSpeed: 40
  })));
};

Assistant.propTypes = {
  words: _propTypes.default.string.isRequired
};
var _default = Assistant;
exports.default = _default;