"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _Button = _interopRequireDefault(require("./lib/Button"));

var _Space = _interopRequireDefault(require("./lib/Space"));

var _Field = _interopRequireDefault(require("./lib/FormFields/components/Field"));

var _Control = _interopRequireDefault(require("./lib/FormFields/components/Control"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var SubmitButton = function SubmitButton(props) {
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_Space.default, null), _react.default.createElement(_Field.default, {
    isRight: true
  }, _react.default.createElement(_Control.default, null, _react.default.createElement(_Button.default, _extends({}, props, {
    hasThemeColor: !props.isDanger,
    isLoading: props.isLoading,
    isDisabled: props.isDisabled,
    submit: true
  }), props.label))));
};

var _default = SubmitButton;
exports.default = _default;