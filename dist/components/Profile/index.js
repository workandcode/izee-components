"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.disabledIf = void 0;

var _react = _interopRequireDefault(require("react"));

var _Constants = _interopRequireDefault(require("../../services/Constants"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var disabledIf = function disabledIf(profile) {
  var hasPermission = true;
  if (!profile) return true;

  if (_Constants.default.get('profiles')) {
    var profiles = _Constants.default.get('profiles').filter(function (item) {
      return profile.indexOf(item.id) !== -1;
    });

    if (profiles.length > 0) hasPermission = false;
  }

  if (!hasPermission) return false;
  return true;
};

exports.disabledIf = disabledIf;

var Profile = function Profile(props) {
  var hasPermission = true;
  if (!props.disabledIf) return props.children;

  if (_Constants.default.get('profiles')) {
    var profiles = _Constants.default.get('profiles').filter(function (item) {
      return props.disabledIf.indexOf(item.id) !== -1;
    });

    if (profiles.length > 0) hasPermission = false;
  }

  if (!hasPermission) return null;
  return props.children;
};

Profile.propTypes = {};
var _default = Profile;
exports.default = _default;