"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _Button = _interopRequireDefault(require("../lib/Button"));

var _radium = _interopRequireDefault(require("radium"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var styles = {
  imageContainer: {
    width: '100%'
  },
  canvas: {
    display: 'block',
    margin: 'auto'
  }
};

var FilePreview =
/*#__PURE__*/
function (_React$Component) {
  _inherits(FilePreview, _React$Component);

  function FilePreview(props) {
    var _this;

    _classCallCheck(this, FilePreview);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(FilePreview).call(this, props));
    _this.state = {
      scale: 1,
      rotate: 0
    };
    _this.container = _react.default.createRef();
    return _this;
  }

  _createClass(FilePreview, [{
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      _toConsumableArray(document.querySelectorAll('body img.modal-img')).forEach(function (img) {
        img.remove();
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          source = _this$props.source,
          ext = _this$props.ext;
      var image = document.createElement('img');
      image.setAttribute('style', 'width: 100%, max-width: 580px; height: auto;');
      image.setAttribute('src', source);
      image.setAttribute('class', 'modal-img');
      image.setAttribute('width', '580');
      document.body.appendChild(image);

      image.onload = function () {
        if (_this2.container.current) {
          var imgCanvas = _this2.container.current;
          var imgContext = imgCanvas.getContext('2d');
          var size = {};
          var _scale = _this2.state.scale;
          if (_this2.state.rotate === 0) size = {
            w: image.width * _scale,
            h: image.height * _scale
          };
          if (_this2.state.rotate === 90) size = {
            w: image.height * _scale,
            h: image.width * _scale
          };
          if (_this2.state.rotate === 180) size = {
            w: image.width * _scale,
            h: image.height * _scale
          };
          if (_this2.state.rotate === 270) size = {
            w: image.height * _scale,
            h: image.width * _scale
          };
          imgCanvas.width = size.w;
          imgCanvas.height = size.h;
          imgContext.translate(size.w / 2, size.h / 2);
          imgContext.rotate(_this2.state.rotate * Math.PI / 180);
          imgContext.scale(_scale, _scale); // somar tudo pela escala

          imgContext.drawImage(image, -(image.width / 2), -(image.height / 2), image.width, image.height);
          imgContext.restore();
        }
      };

      var _this$state = this.state,
          scale = _this$state.scale,
          rotate = _this$state.rotate;
      return ext === 'pdf' ? _react.default.createElement("iframe", {
        title: "Document",
        width: "100%",
        height: "600",
        src: "".concat(source)
      }) : _react.default.createElement(_react.default.Fragment, null, _react.default.createElement("div", {
        style: styles.imageContainer
      }, _react.default.createElement("canvas", {
        ref: this.container,
        style: styles.canvas
      })), _react.default.createElement("div", {
        className: "buttons",
        style: {
          justifyContent: 'center',
          marginTop: 15
        }
      }, _react.default.createElement(_Button.default, {
        isFlat: true,
        hasThemeColor: true,
        onClick: this.rotateFile.bind(this)
      }, "Girar"), _react.default.createElement(_Button.default, {
        isFlat: true,
        hasThemeColor: true,
        onClick: this.scaleFile.bind(this)
      }, scale === 1 ? '2X' : '1X')));
    }
  }, {
    key: "rotateFile",
    value: function rotateFile() {
      var degree = 0;
      if (this.state.rotate === 0) degree = this.state.rotate + 90;
      if (this.state.rotate === 90) degree = this.state.rotate + 90;
      if (this.state.rotate === 180) degree = this.state.rotate + 90;
      if (this.state.rotate === 270) degree = 0;
      this.setState({
        rotate: degree
      });
    }
  }, {
    key: "scaleFile",
    value: function scaleFile() {
      var scale = 2;
      if (this.state.scale === 2) scale = 1;
      this.setState({
        scale: scale
      });
    }
  }]);

  return FilePreview;
}(_react.default.Component);

FilePreview.propTypes = {};

var _default = (0, _radium.default)(FilePreview);

exports.default = _default;