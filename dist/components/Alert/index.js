"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _parsePropsAsBulmaClasses = _interopRequireDefault(require("../lib/_utils/parsePropsAsBulmaClasses"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Alert = function Alert(props) {
  var notificationCSSClass = "notification ".concat((0, _parsePropsAsBulmaClasses.default)(props));
  return _react.default.createElement("div", {
    className: notificationCSSClass
  }, _react.default.createElement("button", {
    className: "delete",
    onClick: props.onClose
  }), props.children);
};

Alert.propTypes = {};
var _default = Alert;
exports.default = _default;