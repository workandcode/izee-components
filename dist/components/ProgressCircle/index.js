"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ProcessCircle = function ProcessCircle(props) {
  var total = 0;
  props.items.forEach(function (item) {
    return total += item.count;
  });
  var items = (props.items || []).map(function (item, index) {
    var percent = item.count * 100 / total;
    return _react.default.createElement("div", {
      className: "progress-component",
      onClick: props.onClick.bind(null, item),
      onKeyDown: props.onClick.bind(null, item),
      style: {
        cursor: 'pointer'
      },
      tabIndex: "0",
      role: "button",
      key: index
    }, _react.default.createElement("div", {
      id: "progress-circle-1",
      className: "progress-circle"
    }, _react.default.createElement("svg", {
      viewBox: "0 0 100 100",
      className: "progress-circle-bar"
    }, _react.default.createElement("path", {
      className: "progress-circle-bg",
      d: "M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94",
      strokeWidth: "3",
      fillOpacity: "0"
    }), _react.default.createElement("path", {
      className: "progress-circle-fill",
      d: "M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94",
      strokeWidth: "3",
      fillOpacity: "0",
      style: {
        strokeDasharray: '295.416, 295.416',
        strokeDashoffset: 295.416 - percent * 295.416 / 100
      }
    })), _react.default.createElement("div", {
      className: "progress-circle-content"
    }, _react.default.createElement("div", {
      className: "progress-circle-info"
    }, _react.default.createElement("h2", {
      className: "progress-circle-heading"
    }, item.count)))), _react.default.createElement("h3", {
      className: "progress-circle-title"
    }, item.name || '-', " - ", _react.default.createElement("span", {
      className: "progress-circle-percentual"
    }, item.perc, "%")));
  });
  return _react.default.createElement("div", {
    className: "progress-container"
  }, items);
};

ProcessCircle.defaultProps = {
  onClick: function onClick() {}
};
ProcessCircle.propTypes = {
  items: _propTypes.default.array.isRequired,
  onClick: _propTypes.default.func
};
var _default = ProcessCircle;
exports.default = _default;