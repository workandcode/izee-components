"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _FlexBar = _interopRequireDefault(require("../lib/FlexBar"));

var _Header = _interopRequireDefault(require("../lib/Header"));

var _Button = _interopRequireDefault(require("../lib/Button"));

var _Icon = _interopRequireDefault(require("../lib/Icon"));

var _this = void 0;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ScreenHeader = function ScreenHeader(props) {
  return _react.default.createElement(_FlexBar.default, {
    isSecondary: true,
    hasNeutralBackground: true,
    hasShadow: true
  }, _react.default.createElement(_FlexBar.default.Child, {
    isGrow: true
  }, !props.hideHomeButton ? _react.default.createElement(_Button.default, {
    isFlat: true,
    isBare: true,
    style: {
      marginLeft: 1,
      marginRight: -5
    },
    onClick: props.onClick.bind(_this, 'back')
  }, _react.default.createElement(_Icon.default, {
    name: "far fa-chevron-left"
  })) : null, _react.default.createElement(_Header.default, {
    isUppercase: true
  }, props.header)), props.children ? _react.default.createElement(_FlexBar.default.Child, null, props.children) : null);
};

ScreenHeader.defaultProps = {
  onClick: function onClick() {}
};
ScreenHeader.propTypes = {
  onClick: _propTypes.default.func
};
var _default = ScreenHeader;
exports.default = _default;