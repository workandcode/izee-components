"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _Button = _interopRequireDefault(require("../lib/Button"));

var _Icon = _interopRequireDefault(require("../lib/Icon"));

var _Tooltip = _interopRequireDefault(require("../lib/Tooltip"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var UserName = function UserName(props) {
  return _react.default.createElement("div", null, props.content.name ? _react.default.createElement(_react.default.Fragment, null, _react.default.createElement("strong", null, props.content.name), props.content.is_verified ? _react.default.createElement(_Tooltip.default, {
    description: _react.default.createElement("span", null, "Locat\xE1rio", _react.default.createElement("br", null), "pr\xE9-aprovado", _react.default.createElement("br", null), "pela Izee")
  }, _react.default.createElement(_Icon.default, {
    name: "fas fa-sm fa-shield-check",
    isSuccess: true
  })) : null, _react.default.createElement("br", null)) : null, _react.default.createElement("span", {
    className: "text has-text-grey"
  }, props.content.email));
};

UserName.defaultProps = {
  content: {}
};
var _default = UserName;
exports.default = _default;