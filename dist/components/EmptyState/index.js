"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _Button = _interopRequireDefault(require("../lib/Button"));

var _Icon = _interopRequireDefault(require("../lib/Icon"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var EmptyState = function EmptyState(props) {
  return _react.default.createElement("div", {
    className: "empty-state"
  }, props.isLoading ? _react.default.createElement("div", {
    className: "empty-state-spinner"
  }) : _react.default.createElement("div", {
    className: "empty-state-icon"
  }, _react.default.createElement(_Icon.default, {
    name: "fas fa-inbox fa-4x"
  })));
};

EmptyState.propTypes = {};
var _default = EmptyState;
exports.default = _default;