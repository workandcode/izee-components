"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Step = exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _swiper = _interopRequireDefault(require("swiper"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Onboarding =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Onboarding, _React$Component);

  function Onboarding(props) {
    var _this;

    _classCallCheck(this, Onboarding);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Onboarding).call(this, props));
    _this.state = {
      isOpen: false
    };
    _this.swiper = null;
    return _this;
  }

  _createClass(Onboarding, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      if (this.props.isOpen === undefined || this.props.isOpen === true) {
        this.buildSwiper().then(function (instance) {
          _this2.swiper = instance;

          _this2.forceUpdate();
        });
      }
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      if (this.swiper) this.swiper.update();
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      if (this.swiper) this.swiper.destroy(); // delete this.swiper;
    }
  }, {
    key: "render",
    value: function render() {
      var _this3 = this;

      var slides = _react.default.Children.toArray(this.props.children).map(function (slide, index) {
        var _slide$props = slide.props,
            children = _slide$props.children,
            template = _slide$props.template,
            isHidden = _slide$props.isHidden;
        if (isHidden || !_this3.swiper) return null;
        return _react.default.createElement("div", {
          className: "swiper-slide",
          key: slide.key
        }, _react.default.createElement("div", {
          className: "onboarding-step-content"
        }, template ? template({
          isActive: _this3.swiper.activeIndex === index,
          index: index,
          next: _this3.swiper.slideNext.bind(_this3.swiper),
          prev: _this3.swiper.slidePrev.bind(_this3.swiper),
          goto: _this3.swiper.slideTo.bind(_this3.swiper)
        }) : children));
      }); // const isContainerHidden = !this.state.isOpen;


      var isContainerHidden = this.state.isOpen !== undefined && this.state.isOpen !== true;
      if (isContainerHidden) slides = null;
      var onboardingCSSClass = "onboarding".concat(!isContainerHidden ? ' is-active' : '');
      return _react.default.createElement("div", {
        className: onboardingCSSClass
      }, _react.default.createElement("div", {
        className: "onboarding-container"
      }, _react.default.createElement("div", {
        className: "swiper-container"
      }, _react.default.createElement("div", {
        className: "swiper-wrapper"
      }, slides), _react.default.createElement("div", {
        className: "swiper-pagination swiper-pagination-onboarding"
      })), this.props.footer), _react.default.createElement("div", {
        className: "onboarding-background"
      }));
    }
  }, {
    key: "handleClose",
    value: function handleClose() {
      this.setState({
        isOpen: false
      });
      if (this.props.onClose) this.props.onClose();
    }
  }, {
    key: "buildSwiper",
    value: function buildSwiper() {
      return new Promise(function (resolve) {
        var swiper = new _swiper.default('.swiper-container', {
          preventInteractionOnTransition: true,
          shortSwipes: false,
          longSwipes: false,
          simulateTouch: false,
          allowTouchMove: false,
          pagination: {
            el: '.swiper-pagination'
          }
        });
        resolve(swiper);
      });
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(props, state) {
      if (state.isOpen !== props.isOpen) {
        return {
          isOpen: props.isOpen
        };
      }

      return null;
    }
  }]);

  return Onboarding;
}(_react.default.Component);

exports.default = Onboarding;
;
Onboarding.defaultProps = {
  onClose: function onClose() {}
};
Onboarding.propTypes = {
  onClose: _propTypes.default.func
};

var Step = function Step(props) {
  return props.children;
};

exports.Step = Step;
Step.displayName = 'Onboarding(Step)';
Onboarding.propTypes = {
  children: _propTypes.default.arrayOf(function (propValue, key, componentName, location, propFullName) {
    if (propValue[key].type.displayName !== Step.displayName) {
      return new Error('Invalid prop `' + propFullName + '` supplied to' + ' `' + componentName + '`. Validation failed.');
    }
  }).isRequired
};