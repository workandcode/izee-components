"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _Button = _interopRequireDefault(require("../lib/Button"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ViewSelector = function ViewSelector(props) {
  var views = props.views.map(function (item, index) {
    var isActive = true;
    if (item.isHidden) return null;
    if (item.isActive !== null && item.isActive !== undefined) isActive = item.isActive;
    return _react.default.createElement(_Button.default, {
      isTab: true,
      isDisabled: !isActive,
      isSelected: props.current === item.code,
      onClick: item.code && props.onClick.bind(null, item.code),
      key: item.code || index + 1,
      role: item.code ? item.code : null
    }, item.name);
  });
  return _react.default.createElement("div", {
    className: "buttons has-addons"
  }, views);
};

ViewSelector.propTypes = {};
var _default = ViewSelector;
exports.default = _default;