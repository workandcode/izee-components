"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var InlineBars = function InlineBars(props) {
  var colors = ['#D678F7', '#7CEC87', '#F3AC76', '#63ADF5', '#BCBFC5', '#D678F7', '#7CEC87', '#F3AC76'];
  var total = 0;
  var length = props.items.length;
  props.items.forEach(function (item) {
    return total += item.value;
  });
  var items = (props.items || []).map(function (item, index) {
    var percent = item.value * 100 / total;
    return _react.default.createElement("div", {
      className: "inline-bars-item",
      style: {
        width: percent + '%'
      },
      key: index
    }, _react.default.createElement("div", {
      className: "inline-bars-content"
    }, _react.default.createElement("div", {
      className: "inline-bars-title"
    }, item.name || '-'), _react.default.createElement("div", {
      className: "inline-bars-value",
      title: "".concat(Math.round(percent), "% (").concat(item.value, ")")
    }, "".concat(Math.round(percent), "% (").concat(item.value, ")"))), _react.default.createElement("div", {
      className: "inline-bars-progress",
      style: {
        backgroundColor: colors[index]
      }
    }));
  });
  return _react.default.createElement("div", {
    className: "inline-bars"
  }, items);
};

InlineBars.propTypes = {};
var _default = InlineBars;
exports.default = _default;