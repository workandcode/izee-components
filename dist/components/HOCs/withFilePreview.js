"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = withFilePreview;

var _react = _interopRequireDefault(require("react"));

var _constants = require("core/constants");

var _reactTimeout = _interopRequireDefault(require("react-timeout"));

var _withModal = _interopRequireDefault(require("./withModal"));

var _Text = _interopRequireDefault(require("../lib/Text"));

var _FilePreview = _interopRequireDefault(require("../FilePreview"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function withFilePreview(Component, path) {
  var WithFilePreviewWrapper =
  /*#__PURE__*/
  function (_React$Component) {
    _inherits(WithFilePreviewWrapper, _React$Component);

    function WithFilePreviewWrapper(props) {
      var _this;

      _classCallCheck(this, WithFilePreviewWrapper);

      _this = _possibleConstructorReturn(this, _getPrototypeOf(WithFilePreviewWrapper).call(this, props));
      _this.state = {};
      _this.modalTitle = 'Visualizar arquivo';
      return _this;
    }

    _createClass(WithFilePreviewWrapper, [{
      key: "render",
      value: function render() {
        return _react.default.createElement(Component, _extends({}, this.props, {
          openFile: this.fetchFile.bind(this)
        }));
      }
    }, {
      key: "fetchFile",
      value: function fetchFile(file, requestPath) {
        var _this2 = this;

        this.props.openModal(this.modalTitle, _react.default.createElement(_Text.default, null, "Carregando..."));
        var path = typeof requestPath === 'string' ? requestPath : '{company}/file';

        _constants.request.get("".concat(path, "/").concat(file.id)).then(function (response) {
          _this2.props.openModal(_this2.modalTitle, _react.default.createElement(_FilePreview.default, {
            source: "".concat(response.data.source),
            ext: response.data.type
          }));
        }).catch(function () {
          return _this2.props.openModal(_this2.modalTitle, _react.default.createElement(_Text.default, null, "Ocorreu um erro :("));
        });
      }
    }]);

    return WithFilePreviewWrapper;
  }(_react.default.Component);

  ;
  WithFilePreviewWrapper.displayName = "WithFilePreviewWrapper(".concat(getDisplayName(Component), ")");
  return (0, _reactTimeout.default)((0, _withModal.default)(WithFilePreviewWrapper));
}

;

function getDisplayName(Component) {
  return Component.displayName || Component.name || 'Component';
}

;