"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = withFilter;

var _react = _interopRequireDefault(require("react"));

var _reactTimeout = _interopRequireDefault(require("react-timeout"));

var _throttleDebounce = require("throttle-debounce");

var _Button = _interopRequireDefault(require("../lib/Button"));

var _Icon = _interopRequireDefault(require("../lib/Icon"));

var _Form = _interopRequireDefault(require("../lib/Form"));

var _Grid = _interopRequireDefault(require("../lib/Grid"));

var _Space = _interopRequireDefault(require("../lib/Space"));

var _FormFields = require("../lib/FormFields");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function withFilter(Component) {
  var WithFilterWrapper =
  /*#__PURE__*/
  function (_React$Component) {
    _inherits(WithFilterWrapper, _React$Component);

    function WithFilterWrapper(props) {
      var _this;

      _classCallCheck(this, WithFilterWrapper);

      _this = _possibleConstructorReturn(this, _getPrototypeOf(WithFilterWrapper).call(this, props));
      _this.state = {
        value: ''
      };
      return _this;
    }

    _createClass(WithFilterWrapper, [{
      key: "render",
      value: function render() {
        var _this2 = this;

        return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_Form.default, {
          getRef: function getRef(ref) {
            return _this2.form = ref;
          },
          onSubmit: this.submitForm.bind(this),
          autoFocus: true
        }, _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, {
          placeholder: "Pesquisar",
          value: this.state.value,
          onChange: (0, _throttleDebounce.debounce)(200, this.handleFieldChanges.bind(this)),
          noValidate: true
        })), _react.default.createElement(_Grid.default.Col, {
          isNarrow: true
        }, _react.default.createElement(_Button.default, {
          hasThemeColor: true,
          submit: true
        }, _react.default.createElement(_Icon.default, {
          name: "fas fa-search"
        }))))), _react.default.createElement(_Space.default, {
          isSmall: true
        }), _react.default.createElement(Component, _extends({}, this.props, {
          createFilterRequest: this.requestSearch.bind(this)
        })));
      }
    }, {
      key: "requestSearch",
      value: function requestSearch(callback) {
        this.request = callback;
      }
    }, {
      key: "handleFieldChanges",
      value: function handleFieldChanges(value, field) {
        if (!field.wasFocused) return;
        this.request(value);
        this.setState({
          value: value
        });
      }
    }, {
      key: "submitForm",
      value: function submitForm() {
        this.request(this.state.value);
      }
    }]);

    return WithFilterWrapper;
  }(_react.default.Component);

  ;
  WithFilterWrapper.displayName = "WithFilterWrapper(".concat(getDisplayName(Component), ")");
  return (0, _reactTimeout.default)(WithFilterWrapper);
}

;

function getDisplayName(Component) {
  return Component.displayName || Component.name || 'Component';
}

;