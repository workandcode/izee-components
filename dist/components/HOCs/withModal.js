"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = withModal;

var _react = _interopRequireDefault(require("react"));

var _reactTimeout = _interopRequireDefault(require("react-timeout"));

var _Modal = _interopRequireDefault(require("../lib/Modal"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function withModal(Component) {
  var WithModalWrapper =
  /*#__PURE__*/
  function (_React$Component) {
    _inherits(WithModalWrapper, _React$Component);

    function WithModalWrapper(props) {
      var _this;

      _classCallCheck(this, WithModalWrapper);

      _this = _possibleConstructorReturn(this, _getPrototypeOf(WithModalWrapper).call(this, props));
      _this.state = {
        modalTitle: '',
        modalIsOpen: false,
        component: null
      };
      return _this;
    }

    _createClass(WithModalWrapper, [{
      key: "render",
      value: function render() {
        return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(Component, _extends({}, this.props, {
          openModal: this.openModal.bind(this),
          closeModal: this.closeModal.bind(this)
        })), _react.default.createElement(_Modal.default, {
          title: this.state.modalTitle,
          isOpen: this.state.modalIsOpen,
          onClose: this.closeModal.bind(this)
        }, this.state.component));
      }
    }, {
      key: "openModal",
      value: function openModal(description, component) {
        var isOpen = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;
        this.setState({
          modalTitle: description,
          modalIsOpen: isOpen,
          component: component
        });
      }
    }, {
      key: "closeModal",
      value: function closeModal() {
        this.setState({
          modalTitle: null,
          modalIsOpen: false,
          component: null
        });
      }
    }]);

    return WithModalWrapper;
  }(_react.default.Component);

  ;
  WithModalWrapper.displayName = "WithModalWrapper(".concat(getDisplayName(Component), ")");
  return (0, _reactTimeout.default)(WithModalWrapper);
}

;

function getDisplayName(Component) {
  return Component.displayName || Component.name || 'Component';
}

;