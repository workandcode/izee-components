"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _Grid = _interopRequireDefault(require("../lib/Grid"));

var _Text = _interopRequireDefault(require("../lib/Text"));

var _Space = _interopRequireDefault(require("../lib/Space"));

var _Address = _interopRequireDefault(require("./components/Address"));

var _PropertyInfo = _interopRequireDefault(require("./components/PropertyInfo"));

var _PersonalData = _interopRequireDefault(require("./components/PersonalData"));

var _PersonalDataSpouse = _interopRequireDefault(require("./components/PersonalDataSpouse"));

var _CompanyData = _interopRequireDefault(require("./components/CompanyData"));

var _Evaluation = _interopRequireDefault(require("./components/Evaluation"));

var _Documents = _interopRequireDefault(require("./components/Documents"));

var _Negotiation = _interopRequireDefault(require("./components/Negotiation"));

var _UserName = _interopRequireDefault(require("./components/UserName"));

var _Section = _interopRequireDefault(require("./components/Section"));

var _utils = require("./components/utils");

var _withFilePreview = _interopRequireDefault(require("../HOCs/withFilePreview"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Proposal =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Proposal, _React$Component);

  function Proposal() {
    _classCallCheck(this, Proposal);

    return _possibleConstructorReturn(this, _getPrototypeOf(Proposal).apply(this, arguments));
  }

  _createClass(Proposal, [{
    key: "getExpirationDateTitle",
    value: function getExpirationDateTitle(item) {
      if (!item || !item.operation_tokens[0]) return null;
      return "Data de Expira\xE7\xE3o: ".concat(item.operation_tokens[0].expiration_date);
    }
  }, {
    key: "render",
    value: function render() {
      var _this = this;

      var current = this.props.data;
      if (!current) return null;
      if (!current.properties) return null;
      var requestPath = this.props.requestPath;
      var hasClick = typeof this.props.onClickResend === 'function' && typeof this.props.onClickCopy === 'function';
      var rentValue = (0, _utils.currency)(current['rent_value']);
      var ownershipDate = (0, _utils.date)(current['ownership_date']);
      var negotiation = current['negotiation'];
      var guarantee = current['guarantee'];
      var guaranteeName = guarantee ? guarantee.name : 'A garantia ainda não foi escolhida';
      var property = current.properties[0];
      var owners = current['property_owners'].map(function (item, index) {
        var shoResend = hasClick && item.evaluation != 2;
        return _react.default.createElement(_react.default.Fragment, {
          key: item.id
        }, _react.default.createElement("div", {
          style: {
            borderTop: index > 0 && '1px solid #f2f2f2',
            margin: index > 0 && '2px 0 15px'
          }
        }), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_UserName.default, _extends({}, item, {
          onClickResend: shoResend && _this.props.onClickResend.bind(null, 'property_owner_ids', item),
          onClickCopy: shoResend && _this.props.onClickCopy.bind(_this, item),
          title: _this.getExpirationDateTitle(item)
        })), item.cnpj && item.cnpj !== "" ? _react.default.createElement("div", null, _react.default.createElement(_PersonalData.default, _extends({}, item, {
          brief: true
        })), item.spouse_name && item.spouse_name !== "" && _react.default.createElement("div", null, _react.default.createElement(_PersonalDataSpouse.default, _extends({}, item, {
          brief: true
        }))), _react.default.createElement(_Space.default, {
          isSmall: true
        }), _react.default.createElement(_Text.default, null, _react.default.createElement("strong", null, _react.default.createElement("span", {
          style: {
            fontSize: 14
          }
        }, item.trading_name))), _react.default.createElement(_CompanyData.default, _extends({}, item, {
          brief: true
        }))) : _react.default.createElement("div", null, _react.default.createElement(_PersonalData.default, _extends({}, item, {
          brief: true
        })), item.spouse_name && item.spouse_name !== "" && _react.default.createElement("div", null, _react.default.createElement(_PersonalDataSpouse.default, _extends({}, item, {
          brief: true
        }))))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_Address.default, {
          address: item.address
        }))), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_Evaluation.default, {
          status: item.evaluation,
          comment: item.denial_comment
        }))));
      });
      var renters = current.renters.map(function (item, index) {
        return _react.default.createElement(_react.default.Fragment, {
          key: item.id
        }, _react.default.createElement("div", {
          style: {
            borderTop: index > 0 && '1px solid #f2f2f2',
            margin: index > 0 && '2px 0 15px'
          }
        }), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_UserName.default, _extends({}, item, {
          onClickResend: hasClick && _this.props.onClickResend.bind(null, 'renter_ids', item),
          onClickCopy: hasClick && _this.props.onClickCopy.bind(_this, item),
          title: _this.getExpirationDateTitle(item)
        })), item.cnpj && item.cnpj !== "" ? _react.default.createElement("div", null, _react.default.createElement(_PersonalData.default, _extends({}, item, {
          brief: true
        })), item.spouse_name && item.spouse_name !== "" && _react.default.createElement("div", null, _react.default.createElement(_PersonalDataSpouse.default, _extends({}, item, {
          brief: true
        }))), _react.default.createElement(_Space.default, {
          isSmall: true
        }), _react.default.createElement(_Text.default, null, _react.default.createElement("strong", null, _react.default.createElement("span", {
          style: {
            fontSize: 14
          }
        }, item.trading_name))), _react.default.createElement(_CompanyData.default, _extends({}, item, {
          brief: true
        }))) : _react.default.createElement("div", null, _react.default.createElement(_PersonalData.default, _extends({}, item, {
          brief: true
        })), item.spouse_name && item.spouse_name !== "" && _react.default.createElement("div", null, _react.default.createElement(_PersonalDataSpouse.default, _extends({}, item, {
          brief: true
        }))))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_Address.default, {
          address: item.address
        }))), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_Documents.default, {
          requestPath: requestPath,
          list: item.documents,
          onClickFile: _this.props.openFile
        }), item.main && _react.default.createElement(_Evaluation.default, {
          status: item.evaluation,
          comment: item.denial_comment
        }))));
      });
      var guarantors = (current.guarantors || []).map(function (item, index) {
        return _react.default.createElement(_react.default.Fragment, {
          key: item.id
        }, _react.default.createElement("div", {
          style: {
            borderTop: index > 0 && '1px solid #f2f2f2',
            margin: index > 0 && '2px 0 15px'
          }
        }), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_UserName.default, _extends({}, item, {
          onClickResend: hasClick && _this.props.onClickResend.bind(null, 'renter_ids', item),
          onClickCopy: hasClick && _this.props.onClickCopy.bind(_this, item),
          title: _this.getExpirationDateTitle(item)
        })), item.cnpj && item.cnpj !== "" ? _react.default.createElement("div", null, _react.default.createElement(_PersonalData.default, _extends({}, item, {
          brief: true
        })), item.spouse_name && item.spouse_name !== "" && _react.default.createElement("div", null, _react.default.createElement(_PersonalDataSpouse.default, _extends({}, item, {
          brief: true
        }))), _react.default.createElement(_Space.default, {
          isSmall: true
        }), _react.default.createElement(_Text.default, null, _react.default.createElement("strong", null, _react.default.createElement("span", {
          style: {
            fontSize: 14
          }
        }, item.trading_name))), _react.default.createElement(_CompanyData.default, _extends({}, item, {
          brief: true
        }))) : _react.default.createElement("div", null, _react.default.createElement(_PersonalData.default, _extends({}, item, {
          brief: true
        })), item.spouse_name && item.spouse_name !== "" && _react.default.createElement("div", null, _react.default.createElement(_PersonalDataSpouse.default, _extends({}, item, {
          brief: true
        }))))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_Address.default, {
          address: item.address
        }))), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_Documents.default, {
          requestPath: requestPath,
          list: item.documents,
          onClickFile: _this.props.openFile
        }), _react.default.createElement(_Evaluation.default, {
          status: item.evaluation,
          comment: item.denial_comment
        }))));
      });
      return _react.default.createElement("div", {
        ref: this.props.ref
      }, _react.default.createElement(_Section.default, {
        title: "Im\xF3vel ".concat(property.type ? "(".concat(property.type.name, ")") : '').concat(property.name ? " // Apelido: ".concat(property.name) : ''),
        iconName: "home"
      }, _react.default.createElement(_Address.default, {
        address: property.address
      })), _react.default.createElement(_Section.default, {
        title: "Propriet\xE1rios",
        iconName: "user"
      }, owners), _react.default.createElement(_Section.default, {
        title: "Locat\xE1rios",
        iconName: "user"
      }, renters), _react.default.createElement(_Section.default, {
        title: 'Garantia ' + (guaranteeName && "(".concat(guaranteeName, ")")),
        iconName: "shield-check"
      }, guarantors.length ? guarantors : null), _react.default.createElement(_Section.default, {
        title: "Valor do aluguel: R$ ".concat(rentValue),
        iconName: "file-invoice-dollar"
      }, _react.default.createElement(_PropertyInfo.default, {
        property: property
      })), ownershipDate && _react.default.createElement(_Section.default, {
        title: "Data da posse: ".concat(ownershipDate),
        iconName: "calendar-alt"
      }), _react.default.createElement(_Section.default, {
        title: "Negocia\xE7\xE3o",
        iconName: "asterisk"
      }, _react.default.createElement(_Negotiation.default, null, negotiation)));
    }
  }]);

  return Proposal;
}(_react.default.Component);

;

var _default = (0, _withFilePreview.default)(Proposal);

exports.default = _default;