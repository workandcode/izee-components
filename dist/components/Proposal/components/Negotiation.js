"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _Text = _interopRequireDefault(require("../../lib/Text"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Negotiation = function Negotiation(_ref) {
  var children = _ref.children;
  return _react.default.createElement(_Text.default, null, _react.default.createElement("span", {
    style: {
      whiteSpace: 'pre-line'
    }
  }, children));
};

var _default = Negotiation;
exports.default = _default;