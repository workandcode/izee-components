"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _Icon = _interopRequireDefault(require("../../lib/Icon"));

var _Button = _interopRequireDefault(require("../../lib/Button"));

var _Text = _interopRequireDefault(require("../../lib/Text"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Documents = function Documents(_ref) {
  var list = _ref.list,
      onClickFile = _ref.onClickFile,
      requestPath = _ref.requestPath;
  var documents = null;
  documents = list.map(function (file, index) {
    return _react.default.createElement(_react.default.Fragment, {
      key: file.id
    }, _react.default.createElement(_Button.default, {
      isLink: true,
      onClick: onClickFile.bind(null, file, requestPath)
    }, file.name), index + 1 < list.length ? ', ' : '');
  });
  return _react.default.createElement("div", {
    style: {
      marginLeft: -4,
      marginTop: -8
    }
  }, _react.default.createElement(_Text.default, null, _react.default.createElement(_Icon.default, {
    name: "fas fa-sm fa-id-card"
  }), "Documentos:\xA0", _react.default.createElement("span", {
    style: {
      bottom: 0,
      position: 'relative'
    }
  }, documents.length ? documents : 'Nenhum documento foi enviado.')));
};

var _default = Documents;
exports.default = _default;