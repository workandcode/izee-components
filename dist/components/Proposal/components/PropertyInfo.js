"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _Text = _interopRequireDefault(require("../../lib/Text"));

var _utils = require("./utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PropertyInfo = function PropertyInfo(_ref) {
  var property = _ref.property;
  if (!property) return null;
  var condoFee = (0, _utils.currency)(property.condoFee);
  var iptu = (0, _utils.currency)(property.iptu);
  return _react.default.createElement("div", null, condoFee && _react.default.createElement(_Text.default, null, "Condom\xEDnio (Mensal aproximado): R$ ", condoFee), iptu && _react.default.createElement(_Text.default, null, "IPTU (Aproximado): R$ ", iptu), property.parking_spaces && property.parking_spaces.length ? _react.default.createElement(_Text.default, null, "Vagas de garagem: ", property.parking_spaces.join(', ')) : null, property.hobby_boxes && property.hobby_boxes.length ? _react.default.createElement(_Text.default, null, "Hobby Box: ", property.hobby_boxes.join(', ')) : null);
};

var _default = PropertyInfo;
exports.default = _default;