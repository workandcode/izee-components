"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _Text = _interopRequireDefault(require("../../lib/Text"));

var _utils = require("./utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PersonalData = function PersonalData(props) {
  var income = (0, _utils.currency)(props['income']);
  var estimatedRentValue = (0, _utils.currency)(props['estimated_rent_value']);
  var birthDate = (0, _utils.date)(props['birth_date']);
  var expeditionDate = (0, _utils.date)(props['rg_expedition_date']);
  var brief = props.brief;
  return _react.default.createElement(_react.default.Fragment, null, props.is_student && !brief && _react.default.createElement("div", {
    style: {
      marginBottom: 5
    }
  }, _react.default.createElement("span", {
    className: "tag is-warning is-small"
  }, "Estudante")), _react.default.createElement(_Text.default, null, "Email: ", props.email, " / Telefone: ", props.phone), _react.default.createElement(_Text.default, null, "Documento: ", props.identity_type, " / N\xFAmero: ", props.identity), _react.default.createElement(_Text.default, null, "CPF: ", props['cpf']), !brief && _react.default.createElement(_Text.default, null, "Data de nascimento: ", birthDate), !brief && income && _react.default.createElement(_Text.default, null, "Renda: R$ ", income), !brief && estimatedRentValue && _react.default.createElement(_Text.default, null, "Valor estimado do aluguel: R$ ", estimatedRentValue), !brief && _react.default.createElement(_Text.default, null, "Estado civil: ", (props['marital_status'] || {}).name), !brief && _react.default.createElement(_Text.default, null, "Ocupa\xE7\xE3o: ", props['occupation']), !brief && props['nationality'] && _react.default.createElement(_Text.default, null, "Nacionalidade: ", props['nationality']));
};

var _default = PersonalData;
exports.default = _default;