"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _Text = _interopRequireDefault(require("../../lib/Text"));

var _utils = require("./utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Address = function Address(_ref) {
  var address = _ref.address,
      inline = _ref.inline;
  if (!address) return null;

  if (inline) {
    return _react.default.createElement("div", null, _react.default.createElement(_Text.default, null, _react.default.createElement("strong", null, "Endere\xE7o:"), "\xA0", address.address, ", ", address.number, "\xA0", address.neighborhood, " - ", address.zip_code, "\xA0", address.city, "/", address.state, "\xA0 - ", address.additional_address));
  }

  return _react.default.createElement("div", null, _react.default.createElement(_Text.default, null, _react.default.createElement("strong", null, _react.default.createElement("span", {
    style: {
      fontSize: 14
    }
  }, "Endere\xE7o"))), _react.default.createElement(_Text.default, null, address.address, ", ", address.number), _react.default.createElement(_Text.default, null, address.neighborhood, " - ", address.zip_code), _react.default.createElement(_Text.default, null, address.city, " / ", address.state), _react.default.createElement(_Text.default, null, address.additional_address), address.area && _react.default.createElement(_Text.default, null, "\xC1rea: ", (0, _utils.area)(address.area), " m\xB2"));
};

var _default = Address;
exports.default = _default;