"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _Text = _interopRequireDefault(require("../../lib/Text"));

var _Icon = _interopRequireDefault(require("../../lib/Icon"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Evaluation = function Evaluation(_ref) {
  var status = _ref.status,
      comment = _ref.comment;

  var statusComponent = _react.default.createElement("span", {
    className: "is-warning"
  }, "Aguardando", _react.default.createElement(_Icon.default, {
    name: "fas fa-xs fa-hand-rock"
  }));

  if (status === 0) {
    statusComponent = _react.default.createElement("span", {
      className: "is-danger"
    }, "Recusado", _react.default.createElement(_Icon.default, {
      name: "fas fa-xs fa-thumbs-down"
    }));
  }

  if (status === 1) {
    statusComponent = _react.default.createElement("span", {
      className: "is-success"
    }, "Aceito", _react.default.createElement(_Icon.default, {
      name: "fas fa-xs fa-thumbs-up"
    }));
  }

  if (status === 2) {
    statusComponent = _react.default.createElement("span", {
      className: ""
    }, "N\xE3o participa", _react.default.createElement(_Icon.default, {
      name: "fas fa-xs fa-hand-rock"
    }));
  }

  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement("div", {
    style: {
      borderTop: '1px solid #f2f2f2',
      margin: '6px 0'
    }
  }), _react.default.createElement(_Text.default, null, _react.default.createElement("strong", null, "Situa\xE7\xE3o:"), " ", statusComponent), comment && _react.default.createElement(_Text.default, null, "Motivo: ", comment));
};

var _default = Evaluation;
exports.default = _default;