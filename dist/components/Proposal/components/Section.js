"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _Icon = _interopRequireDefault(require("../../lib/Icon"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Section = function Section(_ref) {
  var title = _ref.title,
      iconName = _ref.iconName,
      children = _ref.children;

  var icon = iconName && _react.default.createElement(_Icon.default, {
    name: "fas fa-".concat(iconName),
    style: {
      marginLeft: -4
    }
  });

  return _react.default.createElement("div", {
    className: "proposal-section",
    style: {
      pageBreakAfter: 'auto'
    }
  }, _react.default.createElement("header", {
    className: "proposal-section-title",
    style: {
      border: !children && 'none'
    }
  }, icon, " ", _react.default.createElement("strong", null, title)), _react.default.createElement("div", {
    className: "proposal-section-body"
  }, children));
};

var _default = Section;
exports.default = _default;