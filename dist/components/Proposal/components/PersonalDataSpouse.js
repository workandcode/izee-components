"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _Text = _interopRequireDefault(require("../../lib/Text"));

var _Space = _interopRequireDefault(require("../../lib/Space"));

var _utils = require("./utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PersonalDataSpouse = function PersonalDataSpouse(props) {
  var birthDate = (0, _utils.date)(props['birth_date']);
  var brief = props.brief;
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_Space.default, {
    isSmall: true
  }), _react.default.createElement(_Text.default, null, _react.default.createElement("strong", null, _react.default.createElement("span", {
    style: {
      fontSize: 14
    }
  }, props.spouse_name, " (C\xF4njuge)"))), _react.default.createElement(_Text.default, null, "Email: ", props.spouse_email, " / Telefone: ", props.spouse_phone), _react.default.createElement(_Text.default, null, "Documento: ", props.spouse_identity_type, " / N\xFAmero: ", props.spouse_identity), _react.default.createElement(_Text.default, null, "CPF: ", props['spouse_cpf']), !brief && _react.default.createElement(_Text.default, null, "Data de nascimento: ", spouse_birthDate), !brief && _react.default.createElement(_Text.default, null, "Ocupa\xE7\xE3o: ", props['spouse_occupation']), !brief && props['spouse_nationality'] && _react.default.createElement(_Text.default, null, "Nacionalidade: ", props['nationality']));
};

var _default = PersonalDataSpouse;
exports.default = _default;