"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _Text = _interopRequireDefault(require("../../lib/Text"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CompanyData = function CompanyData(props) {
  var brief = props.brief;
  return _react.default.createElement(_react.default.Fragment, null, props.is_student && !brief && _react.default.createElement("div", {
    style: {
      marginBottom: 5
    }
  }, _react.default.createElement("span", {
    className: "tag is-warning is-small"
  }, "Estudante")), _react.default.createElement(_Text.default, null, "Email: ", props.email), _react.default.createElement(_Text.default, null, "CNPJ: ", props['cnpj'], " "), !brief && _react.default.createElement(_Text.default, null, "Raz\xE3o Social: ", props['trading_name']), !brief && _react.default.createElement(_Text.default, null, "Inscri\xE7\xE3o Municipal: ", props['municipal_enrollment']), !brief && _react.default.createElement(_Text.default, null, "Inscri\xE7\xE3o Estadual: ", props['state_enrollment']));
};

var _default = CompanyData;
exports.default = _default;