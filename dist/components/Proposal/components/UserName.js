"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _Text = _interopRequireDefault(require("../../lib/Text"));

var _Button = _interopRequireDefault(require("../../lib/Button"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var ButtonWrapper =
/*#__PURE__*/
function (_React$Component) {
  _inherits(ButtonWrapper, _React$Component);

  function ButtonWrapper(props) {
    var _this;

    _classCallCheck(this, ButtonWrapper);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(ButtonWrapper).call(this, props));
    _this.state = {
      showMessage: false
    };
    return _this;
  }

  _createClass(ButtonWrapper, [{
    key: "onClick",
    value: function onClick() {
      var _this2 = this;

      if (this.props.onClick()) {
        this.setState({
          showMessage: true
        });
        setTimeout(function () {
          _this2.setState({
            showMessage: false
          });
        }, 1000);
      }
    }
  }, {
    key: "render",
    value: function render() {
      return _react.default.createElement(_Button.default, {
        isBare: true,
        onClick: this.onClick.bind(this),
        style: {
          lineHeight: 1.35
        }
      }, _react.default.createElement("span", {
        style: {
          color: '#363636',
          marginLeft: 0
        }
      }, this.props.children), "\xA0", _react.default.createElement("span", {
        title: this.props.title,
        style: {
          color: '#2b71eb',
          fontSize: 12
        }
      }, _react.default.createElement("strong", null, this.props.onClick && this.props.value, this.state.showMessage && this.props.message)));
    }
  }]);

  return ButtonWrapper;
}(_react.default.Component);

;

var Name = function Name(props) {
  var isMain = props.main;
  var hasCallback = props.onClickResend;
  var hasToken = props.onClickCopy;

  var formatedName = _react.default.createElement("strong", null, _react.default.createElement("span", {
    style: {
      fontSize: 14
    }
  }, props.name, " ", isMain && '(Principal)'));

  var nameComponent = hasCallback ? _react.default.createElement(ButtonWrapper, {
    onClick: props.onClickResend,
    value: "(Reenviar)"
  }, formatedName) : formatedName;
  var copyComponent = hasToken ? _react.default.createElement(ButtonWrapper, {
    onClick: props.onClickCopy,
    title: props.title,
    value: "(Copiar Link)",
    message: " Copiado!"
  }) : '';
  return _react.default.createElement(_Text.default, null, nameComponent, "\xA0", copyComponent, "\xA0", props.children);
};

var _default = Name;
exports.default = _default;