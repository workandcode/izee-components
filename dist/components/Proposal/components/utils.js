"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.date = exports.area = exports.currency = void 0;

var _moment = _interopRequireDefault(require("moment"));

var _utils = require("../../../utils/utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var currency = function currency(number) {
  if (!(0, _utils.isFloat)(number) && !(0, _utils.isInvalidNumber)(number)) {
    number = (0, _utils.fixNumber)(number, 2) + ',00';
  } else {
    number = (0, _utils.fixNumber)(number, 2, 'pt-BR');
  }

  return number;
};

exports.currency = currency;

var area = function area(number) {
  number = (0, _utils.fixNumber)(number, 2, 'pt-BR');
  return number;
};

exports.area = area;

var date = function date(value) {
  if (!(0, _moment.default)(value, 'YYYY-MM-DD')._isValid) return null;
  return (0, _moment.default)(value, 'YYYY-MM-DD').format('DD/MM/YYYY');
};

exports.date = date;