"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactColor = require("react-color");

var _Icon = _interopRequireDefault(require("../lib/Icon"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Components
var ColorSelector = function ColorSelector(props) {
  return _react.default.createElement("div", {
    className: "theme-color"
  }, _react.default.createElement(_reactColor.TwitterPicker, {
    color: props.defaultColor,
    colors: ['#FF6900', '#FCB900', '#7BDCB5', '#00D084', '#2670FF', '#0693E3', '#ABB8C3', '#EB144C', '#F78DA7', '#9900EF'],
    triangle: "hide",
    onChange: props.onChange
  }), _react.default.createElement("div", {
    className: "theme-color-selected",
    style: {
      backgroundColor: props.defaultColor
    }
  }, props.isLoading ? _react.default.createElement(_Icon.default, {
    name: "far fa-fill-drip"
  }) : _react.default.createElement(_Icon.default, {
    name: "far fa-check"
  })));
};

ColorSelector.propTypes = {};
var _default = ColorSelector;
exports.default = _default;