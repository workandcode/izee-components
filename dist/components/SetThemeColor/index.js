"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var CreateTag =
/*#__PURE__*/
function (_React$PureComponent) {
  _inherits(CreateTag, _React$PureComponent);

  function CreateTag(props) {
    var _this;

    _classCallCheck(this, CreateTag);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(CreateTag).call(this, props));
    _this.state = {
      color: props.color || '#2670FF'
    };
    return _this;
  }

  _createClass(CreateTag, [{
    key: "render",
    value: function render() {
      return _react.default.createElement(_react.default.Fragment, null, ".has-theme-color ", '{', "background-color: ", this.state.color, " !important", '}');
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(props, state) {
      if (props.color !== state.color && !!props.color) {
        return {
          color: props.color
        };
      }

      return null;
    }
  }]);

  return CreateTag;
}(_react.default.PureComponent);

;
var body = document.body;

var Portal =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Portal, _React$Component);

  function Portal(props) {
    var _this2;

    _classCallCheck(this, Portal);

    _this2 = _possibleConstructorReturn(this, _getPrototypeOf(Portal).call(this, props));
    _this2.element = document.createElement('style');
    return _this2;
  }

  _createClass(Portal, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      body.appendChild(this.element);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      body.removeChild(this.element);
    }
  }, {
    key: "render",
    value: function render() {
      return _reactDom.default.createPortal(this.props.children, this.element);
    }
  }]);

  return Portal;
}(_react.default.Component);

var SetThemeColor = function SetThemeColor(props) {
  return _react.default.createElement(Portal, null, _react.default.createElement(CreateTag, props));
};

var _default = SetThemeColor;
exports.default = _default;