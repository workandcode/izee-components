"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _parsePropsAsBulmaClasses = _interopRequireDefault(require("../_utils/parsePropsAsBulmaClasses"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Title = function Title(props) {
  var titleCSSClass = "title ".concat((0, _parsePropsAsBulmaClasses.default)(props));

  var component = _react.default.createElement("h1", {
    className: titleCSSClass
  }, props.children);

  if (props.isH1) return _react.default.createElement("h1", {
    className: titleCSSClass
  }, props.children);
  if (props.isH2) return _react.default.createElement("h2", {
    className: titleCSSClass
  }, props.children);
  if (props.isH3) return _react.default.createElement("h3", {
    className: titleCSSClass
  }, props.children);
  if (props.isH4) return _react.default.createElement("h4", {
    className: titleCSSClass
  }, props.children);
  if (props.isH5) return _react.default.createElement("h5", {
    className: titleCSSClass
  }, props.children);
  if (props.isH6) return _react.default.createElement("h6", {
    className: titleCSSClass
  }, props.children);
  return component;
};

Title.defaultProps = {
  children: null
};
Title.propTypes = {
  children: _propTypes.default.any
};
var _default = Title;
exports.default = _default;