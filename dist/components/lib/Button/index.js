"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _Tooltip = _interopRequireDefault(require("../Tooltip"));

var _parsePropsAsBulmaClasses = _interopRequireDefault(require("../_utils/parsePropsAsBulmaClasses"));

var _utils = require("../../../utils/utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Button = function Button(props) {
  var buttonCSSClass = "button ".concat((0, _parsePropsAsBulmaClasses.default)(props));
  if (props.className) buttonCSSClass = props.className;

  var children = _react.default.Children.toArray(props.children).map(function (element, index) {
    if ((0, _utils.getTypeOf)(element) === 'String') {
      return _react.default.createElement("span", {
        key: index
      }, element);
    } else {
      return element;
    }
  });

  return _react.default.createElement("button", {
    type: props.submit ? 'submit' : 'button',
    "data-spec-selector": props.specSelector,
    className: buttonCSSClass,
    onClick: props.onClick || null,
    style: props.style,
    disabled: props.disabled || props.isDisabled,
    "aria-hidden": props.isStatic,
    tabIndex: props.isStatic ? -1 : props.tabIndex,
    role: props.role ? props.role : null
  }, props.title ? _react.default.createElement(_Tooltip.default, {
    description: props.title
  }, children) : children);
};

Button.propTypes = {
  children: _propTypes.default.any.isRequired,
  // onClick: PropTypes.func,
  disabled: _propTypes.default.bool
};
var _default = Button;
exports.default = _default;