"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _parsePropsAsBulmaClasses = _interopRequireDefault(require("../_utils/parsePropsAsBulmaClasses"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var BodyContainer =
/*#__PURE__*/
function (_React$Component) {
  _inherits(BodyContainer, _React$Component);

  function BodyContainer(props) {
    var _this;

    _classCallCheck(this, BodyContainer);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(BodyContainer).call(this, props));
    _this.container = _react.default.createRef();
    _this.pathname = '';
    return _this;
  }

  _createClass(BodyContainer, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.props.getRef(this.container.current);
      this.pathname = location.pathname;
    }
  }, {
    key: "getSnapshotBeforeUpdate",
    value: function getSnapshotBeforeUpdate() {
      return null;
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      if (this.pathname !== location.pathname) {
        this.pathname = location.pathname;
        this.container.current.scrollTo({
          'behavior': 'smooth',
          'left': 0,
          'top': 0
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      var hasLoadingProp = this.props.isLoading !== null && this.props.isLoading !== undefined;
      var isLoading = hasLoadingProp && this.props.isLoading;
      var CSSClass = "body-container ".concat((0, _parsePropsAsBulmaClasses.default)(this.props)).concat(hasLoadingProp ? ' has-loading' : '').concat(isLoading ? ' is-loading' : '');
      return _react.default.createElement("div", {
        className: CSSClass,
        ref: this.container,
        style: this.props.style
      }, isLoading ? _react.default.createElement("div", {
        className: "empty-state-spinner"
      }) : null, this.props.children);
    }
  }]);

  return BodyContainer;
}(_react.default.Component);

exports.default = BodyContainer;
;
BodyContainer.defaultProps = {
  getRef: function getRef() {}
};