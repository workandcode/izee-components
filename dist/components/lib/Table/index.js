"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _utils = require("../../../utils/utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Table =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Table, _React$Component);

  function Table(props) {
    var _this;

    _classCallCheck(this, Table);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Table).call(this, props));
    _this.state = {
      items: _this.props.items,
      invertOrder: true
    };
    return _this;
  }

  _createClass(Table, [{
    key: "getSnapshotBeforeUpdate",
    value: function getSnapshotBeforeUpdate(prevProps) {
      if (JSON.stringify(prevProps.items) !== JSON.stringify(this.props.items)) {
        this.setState({
          items: this.props.items
        });
      }

      return null;
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var tableHeader = this.props.cells.map(function (item, index) {
        var isObject = (0, _utils.getTypeOf)(item) === 'Object';
        var headerLabel = isObject ? item.name : item;
        var sortCallback = item.path ? _this2.sort.bind(_this2, item.path) : null;
        return _react.default.createElement("th", {
          key: index,
          className: "datatable-cell",
          style: {
            cursor: item.path ? 'pointer' : null,
            textAlign: item.align,
            width: item.width
          },
          width: item.shrink ? '1px' : 'auto',
          onClick: sortCallback
        }, _react.default.createElement("span", {
          className: item.path ? 'is-sortable' : null
        }, headerLabel));
      });

      var Cells = function Cells(props) {
        var rowKeys = Object.keys(props);
        return _this2.props.cells.map(function (item, index) {
          var content = !item.render ? function (itemData) {
            return itemData;
          } : item.render;
          var contentData = props[rowKeys[index]];
          if (item.path) contentData = (0, _utils.deepKey)(props, item.path);
          var CSSClass = "datatable-cell ".concat(_this2.props.isUpdatingRows ? 'opacity has-text-grey-light' : '').concat(item.isMultiline ? ' is-multiline' : '');
          return _react.default.createElement("td", {
            className: CSSClass,
            "data-label": item.name,
            style: {
              textAlign: item.align
            },
            key: index
          }, _react.default.createElement("span", {
            onClick: item.onClick ? item.onClick.bind(null, props[rowKeys[index]]) : null,
            onKeyDown: item.onClick ? item.onClick.bind(null, props[rowKeys[index]]) : null,
            tabIndex: "0",
            role: "button"
          }, content(contentData, props)));
        });
      };

      var hasActionCell = this.props.actionCell;
      var tableRows = this.state.items.map(function (item, index) {
        var onClickRow = _this2.props.onClickRow;
        return _react.default.createElement("tr", {
          className: "datatable-row has-hover",
          onClick: onClickRow ? onClickRow.bind(null, item) : null,
          onKeyDown: onClickRow ? onClickRow.bind(null, item) : null,
          tabIndex: "0",
          role: "button",
          style: {
            cursor: onClickRow ? 'pointer' : null
          },
          key: index
        }, _react.default.createElement(Cells, item), hasActionCell ? _react.default.createElement("td", {
          className: "datatable-cell"
        }, _react.default.createElement("div", {
          className: "buttons"
        }, _this2.props.actionCell(item))) : null);
      });
      var isEmpty = this.state.items.length < 1;
      return _react.default.createElement(_react.default.Fragment, null, isEmpty ? _react.default.createElement("div", {
        className: "datatable-empty"
      }, _react.default.createElement("table", {
        className: "datatable"
      }, _react.default.createElement("thead", {
        className: "datatable-head"
      }, _react.default.createElement("tr", {
        className: "datatable-row"
      }, _react.default.createElement("th", {
        className: "datatable-cell mock-text",
        colSpan: "100"
      }, "The table is empty :("))), _react.default.createElement("tbody", {
        className: "datatable-body"
      }, _react.default.createElement("tr", {
        className: "datatable-row"
      }, _react.default.createElement("td", {
        className: "datatable-cell mock-text",
        colSpan: "100"
      }, this.props.emptyState ? this.props.emptyState : '. . .'))), this.props.footer ? _react.default.createElement("tfoot", {
        className: "datatable-footer"
      }, _react.default.createElement("tr", {
        className: "datatable-row mock-text"
      }, _react.default.createElement("td", {
        className: "datatable-cell",
        colSpan: "100"
      }))) : null)) : _react.default.createElement("table", {
        className: "datatable"
      }, _react.default.createElement("thead", {
        className: "datatable-head"
      }, _react.default.createElement("tr", {
        className: "datatable-row"
      }, tableHeader, hasActionCell ? _react.default.createElement("th", {
        className: "datatable-cell",
        width: "1"
      }) : null)), _react.default.createElement("tbody", {
        className: "datatable-body"
      }, tableRows), this.props.footer ? _react.default.createElement("tfoot", {
        className: "datatable-footer"
      }, _react.default.createElement("tr", {
        className: "datatable-row"
      }, _react.default.createElement("td", {
        className: "datatable-cell",
        colSpan: "100"
      }, this.props.footer))) : null));
    }
  }, {
    key: "handleFilter",
    value: function handleFilter(_ref) {
      var target = _ref.target;
      var items = (0, _utils.filterArrayBy)(this.props.items, target.value);
      this.setState({
        items: items
      });
    }
  }, {
    key: "sort",
    value: function sort(orderBy) {
      if (this.props.onSort) {
        this.props.onSort(orderBy);
        return;
      }

      var items = this.state.items;

      if (this.state.invertOrder) {
        items.sort(function (array1, array2) {
          if ((0, _utils.deepKey)(array1, orderBy) > (0, _utils.deepKey)(array2, orderBy)) return 1;
          if ((0, _utils.deepKey)(array1, orderBy) < (0, _utils.deepKey)(array2, orderBy)) return -1;
          return 0;
        });
      } else {
        items.sort(function (array1, array2) {
          if ((0, _utils.deepKey)(array1, orderBy) < (0, _utils.deepKey)(array2, orderBy)) return 1;
          if ((0, _utils.deepKey)(array1, orderBy) > (0, _utils.deepKey)(array2, orderBy)) return -1;
          return 0;
        });
      }

      this.setState({
        items: items,
        invertOrder: !this.state.invertOrder
      });
    }
  }]);

  return Table;
}(_react.default.Component);

exports.default = Table;
;
Table.defaultProps = {
  onSort: null,
  onClickRow: null
};
Table.propTypes = {
  cells: _propTypes.default.array.isRequired,
  items: _propTypes.default.array.isRequired,
  onSort: _propTypes.default.func,
  onClickRow: _propTypes.default.func
};