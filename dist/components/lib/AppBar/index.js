"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _parsePropsAsBulmaClasses = _interopRequireDefault(require("../_utils/parsePropsAsBulmaClasses"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Utils
var FlexBar = function FlexBar(props) {
  var appBarCSSClass = "appbar ".concat((0, _parsePropsAsBulmaClasses.default)(props)).concat(props.className ? ' ' + props.className : '');
  return _react.default.createElement("div", {
    "data-spec-selector": props.specSelector,
    className: appBarCSSClass
  }, props.children);
};

FlexBar.Child = function (props) {
  var appBarChildCSSClass = "appbar-child ".concat((0, _parsePropsAsBulmaClasses.default)(props)).concat(props.className ? ' ' + props.className : '');
  return _react.default.createElement("div", {
    "data-spec-selector": props.specSelector,
    className: appBarChildCSSClass,
    style: props.style
  }, props.children);
};

var _default = FlexBar;
exports.default = _default;