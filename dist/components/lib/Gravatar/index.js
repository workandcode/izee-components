"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _gravatar = _interopRequireDefault(require("gravatar"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Gravatar = function Gravatar(props) {
  var size = props.size || 240;
  return _react.default.createElement("img", {
    src: _gravatar.default.url(props.email, {
      size: size,
      d: 'mp'
    }),
    alt: props.alt
  });
};

Gravatar.propTypes = {
  email: _propTypes.default.string.isRequired
};
var _default = Gravatar;
exports.default = _default;