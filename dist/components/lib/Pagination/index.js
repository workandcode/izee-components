"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Pagination =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Pagination, _React$Component);

  function Pagination() {
    var _this;

    _classCallCheck(this, Pagination);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Pagination).call(this));
    _this.state = {
      currentPage: 1,
      lastPage: 10,
      firstPage: 1,
      total: 0,
      itemsPerPage: 0,
      pages: 0
    };
    return _this;
  }

  _createClass(Pagination, [{
    key: "getSnapshotBeforeUpdate",
    value: function getSnapshotBeforeUpdate(prevProps) {
      if (this.props.total !== prevProps.total || this.props.itemsPerPage !== prevProps.itemsPerPage || this.props.current !== prevProps.current) {
        var totalPages = this.props.total / this.props.itemsPerPage;

        if (isNaN(totalPages)) {
          if (this.props.current) {
            if (this.props.onChange) this.props.onChange(this.props.current);
          }

          return;
        }

        this.setState({
          currentPage: this.props.current || 1,
          pages: Math.ceil(totalPages)
        });
      }

      return null;
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {}
  }, {
    key: "componentDidMount",
    value: function componentDidMount() {
      var totalPages = this.props.total / this.props.itemsPerPage;

      if (isNaN(totalPages)) {
        if (this.props.current) {
          if (this.props.onChange) this.props.onChange(this.props.current);
        }

        return;
      }

      this.setState({
        currentPage: this.props.current || 1,
        pages: Math.ceil(totalPages)
      });
    }
  }, {
    key: "updatePage",
    value: function updatePage(page) {
      var currentPage = page > 0 ? page < this.state.pages ? page : this.state.pages : 1;
      var lastPage = 10;
      var firstPage = 1;
      var previousPage = this.state.currentPage;

      if (page < this.state.lastPage) {
        firstPage = currentPage < this.state.lastPage && currentPage > this.state.firstPage ? this.state.firstPage : currentPage - 1 > 0 ? currentPage - 1 : 1;
        lastPage = firstPage + 9;
        this.setState({
          currentPage: currentPage,
          firstPage: firstPage,
          lastPage: lastPage
        });
      } else {
        lastPage = currentPage < this.state.lastPage ? this.state.lastPage : currentPage + 1 < this.state.pages ? currentPage + 1 : this.state.pages;
        firstPage = currentPage > this.state.firstPage ? lastPage - 9 : currentPage;
        this.setState({
          currentPage: currentPage,
          firstPage: firstPage,
          lastPage: lastPage
        });
      }

      if (this.props.onChange) this.props.onChange(currentPage, previousPage);
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$state = this.state,
          firstPage = _this$state.firstPage,
          lastPage = _this$state.lastPage,
          currentPage = _this$state.currentPage;
      var pages = this.state.pages;
      var pageNumbers = [];

      for (var i = firstPage; i <= (pages < 11 ? pages : lastPage); i++) {
        pageNumbers.push(i);
      }

      var pageNumbersSelect = [];

      for (var _i = firstPage; _i <= (pages < 11 ? pages : lastPage); _i++) {
        pageNumbersSelect.push(_i);
      }

      var pagesNumbers = pageNumbers.map(function (item, index) {
        var pageClasses = (0, _classnames.default)(['pagination-link', {
          'is-current': _this2.state.currentPage == item
        }]);
        return _react.default.createElement("li", {
          className: pageClasses,
          tabIndex: "0",
          role: "button",
          key: index,
          onKeyDown: _this2.updatePage.bind(_this2, item),
          onClick: _this2.updatePage.bind(_this2, item)
        }, item);
      });
      var pagesNumbersSelect = pageNumbersSelect.map(function (item, index) {
        return _react.default.createElement("option", {
          key: index,
          onKeyDown: _this2.updatePage.bind(_this2, item),
          onClick: _this2.updatePage.bind(_this2, item)
        }, item);
      });
      if (pages < 2) return null;
      return _react.default.createElement("div", {
        className: "pagination-container"
      }, _react.default.createElement("ul", {
        className: "pagination"
      }, pages > 3 ? _react.default.createElement("li", {
        className: "pagination-previous",
        tabIndex: "0",
        role: "button",
        onKeyDown: this.updatePage.bind(this, 1),
        onClick: this.updatePage.bind(this, 1)
      }, _react.default.createElement("i", {
        className: "fal fa-lg fa-angle-double-left"
      })) : null, pages > 10 ? _react.default.createElement("li", {
        className: "pagination-link is-arrow",
        tabIndex: "0",
        role: "button",
        onKeyDown: this.updatePage.bind(this, this.state.currentPage - 1),
        onClick: this.updatePage.bind(this, this.state.currentPage - 1)
      }, _react.default.createElement("i", {
        className: "fal fa-lg fa-angle-left"
      })) : null, pagesNumbers, pages > 10 ? _react.default.createElement("li", {
        className: "pagination-link is-arrow",
        tabIndex: "0",
        role: "button",
        onKeyDown: this.updatePage.bind(this, this.state.currentPage + 1),
        onClick: this.updatePage.bind(this, this.state.currentPage + 1)
      }, _react.default.createElement("i", {
        className: "fal fa-lg fa-angle-right"
      })) : null, pages > 3 ? _react.default.createElement("li", {
        className: "pagination-next",
        tabIndex: "0",
        role: "button",
        onKeyDown: this.updatePage.bind(this, this.state.pages),
        onClick: this.updatePage.bind(this, this.state.pages)
      }, _react.default.createElement("i", {
        className: "fal fa-lg fa-angle-double-right"
      })) : null));
    }
  }]);

  return Pagination;
}(_react.default.Component);

exports.default = Pagination;
;
Pagination.propTypes = {};