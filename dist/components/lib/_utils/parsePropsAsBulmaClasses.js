"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = parsePropsAsBulmaClasses;

var _utils = require("../../../utils/utils");

/**
 * Find bulma classes as react props like isSomething or hasSomething and transform into css classes
 * @param {Object} componentProps
 * @return {String} of classes
 */
function parsePropsAsBulmaClasses(componentProps) {
  var props = Object.keys(componentProps);
  var classNames = [];
  props.forEach(function (propsKey) {
    if (propsKey.match(/(is|has)([A-Z|0-9]{1})/g) && componentProps[propsKey]) classNames.push((0, _utils.camelCaseToDash)(propsKey));
  });
  return classNames.join(' ');
}

;