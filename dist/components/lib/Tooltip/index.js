"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

var Tooltip =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Tooltip, _React$Component);

  function Tooltip() {
    var _this;

    _classCallCheck(this, Tooltip);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Tooltip).call(this));
    _this.state = {
      style: {
        position: 'fixed',
        visibility: 'hidden',
        opacity: 0
      }
    };
    _this.onMouseOutHandler = _this.onMouseOutHandler.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    return _this;
  }

  _createClass(Tooltip, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      window.addEventListener('wheel', this.onMouseOutHandler);
      window.addEventListener('touchmove', this.onMouseOutHandler);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      window.removeEventListener('wheel', this.onMouseOutHandler);
      window.removeEventListener('touchmove', this.onMouseOutHandler);
    }
  }, {
    key: "onMouseOverHandler",
    value: function onMouseOverHandler() {
      var container = this.tooltipContainer.getBoundingClientRect();
      var tooltip = this.tooltip.getBoundingClientRect();
      this.setState({
        style: {
          position: 'fixed',
          zIndex: 99999999999,
          visibility: 'visible',
          opacity: 1,
          top: container.top - tooltip.height - 10,
          left: container.left - (tooltip.width / 2 - container.width / 2)
        }
      });
    }
  }, {
    key: "onMouseOutHandler",
    value: function onMouseOutHandler() {
      this.setState({
        style: {
          position: 'fixed',
          visibility: 'hidden',
          opacity: 0
        }
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var _this$props = this.props,
          description = _this$props.description,
          direction = _this$props.direction,
          children = _this$props.children;
      var tooltipClasses = (0, _classnames.default)(['tooltip', "tooltip--".concat(direction)]);
      return _react.default.createElement("span", null, _react.default.createElement("span", {
        className: tooltipClasses,
        onMouseOver: this.onMouseOverHandler.bind(this),
        onPointerDown: this.onMouseOverHandler.bind(this),
        onMouseOut: this.onMouseOutHandler.bind(this),
        onTouchMove: this.onMouseOutHandler.bind(this),
        ref: function ref(element) {
          return _this2.tooltipContainer = element;
        }
      }, children), _react.default.createElement("span", {
        className: "tooltip__description",
        style: this.state.style,
        ref: function ref(element) {
          return _this2.tooltip = element;
        }
      }, description));
    }
  }]);

  return Tooltip;
}(_react.default.Component);

exports.default = Tooltip;
;
Tooltip.defaultProps = {
  direction: 'top'
};
Tooltip.propTypes = {};