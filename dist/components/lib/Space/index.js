"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _parsePropsAsBulmaClasses = _interopRequireDefault(require("../_utils/parsePropsAsBulmaClasses"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Space = function Space(props) {
  var spaceCSSClass = "space ".concat((0, _parsePropsAsBulmaClasses.default)(props)).concat(props.isHorizontal ? ' is-horizontal' : ' is-vertical');
  return _react.default.createElement("div", {
    className: spaceCSSClass
  });
};

Space.propTypes = {};
var _default = Space;
exports.default = _default;