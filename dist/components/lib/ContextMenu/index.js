"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _Button = _interopRequireDefault(require("../Button"));

var _Icon = _interopRequireDefault(require("../Icon"));

var _parsePropsAsBulmaClasses = _interopRequireDefault(require("../_utils/parsePropsAsBulmaClasses"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

var ContextMenu =
/*#__PURE__*/
function (_React$Component) {
  _inherits(ContextMenu, _React$Component);

  function ContextMenu() {
    var _this;

    _classCallCheck(this, ContextMenu);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(ContextMenu).call(this));
    _this.state = {
      isActive: false
    };
    _this.container = _react.default.createRef();
    _this.handleClickOutside = _this.handleClickOutside.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    return _this;
  }

  _createClass(ContextMenu, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      document.addEventListener('click', this.handleClickOutside, true);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      document.removeEventListener('click', this.handleClickOutside, true);
    }
  }, {
    key: "render",
    value: function render() {
      var contextMenuCSSClass = "context-menu is-visible".concat(this.state.isActive ? ' is-active' : '').concat(' ' + (0, _parsePropsAsBulmaClasses.default)(this.props));
      return _react.default.createElement("div", {
        className: contextMenuCSSClass,
        ref: this.container,
        role: "menu"
      }, _react.default.createElement("button", {
        className: "context-menu-button has-theme-color",
        onClick: this.handleToggleState.bind(this)
      }, _react.default.createElement(_Icon.default, {
        name: "far fa-md fa-plus"
      })), _react.default.createElement("div", {
        className: "context-menu-content"
      }, this.props.children));
    }
  }, {
    key: "handleClickOutside",
    value: function handleClickOutside(event) {
      var container = this.container.current;
      if (container && !container.contains(event.target)) this.closeContextMenu();
    }
  }, {
    key: "handleToggleState",
    value: function handleToggleState() {
      if (_react.default.Children.toArray(this.props.children).length < 1) {
        if (this.props.onClick) this.props.onClick();
        return;
      }

      this.setState({
        isActive: !this.state.isActive
      });
    }
  }, {
    key: "closeContextMenu",
    value: function closeContextMenu() {
      if (this.state.isActive) this.setState({
        isActive: false
      });
    }
  }]);

  return ContextMenu;
}(_react.default.Component);

exports.default = ContextMenu;
;
ContextMenu.defaultProps = {
  children: null,
  onClick: null
};
ContextMenu.propTypes = {
  onClick: _propTypes.default.func,
  children: _propTypes.default.any
};