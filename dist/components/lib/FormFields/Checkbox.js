"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _Checkbox = _interopRequireWildcard(require("./components/Checkbox"));

var _Field = _interopRequireDefault(require("./components/Field"));

var _Control = _interopRequireDefault(require("./components/Control"));

var _fieldCreator = _interopRequireDefault(require("./fieldCreator"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var FormFieldCheckbox = function FormFieldCheckbox(props) {
  var isValid = props.isValid !== null && !props.isValid && !props.isEmpty;
  return _react.default.createElement(_Field.default, {
    label: props.label
  }, _react.default.createElement(_Control.default, _extends({}, props, {
    isLoading: false,
    hasCheckbox: true
  }), _react.default.createElement(_Checkbox.default, _extends({}, (0, _Checkbox.propsObject)(props), {
    description: props.description,
    info: props.info,
    checked: props.checked,
    isLoading: props.isLoading,
    isDanger: isValid || props.forceInvalid
  }))), props.forceInvalid && props.errorMessage ? _react.default.createElement("p", {
    className: "help is-danger"
  }, props.errorMessage) : null);
};

var _default = (0, _fieldCreator.default)(FormFieldCheckbox, {
  masks: [],
  validators: [{
    'minLength': 1
  }],
  valueType: 'boolean'
});

exports.default = _default;