"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var invalidFields = function invalidFields(fields) {
  if (!fields) return;
  var fieldsArray = Object.keys(fields);
  fieldsArray = fieldsArray.filter(function (field) {
    return fields[field].isValid !== null && !fields[field].isValid;
  });
  return fieldsArray;
};

var _default = invalidFields;
exports.default = _default;