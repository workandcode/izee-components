"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _utils = require("../../../../utils/utils");

var getValues = function getValues(fieldsObject) {
  var newObject = {};
  var fieldsArray = Object.keys(fieldsObject);
  fieldsArray.forEach(function (field) {
    (0, _utils.deepKey)(newObject, field, fieldsObject[field].value);
  });
  return newObject;
};

var _default = getValues;
exports.default = _default;