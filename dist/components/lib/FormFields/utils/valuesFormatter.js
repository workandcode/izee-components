"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var getValues = function getValues(fieldsObject) {
  var newObject = {};
  var fieldsArray = Object.keys(fieldsObject);
  fieldsArray.forEach(function (field) {
    newObject[field] = fieldsObject[field].value;
  });
  return newObject;
};

var _default = getValues;
exports.default = _default;