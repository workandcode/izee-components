"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.propsObject = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _propsObject2 = _interopRequireDefault(require("./propsObject"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var Select = function Select(props) {
  var options = props.options.map(function (item, index) {
    return _react.default.createElement("option", {
      value: item[props.valueKey] || item.value,
      key: index
    }, item[props.labelKey] || item.name);
  });
  return _react.default.createElement("div", {
    className: "select"
  }, _react.default.createElement("select", _extends({}, (0, _propsObject2.default)(props), {
    value: props.value && props.value[props.valueKey] || props.value || ''
  }), props.placeholder && _react.default.createElement("option", {
    value: ""
  }, props.placeholder), !props.hideBlankOption && _react.default.createElement("option", {
    value: ""
  }), options));
};

Select.defaultProps = {
  valueKey: 'name'
};
Select.propTypes = {
  options: _propTypes.default.arrayOf(_propTypes.default.object).isRequired
};
var propsObject = _propsObject2.default;
exports.propsObject = propsObject;
var _default = Select;
exports.default = _default;