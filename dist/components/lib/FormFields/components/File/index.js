"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.propsObject = void 0;

var _react = _interopRequireDefault(require("react"));

var _propsObject2 = _interopRequireDefault(require("./propsObject"));

var _parsePropsAsBulmaClasses = _interopRequireDefault(require("../../../_utils/parsePropsAsBulmaClasses"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var File = function File(props) {
  var inputCSSClass = "file ".concat((0, _parsePropsAsBulmaClasses.default)(props));

  if (_react.default.Children.toArray(props.children).length > 0) {
    return _react.default.createElement("label", {
      htmlFor: props.id,
      className: "file-input-wrapper"
    }, _react.default.createElement("input", (0, _propsObject2.default)(props)), _react.default.createElement("span", {
      className: "file-input-wrapper-content"
    }, props.children), _react.default.createElement("span", {
      className: "file-input-wrapper-label"
    }, props.label));
  }

  return _react.default.createElement("div", {
    className: inputCSSClass
  }, _react.default.createElement("label", {
    htmlFor: props.id,
    className: "file-label"
  }, _react.default.createElement("input", _extends({
    className: "file-input"
  }, (0, _propsObject2.default)(props))), _react.default.createElement("span", {
    className: "file-cta"
  }, _react.default.createElement("span", {
    className: "file-icon"
  }, _react.default.createElement("i", {
    className: "fas fa-upload"
  })), _react.default.createElement("span", {
    className: "file-label",
    style: {
      marginLeft: props.description ? 0 : '-0.5em'
    }
  }, props.description)), _react.default.createElement("span", {
    className: "file-name",
    title: props.filename
  }, props.filename)));
};

var propsObject = _propsObject2.default;
exports.propsObject = propsObject;
var _default = File;
exports.default = _default;