"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = function _default(props) {
  return {
    id: props.id,
    type: 'file',
    multiple: props.multiple || props.isMultiple,
    accept: props.accept,
    'data-spec-selector': props.specSelector || props.name || props.id,
    name: props.name || props.id,
    onClick: props.onClick,
    onChange: props.onChange,
    onKeyDown: props.onKeyDown,
    onKeyUp: props.onKeyUp,
    onInput: props.onInput,
    onFocus: props.onFocus,
    onBlur: props.onBlur,
    placeholder: props.placeholder,
    disabled: props.disabled || props.isDisabled,
    style: props.style
  };
};

exports.default = _default;