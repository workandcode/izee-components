"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _parsePropsAsBulmaClasses = _interopRequireDefault(require("../../../_utils/parsePropsAsBulmaClasses"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Control = function Control(props) {
  var controlCSSClass = "control ".concat((0, _parsePropsAsBulmaClasses.default)(props));
  return _react.default.createElement("span", {
    className: controlCSSClass,
    style: props.style
  }, props.children);
};

Control.propTypes = {
  children: _propTypes.default.any.isRequired
};
var _default = Control;
exports.default = _default;