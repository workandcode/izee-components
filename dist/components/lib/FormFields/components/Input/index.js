"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.propsObject = void 0;

var _react = _interopRequireDefault(require("react"));

var _propsObject2 = _interopRequireDefault(require("./propsObject"));

var _parsePropsAsBulmaClasses = _interopRequireDefault(require("../../../_utils/parsePropsAsBulmaClasses"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var Input = function Input(props) {
  var inputCSSClass = "input ".concat((0, _parsePropsAsBulmaClasses.default)(props));
  return _react.default.createElement("input", _extends({
    className: inputCSSClass
  }, (0, _propsObject2.default)(props)));
};

var propsObject = _propsObject2.default;
exports.propsObject = propsObject;
var _default = Input;
exports.default = _default;