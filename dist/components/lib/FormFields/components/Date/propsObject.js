"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _moment = _interopRequireDefault(require("moment"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _default = function _default(props) {
  // console.log(props.value, props.value && moment(props.value, 'DD/MM/YYYY').format('YYYY-MM-DD'));
  // const date = props.value && moment(props.value, 'DD/MM/YYYY').format('YYYY-MM-DD');
  // console.log(props.defaultValue, props.value);
  return {
    id: props.id,
    type: props.type || 'text',
    'data-spec-selector': props.specSelector || props.name || props.id,
    name: props.name || props.id,
    autoComplete: props.autoComplete || 'autocomplete-off',
    autoCorrect: props.autoCorrect,
    autoCapitalize: props.autoCapitalize,
    spellCheck: props.spellCheck,
    value: props.value,
    defaultValue: props.defaultValue,
    onClick: props.onClick,
    onChange: props.onChange,
    onKeyDown: props.onKeyDown,
    onKeyUp: props.onKeyUp,
    onInput: props.onInput,
    onFocus: props.onFocus,
    onBlur: props.onBlur,
    placeholder: props.placeholder,
    readOnly: props.readOnly,
    disabled: props.disabled || props.isDisabled,
    style: props.style
  };
};

exports.default = _default;