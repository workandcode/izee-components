"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.propsObject = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _Button = _interopRequireDefault(require("../../../Button"));

var _Icon = _interopRequireDefault(require("../../../Icon"));

var _Tooltip = _interopRequireDefault(require("../../../Tooltip"));

var _propsObject2 = _interopRequireDefault(require("./propsObject"));

var _parsePropsAsBulmaClasses = _interopRequireDefault(require("../../../_utils/parsePropsAsBulmaClasses"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Checkbox = function Checkbox(props) {
  var checkboxCSSClass = "checkbox ".concat((0, _parsePropsAsBulmaClasses.default)(props));
  var tooltip = props.info ? _react.default.createElement(_Tooltip.default, {
    description: props.info
  }, "\xA0", _react.default.createElement(_Button.default, {
    isBare: true,
    tabIndex: -1
  }, _react.default.createElement(_Icon.default, {
    name: "fas fa-question-circle fa-xs",
    isInfo: true,
    isSmall: true
  }))) : null;
  var loading = props.isLoading ? _react.default.createElement(_react.default.Fragment, null, "\xA0", _react.default.createElement("span", {
    className: "loader"
  })) : null;
  return _react.default.createElement("label", {
    htmlFor: props.id || props.name,
    className: checkboxCSSClass
  }, _react.default.createElement("input", (0, _propsObject2.default)(props)), _react.default.createElement("span", {
    className: "checkbox-token"
  }), _react.default.createElement("span", {
    className: "checkbox-description"
  }, props.description, tooltip, loading));
};

Checkbox.propTypes = {
  id: _propTypes.default.any,
  name: _propTypes.default.any,
  value: _propTypes.default.any,
  defaultValue: _propTypes.default.any,
  onClick: _propTypes.default.func,
  onChange: _propTypes.default.func,
  onInput: _propTypes.default.func,
  onFocus: _propTypes.default.func,
  onBlur: _propTypes.default.func
};
var propsObject = _propsObject2.default;
exports.propsObject = propsObject;
var _default = Checkbox;
exports.default = _default;