"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _default = function _default(props) {
  return {
    id: props.id || props.name,
    type: props.type || 'checkbox',
    'data-spec-selector': props.specSelector || props.name || props.id,
    name: props.name || props.id,
    value: props.value,
    defaultChecked: props.defaultChecked,
    checked: props.checked,
    onClick: props.onClick,
    onChange: props.onChange,
    onKeyDown: props.onKeyDown,
    onKeyUp: props.onKeyUp,
    onInput: props.onInput,
    onFocus: props.onFocus,
    onBlur: props.onBlur,
    placeholder: props.placeholder,
    disabled: props.disabled || props.isDisabled,
    style: props.style
  };
};

exports.default = _default;