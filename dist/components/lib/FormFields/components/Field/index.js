"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _Button = _interopRequireDefault(require("../../../Button"));

var _Icon = _interopRequireDefault(require("../../../Icon"));

var _Tooltip = _interopRequireDefault(require("../../../Tooltip"));

var _parsePropsAsBulmaClasses = _interopRequireDefault(require("../../../_utils/parsePropsAsBulmaClasses"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Field = function Field(props) {
  var fieldCSSClass = "field ".concat((0, _parsePropsAsBulmaClasses.default)(props));
  var tooltip = props.info ? _react.default.createElement(_Tooltip.default, {
    description: props.info
  }, _react.default.createElement(_Button.default, {
    isBare: true,
    tabIndex: -1
  }, _react.default.createElement(_Icon.default, {
    name: "fas fa-question-circle fa-xs",
    isInfo: true,
    isSmall: true
  }))) : null;
  return _react.default.createElement("div", {
    className: fieldCSSClass,
    style: props.style
  }, props.label ? _react.default.createElement("label", {
    className: "label",
    htmlFor: props.id
  }, props.label, " ", tooltip) : null, props.children);
};

Field.propTypes = {
  children: _propTypes.default.any.isRequired
};
var _default = Field;
exports.default = _default;