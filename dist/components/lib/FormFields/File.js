"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _File = _interopRequireWildcard(require("./components/File"));

var _Field = _interopRequireDefault(require("./components/Field"));

var _Control = _interopRequireDefault(require("./components/Control"));

var _fieldCreator = _interopRequireDefault(require("./fieldCreator"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var FormFieldFile =
/*#__PURE__*/
function (_React$Component) {
  _inherits(FormFieldFile, _React$Component);

  function FormFieldFile(props) {
    var _this;

    _classCallCheck(this, FormFieldFile);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(FormFieldFile).call(this, props));
    _this.state = {
      filename: null,
      files: []
    };
    return _this;
  }

  _createClass(FormFieldFile, [{
    key: "render",
    value: function render() {
      var isValid = this.props.isValid !== null && !this.props.isValid && !this.props.isEmpty;
      var showMessage = this.props.forceInvalid && this.props.errorMessage;

      if (this.props.render) {
        return _react.default.createElement(_File.default, _extends({}, (0, _File.propsObject)(this.props), {
          label: this.props.label,
          onChange: this.handleFileChanges.bind(this)
        }), this.props.render(_objectSpread({}, this.props, this.state, {
          filename: this.state.filename,
          isValid: isValid,
          showMessage: showMessage
        })));
      }

      return _react.default.createElement(_Field.default, this.props, _react.default.createElement(_Control.default, this.props, _react.default.createElement(_File.default, _extends({}, (0, _File.propsObject)(this.props), {
        hasName: true,
        filename: this.state.filename,
        description: this.props.description,
        onChange: this.handleFileChanges.bind(this),
        isDanger: isValid || this.props.forceInvalid
      }))), showMessage ? _react.default.createElement("p", {
        className: "help is-danger"
      }, this.props.errorMessage) : null);
    }
  }, {
    key: "handleFileChanges",
    value: function handleFileChanges(event) {
      var files = [];
      var filename = [];
      var readAsText = this.props.readAsText;

      function readFiles(file) {
        filename.push(file.name);
        files.push({
          raw: file,
          name: file.name,
          size: file.size,
          ext: file.name ? file.name.match(/[^.]+$/)[0] : null,
          preview: function preview() {
            return new Promise(function (resolve) {
              var reader = new FileReader();
              reader.addEventListener('load', function () {
                return resolve(reader.result);
              });

              if (readAsText) {
                reader.readAsText(file);
              } else {
                reader.readAsDataURL(file);
              }
            });
          }
        });
      }

      if (event.target.files) {
        _toConsumableArray(event.target.files).forEach(readFiles);
      }

      this.setState({
        filename: filename.join(', '),
        files: files
      });
      this.props.onChange({
        target: {
          type: 'file',
          value: files
        }
      });
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(props, state) {
      if (state.filename === null) {
        return {
          filename: props.filename
        };
      }

      return null;
    }
  }]);

  return FormFieldFile;
}(_react.default.Component);

;

var _default = (0, _fieldCreator.default)(FormFieldFile, {
  masks: [],
  validators: []
});

exports.default = _default;