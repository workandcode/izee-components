"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactDatepicker = _interopRequireDefault(require("react-datepicker"));

require("react-datepicker/dist/react-datepicker.css");

var _moment = _interopRequireDefault(require("moment"));

var _Input = _interopRequireWildcard(require("./components/Input"));

var _Field = _interopRequireDefault(require("./components/Field"));

var _Button = _interopRequireDefault(require("../Button"));

var _Control = _interopRequireDefault(require("./components/Control"));

var _Icon = _interopRequireDefault(require("../Icon"));

var _fieldCreator = _interopRequireDefault(require("./fieldCreator"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var FormFieldDate =
/*#__PURE__*/
function (_React$Component) {
  _inherits(FormFieldDate, _React$Component);

  function FormFieldDate(props) {
    var _this;

    _classCallCheck(this, FormFieldDate);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(FormFieldDate).call(this, props));
    _this.state = {// selected: moment()
    };
    _this.inputContainer = _react.default.createRef();
    return _this;
  }

  _createClass(FormFieldDate, [{
    key: "render",
    value: function render() {
      _moment.default.locale('pt-BR', {
        longDateFormat: {
          L: 'DD/MM/YYYY'
        }
      });

      var isValid = this.props.isValid !== null && !this.props.isValid && !this.props.isEmpty;
      return _react.default.createElement(_Field.default, _extends({}, this.props, {
        hasAddons: true,
        style: {
          width: 141
        }
      }), _react.default.createElement(_Control.default, this.props, _react.default.createElement("span", {
        ref: this.inputContainer
      }, _react.default.createElement(_Input.default, _extends({}, (0, _Input.propsObject)(this.props), {
        isDanger: isValid || this.props.forceInvalid,
        isDisabled: this.props.disabledInput,
        style: {
          width: 108,
          borderTopLeftRadius: 4,
          borderBottomLeftRadius: 4
        }
      })))), _react.default.createElement(_reactDatepicker.default, {
        dateFormat: "DD/MM/YYYY",
        minDate: this.props.minDate,
        maxDate: this.props.maxDate,
        selected: this.state.selected,
        customInput: _react.default.createElement(_Button.default, {
          type: "button",
          style: {
            borderTopLeftRadius: 0,
            borderBottomLeftRadius: 0
          },
          isStatic: true
        }, _react.default.createElement(_Icon.default, {
          name: "fas fa-calendar-alt"
        })),
        onChange: this.handleDateChanges.bind(this),
        className: "button is-featured",
        disabled: this.props.isDisabled,
        popperModifiers: {
          offset: {
            enabled: true,
            escapeWithReference: true,
            boundariesElement: 'viewport',
            offset: "".concat(this.inputContainer.current ? -this.inputContainer.current.getBoundingClientRect().width + 'px' : null, ", 0")
          }
        }
      }), this.props.forceInvalid && this.props.errorMessage ? _react.default.createElement("p", {
        className: "help is-danger"
      }, this.props.errorMessage) : null);
    }
  }, {
    key: "handleDateChangesInput",
    value: function handleDateChangesInput(date) {
      this.props.onChange({
        target: {
          value: date.target.value
        }
      });
    }
  }, {
    key: "handleDateChanges",
    value: function handleDateChanges(date) {
      console.log(date);
      this.setState({
        selected: date
      });
      this.props.onChange({
        target: {
          value: (0, _moment.default)(date).format('DD/MM/YYYY')
        }
      });
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(props, state) {
      if (props.value.length === 10 && state.selected && state.selected.format('DD/MM/YYYY') !== (0, _moment.default)(props.value, 'DD/MM/YYYY').format('DD/MM/YYYY')) {
        return {
          selected: (0, _moment.default)(props.value, 'DD/MM/YYYY')
        };
      }

      return null;
    }
  }]);

  return FormFieldDate;
}(_react.default.Component);

;

var _default = (0, _fieldCreator.default)(FormFieldDate, {
  masks: ['date'],
  validators: [{
    'minLength': 1
  }]
});

exports.default = _default;