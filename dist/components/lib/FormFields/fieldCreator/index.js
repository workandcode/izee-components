"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactTimeout = _interopRequireDefault(require("react-timeout"));

var _ValueParser = _interopRequireDefault(require("../../../../services/ValueParser"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var formFieldCreator = function formFieldCreator(Component) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  var FormFieldWrapper =
  /*#__PURE__*/
  function (_React$Component) {
    _inherits(FormFieldWrapper, _React$Component);

    function FormFieldWrapper(props) {
      var _this;

      _classCallCheck(this, FormFieldWrapper);

      _this = _possibleConstructorReturn(this, _getPrototypeOf(FormFieldWrapper).call(this, props));
      _this.state = {
        value: '',
        isEmpty: true,
        isValid: null,
        forceInvalid: false,
        wasFocused: false
      };
      _this.isTyping = false;
      return _this;
    }

    _createClass(FormFieldWrapper, [{
      key: "componentDidMount",
      value: function componentDidMount() {
        var defaultValue = this.props.valueDefault || '';

        if (options.valueType === 'boolean') {
          defaultValue = this.props.checkedDefault || false;
        }

        this.updateField(defaultValue);
        this.timer = null;
      }
    }, {
      key: "getSnapshotBeforeUpdate",
      value: function getSnapshotBeforeUpdate(prevProps) {
        // Set default values and clear inputs when receive empty values
        if (options.valueType === 'boolean') {
          if (prevProps.checked !== this.props.checked && !this.props.checked) {
            this.updateField(this.props.checked || false);
          }

          if (prevProps.checkedDefault !== this.props.checkedDefault && !this.props.checkedDefault) {
            this.updateField(this.props.checkedDefault || false);
          }

          return null;
        }

        if (prevProps.value !== this.props.value && !this.props.value) {
          this.updateField('');
          return;
        }

        if (prevProps.valueDefault !== this.props.valueDefault) {
          var defaultValue = this.props.valueDefault || '';
          this.updateField(defaultValue);
          return;
        }

        return null;
      }
    }, {
      key: "componentDidUpdate",
      value: function componentDidUpdate() {}
    }, {
      key: "render",
      value: function render() {
        return _react.default.createElement(Component, _extends({}, this.props, this.state, {
          onChange: this.handleFieldChanges.bind(this),
          onFocus: this.handleFieldFocus.bind(this),
          onKeyDown: this.handleKeyDown.bind(this),
          onKeyUp: this.handleKeyUp.bind(this)
        }));
      }
    }, {
      key: "handleFieldChanges",
      value: function handleFieldChanges(_ref) {
        var target = _ref.target;
        var value = target.value;
        if (target.type === 'checkbox') value = target.checked;
        this.updateField(value);
      }
    }, {
      key: "handleFieldFocus",
      value: function handleFieldFocus() {
        this.setState({
          wasFocused: true
        });
      }
    }, {
      key: "handleKeyDown",
      value: function handleKeyDown() {}
    }, {
      key: "handleKeyUp",
      value: function handleKeyUp() {
        this.props.clearTimeout(this.timer);
      }
    }, {
      key: "updateField",
      value: function updateField(value) {
        var _this2 = this;

        this.props.clearTimeout(this.timer);

        var parsedValue = _ValueParser.default.parse(value, {
          mask: this.props.mask || options.masks,
          validator: this.props.validator || options.validators
        });

        var isValid = (value || '').length > 0 ? parsedValue.isValid : null;

        if (this.props.noValidate) {
          parsedValue.isValid = null;
          isValid = null;
        }

        this.setState({
          value: parsedValue.value,
          isEmpty: parsedValue.isEmpty,
          isValid: null,
          wasFocused: this.state.wasFocused
        });
        parsedValue.wasFocused = this.state.wasFocused;
        this.timer = this.props.setTimeout(function () {
          _this2.setState({
            isValid: isValid
          });
        }, 1000); // if (!this.state.wasFocused && !value) return;

        if (typeof this.props.onChange === 'function') this.props.onChange(parsedValue.value, parsedValue);
      }
    }], [{
      key: "getDerivedStateFromProps",
      value: function getDerivedStateFromProps(props, state) {
        if (state.forceInvalid !== props.forceInvalid) {
          return {
            forceInvalid: props.forceInvalid
          };
        }

        return null;
      }
    }]);

    return FormFieldWrapper;
  }(_react.default.Component);

  ;
  FormFieldWrapper.displayName = "FormFieldWrapper(".concat(getDisplayName(Component), ")");
  return (0, _reactTimeout.default)(FormFieldWrapper);
};

function getDisplayName(Component) {
  return Component.displayName || Component.name || 'Component';
}

;
var _default = formFieldCreator;
exports.default = _default;