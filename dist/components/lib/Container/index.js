"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _parsePropsAsBulmaClasses = _interopRequireDefault(require("../_utils/parsePropsAsBulmaClasses"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Container = function Container(props) {
  var CSSClass = "container ".concat((0, _parsePropsAsBulmaClasses.default)(props));
  return _react.default.createElement("div", {
    className: CSSClass,
    style: props.style
  }, props.children);
};

var _default = Container;
exports.default = _default;