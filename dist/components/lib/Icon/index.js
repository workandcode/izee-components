"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _parsePropsAsBulmaClasses = _interopRequireDefault(require("../_utils/parsePropsAsBulmaClasses"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Icon = function Icon(props) {
  var iconCSSClass = "icon ".concat((0, _parsePropsAsBulmaClasses.default)(props));
  return _react.default.createElement("span", {
    className: iconCSSClass,
    style: props.style
  }, _react.default.createElement("i", {
    className: props.name
  }));
};

var _default = Icon;
exports.default = _default;