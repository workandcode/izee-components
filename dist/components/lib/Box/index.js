"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _parsePropsAsBulmaClasses = _interopRequireDefault(require("../_utils/parsePropsAsBulmaClasses"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Box = function Box(props) {
  var boxCSSClass = "box ".concat((0, _parsePropsAsBulmaClasses.default)(props));
  if (props.className) boxCSSClass = props.className;
  return _react.default.createElement("div", {
    className: boxCSSClass,
    "data-spec-selector": props.specSelector,
    style: {
      maxWidth: props.maxWidth
    }
  }, props.children);
};

var _default = Box;
exports.default = _default;