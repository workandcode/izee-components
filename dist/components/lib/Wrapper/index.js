"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _parsePropsAsBulmaClasses = _interopRequireDefault(require("../_utils/parsePropsAsBulmaClasses"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Wrapper = function Wrapper(props) {
  var wrapperCSSClass = "wrapper ".concat((0, _parsePropsAsBulmaClasses.default)(props));
  return _react.default.createElement("div", {
    className: wrapperCSSClass
  }, props.children);
};

Wrapper.propTypes = {
  children: _propTypes.default.oneOfType([_propTypes.default.element.isRequired, _propTypes.default.arrayOf(_propTypes.default.element).isRequired])
};
var _default = Wrapper;
exports.default = _default;