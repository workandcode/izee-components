"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _parsePropsAsBulmaClasses = _interopRequireDefault(require("../_utils/parsePropsAsBulmaClasses"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Layout = function Layout(props) {
  var layoutCSSClass = "layout ".concat((0, _parsePropsAsBulmaClasses.default)(props));
  return _react.default.createElement("div", {
    className: layoutCSSClass
  }, props.children);
};

Layout.Header = function (props) {
  var layoutHeaderCSSClass = "layout-header ".concat((0, _parsePropsAsBulmaClasses.default)(props));
  return _react.default.createElement("div", {
    className: layoutHeaderCSSClass
  }, props.children);
};

Layout.Aside = function (props) {
  var layoutHeaderCSSClass = "layout-aside ".concat((0, _parsePropsAsBulmaClasses.default)(props));
  return _react.default.createElement("aside", {
    className: layoutHeaderCSSClass
  }, props.children);
};

Layout.Body = function (props) {
  var layoutBodyCSSClass = "layout-body ".concat((0, _parsePropsAsBulmaClasses.default)(props));
  return _react.default.createElement("div", {
    className: layoutBodyCSSClass
  }, props.children);
};

Layout.Main = function (props) {
  var layoutMainCSSClass = "layout-main ".concat((0, _parsePropsAsBulmaClasses.default)(props));
  return _react.default.createElement("main", {
    className: layoutMainCSSClass,
    role: "main"
  }, props.children);
};

Layout.Footer = function (props) {
  var layoutFooterCSSClass = "layout-footer ".concat((0, _parsePropsAsBulmaClasses.default)(props));
  return _react.default.createElement("footer", {
    className: layoutFooterCSSClass,
    role: "contentinfo"
  }, props.children);
};

var _default = Layout;
exports.default = _default;