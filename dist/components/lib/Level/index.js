"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _parsePropsAsBulmaClasses = _interopRequireDefault(require("../_utils/parsePropsAsBulmaClasses"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Level = function Level(props) {
  var levelCSSClass = "level ".concat((0, _parsePropsAsBulmaClasses.default)(props));
  return _react.default.createElement("div", {
    className: levelCSSClass
  }, props.children);
};

Level.Item = function (props) {
  var levelCSSClass = "level-item ".concat((0, _parsePropsAsBulmaClasses.default)(props));
  return _react.default.createElement("div", {
    className: levelCSSClass
  }, _react.default.createElement("div", null, props.children));
};

Level.propTypes = {
  children: _propTypes.default.any.isRequired
};
var _default = Level;
exports.default = _default;