"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.withSplashScreen = withSplashScreen;
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactTimeout = _interopRequireDefault(require("react-timeout"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var defaultOptions = {
  time: 300,
  children: null,
  showSpinner: false,
  top: 0,
  left: 0
};

var SplashScreen = function SplashScreen(props) {
  return _react.default.createElement("div", {
    className: "splash-screen".concat(props.isVisible ? ' is-visible' : '').concat(props.showSpinner ? ' has-spinner' : ''),
    style: props.style
  }, props.children);
};

function withSplashScreen(Component, options) {
  var SplashScreenWrapper =
  /*#__PURE__*/
  function (_React$Component) {
    _inherits(SplashScreenWrapper, _React$Component);

    function SplashScreenWrapper(props) {
      var _this;

      _classCallCheck(this, SplashScreenWrapper);

      _this = _possibleConstructorReturn(this, _getPrototypeOf(SplashScreenWrapper).call(this, props));
      _this.state = {
        isVisible: true
      };
      _this.options = _objectSpread({}, defaultOptions, options);
      return _this;
    }

    _createClass(SplashScreenWrapper, [{
      key: "componentDidMount",
      value: function componentDidMount() {
        var _this2 = this;

        this.props.setTimeout(function () {
          _this2.setState({
            isVisible: false
          });
        }, this.options.time);
      }
    }, {
      key: "componentWillUnmount",
      value: function componentWillUnmount() {
        this.setState({
          isVisible: true
        });
      }
    }, {
      key: "render",
      value: function render() {
        return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(SplashScreen, {
          isVisible: this.state.isVisible,
          showSpinner: this.options.showSpinner,
          style: {
            marginTop: this.options.top,
            marginLeft: this.options.left
          }
        }, this.options.children), _react.default.createElement(Component, this.props));
      }
    }]);

    return SplashScreenWrapper;
  }(_react.default.Component);

  ;
  SplashScreenWrapper.displayName = "SplashScreenWrapper(".concat(getDisplayName(Component), ")");
  return (0, _reactTimeout.default)(SplashScreenWrapper);
}

;

function getDisplayName(Component) {
  return Component.displayName || Component.name || 'Component';
}

;
var _default = SplashScreen;
exports.default = _default;