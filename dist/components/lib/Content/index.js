"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _utils = require("../../../utils/utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Content = function Content(props) {
  return _react.default.createElement("div", {
    className: "content-wrapper"
  }, props.children);
};

Content.Header = function (props) {
  return _react.default.createElement("header", {
    className: "content-header"
  }, props.children);
};

Content.Body = function (props) {
  return _react.default.createElement("div", {
    className: 'content-body ' + props.className
  }, props.children);
};

Content.propTypes = {
  children: _propTypes.default.any.isRequired
};
var _default = Content;
exports.default = _default;