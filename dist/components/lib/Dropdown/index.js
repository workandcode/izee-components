"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _parsePropsAsBulmaClasses = _interopRequireDefault(require("../_utils/parsePropsAsBulmaClasses"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var Dropdown =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Dropdown, _React$Component);

  function Dropdown() {
    var _this;

    _classCallCheck(this, Dropdown);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Dropdown).call(this));
    _this.state = {
      isActive: false
    };
    _this.container = _react.default.createRef();
    return _this;
  }

  _createClass(Dropdown, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      document.addEventListener('click', this.handleClickOutside.bind(this), true);
    }
  }, {
    key: "componentWillUnmount",
    value: function componentWillUnmount() {
      document.removeEventListener('click', this.handleClickOutside.bind(this), true);
    }
  }, {
    key: "render",
    value: function render() {
      var dropdownCSSClass = "dropdown ".concat(this.state.isActive ? ' is-active' : '').concat(' ' + (0, _parsePropsAsBulmaClasses.default)(this.props));
      return _react.default.createElement("div", {
        className: dropdownCSSClass,
        ref: this.container
      }, _react.default.createElement("div", {
        className: "dropdown-trigger is-featured",
        onClick: this.toggleDropdown.bind(this),
        tabIndex: "0",
        role: "button",
        onKeyPress: null
      }, this.props.trigger), _react.default.createElement("div", {
        className: "dropdown-menu",
        role: "menu"
      }, _react.default.createElement("div", {
        className: "dropdown-content",
        onClick: this.toggleDropdown.bind(this),
        tabIndex: "0",
        role: "button",
        onKeyPress: null
      }, this.props.children)));
    }
  }, {
    key: "handleClickOutside",
    value: function handleClickOutside(event) {
      var container = this.container.current;
      if (container && !container.contains(event.target)) this.closeDropdown();
    }
  }, {
    key: "toggleDropdown",
    value: function toggleDropdown() {
      this.setState({
        isActive: !this.state.isActive
      });
    }
  }, {
    key: "closeDropdown",
    value: function closeDropdown() {
      if (this.state.isActive) this.setState({
        isActive: false
      });
    }
  }]);

  return Dropdown;
}(_react.default.Component);

exports.default = Dropdown;
;

Dropdown.Item = function (props) {
  var dropdownItemCSSClass = "dropdown-item ".concat((0, _parsePropsAsBulmaClasses.default)(props));
  return _react.default.createElement(_react.default.Fragment, null, props.children ? _react.default.createElement("span", {
    onClick: props.onClick,
    onKeyDown: props.onClick,
    tabIndex: props.onClick ? '0' : '-1',
    role: "button",
    className: dropdownItemCSSClass
  }, props.children) : null, props.hasDivider ? _react.default.createElement("hr", {
    className: "dropdown-divider"
  }) : null);
};

Dropdown.propTypes = {
  children: _propTypes.default.any.isRequired,
  trigger: _propTypes.default.any.isRequired
};