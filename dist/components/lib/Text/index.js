"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _parsePropsAsBulmaClasses = _interopRequireDefault(require("../_utils/parsePropsAsBulmaClasses"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Text = function Text(props) {
  var textCSSClass = "text ".concat((0, _parsePropsAsBulmaClasses.default)(props));
  return _react.default.createElement("p", {
    className: textCSSClass,
    style: props.style
  }, props.children);
};

Text.propTypes = {
  children: _propTypes.default.any
};
var _default = Text;
exports.default = _default;