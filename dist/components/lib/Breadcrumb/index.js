"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Breadcrumb = function Breadcrumb(props) {
  var breadcrumbItems = props.path.map(function (item, index, array) {
    return _react.default.createElement("li", {
      className: index === array.length ? ' is-active' : '',
      key: index
    }, _react.default.createElement("a", {
      onClick: props.onClick.bind(null, item)
    }, item));
  });
  return _react.default.createElement("nav", {
    className: "breadcrumb",
    "aria-label": "breadcrumbs"
  }, _react.default.createElement("ul", null, breadcrumbItems));
};

Breadcrumb.propTypes = {
  onClick: _propTypes.default.func,
  path: _propTypes.default.array
};
Breadcrumb.defaultProps = {
  onClick: function onClick() {},
  path: []
};
var _default = Breadcrumb;
exports.default = _default;