"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _parsePropsAsBulmaClasses = _interopRequireDefault(require("../_utils/parsePropsAsBulmaClasses"));

var _Title = _interopRequireDefault(require("../Title"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Header = function Header(props) {
  var headerCSSClass = "header ".concat((0, _parsePropsAsBulmaClasses.default)(props));
  return _react.default.createElement("header", {
    className: headerCSSClass
  }, _react.default.createElement(_Title.default, {
    isH1: true
  }, props.children), props.subtitle ? _react.default.createElement(_Title.default, {
    isH2: true
  }, props.subtitle) : null);
};

Header.defaultProps = {
  children: null
};
Header.propTypes = {
  children: _propTypes.default.any
};
var _default = Header;
exports.default = _default;