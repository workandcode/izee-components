"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _Header = _interopRequireDefault(require("../lib/Header"));

var _Container = _interopRequireDefault(require("../lib/Container"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SectionWrapper = function SectionWrapper(props) {
  return _react.default.createElement("section", {
    className: "section",
    style: props.style
  }, _react.default.createElement(_Container.default, {
    isMedium: !props.isMedium
  }, props.header ? _react.default.createElement(_Header.default, {
    isTiny: true,
    isUppercase: true,
    subtitle: _react.default.createElement("hr", {
      style: {
        marginTop: '1rem'
      }
    })
  }, props.header, _react.default.createElement("div", null, props.headerRight)) : null, props.children));
};

var _default = SectionWrapper;
exports.default = _default;