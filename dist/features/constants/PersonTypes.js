"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NATURAL_PERSON = exports.LEGAL_PERSON = void 0;
var LEGAL_PERSON = "Pessoa Jurídica";
exports.LEGAL_PERSON = LEGAL_PERSON;
var NATURAL_PERSON = "Pessoa Física";
exports.NATURAL_PERSON = NATURAL_PERSON;