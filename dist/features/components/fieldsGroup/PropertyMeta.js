"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _FormFields = require("../../../components/lib/FormFields");

var _Grid = _interopRequireDefault(require("../../../components/lib/Grid"));

var _formCreator = require("../formCreator");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var PropertyMeta =
/*#__PURE__*/
function (_React$Component) {
  _inherits(PropertyMeta, _React$Component);

  function PropertyMeta(props) {
    var _this;

    _classCallCheck(this, PropertyMeta);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(PropertyMeta).call(this, props));
    _this.state = {
      selectedType: _this.props.defaultValue && _this.props.defaultValue.type ? _this.props.defaultValue.type.id : null
    };
    return _this;
  }

  _createClass(PropertyMeta, [{
    key: "render",
    value: function render() {
      var defaultType = this.props.defaultValue && this.props.defaultValue.type ? this.props.defaultValue.type.id : null;
      var type = this.state.selectedType || defaultType;
      var condoFee = type == 2 || type == 3 || type == 4;
      var parking = type == 2 || type == 4;
      var hobbyBoxes = type == 2 || type == 4;
      return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldSelect, _extends({}, (0, _formCreator.fieldProps)(this.props, 'type'), {
        label: "Tipo de im\xF3vel",
        onChange: this.handleTypeChange.bind(this),
        value: type,
        valueKey: "id",
        options: [{
          id: 1,
          code: 'TYPE_HOUSE',
          name: 'Casa'
        }, {
          id: 2,
          code: 'TYPE_APARTMENT',
          name: 'Apartamento'
        }, {
          id: 3,
          code: 'TYPE_CONDO_HOUSE',
          name: 'Casa em condomínio'
        }, {
          id: 4,
          code: 'TYPE_PENTHOUSE',
          name: 'Cobertura'
        }]
      }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'area'), {
        label: "\xC1rea privativa (m\xB2)",
        mask: ['onlyNumbers']
      }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'iptu'), {
        label: "IPTU (Aproximado)",
        mask: ['number']
      }))), _react.default.createElement(_Grid.default.Col, null, condoFee && _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'condo_fee'), {
        label: "Condom\xEDnio (Mensal aproximado)",
        mask: ['number']
      })))), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, parking && _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'parking_spaces'), {
        label: "Vaga de garagem"
      }))), _react.default.createElement(_Grid.default.Col, null, hobbyBoxes && _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'hobby_boxes'), {
        label: "Hobby box"
      })))));
    }
  }, {
    key: "handleTypeChange",
    value: function handleTypeChange(value, field) {
      if (!field.wasFocused) return;
      this.setState({
        selectedType: value
      });
      this.props.handleFieldChanges('type_id', value, field);
    }
  }]);

  return PropertyMeta;
}(_react.default.Component);

exports.default = PropertyMeta;
;