"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactTimeout = _interopRequireDefault(require("react-timeout"));

var _Async = _interopRequireDefault(require("react-select/lib/Async"));

var _reactSelect = require("react-select");

var _Field = _interopRequireDefault(require("../../../components/lib/FormFields/components/Field"));

var _Control = _interopRequireDefault(require("../../../components/lib/FormFields/components/Control"));

var _Icon = _interopRequireDefault(require("../../../components/lib/Icon"));

var _Text = _interopRequireDefault(require("../../../components/lib/Text"));

var _constants = require("core/constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var PropertySelection =
/*#__PURE__*/
function (_React$Component) {
  _inherits(PropertySelection, _React$Component);

  function PropertySelection(props) {
    var _this;

    _classCallCheck(this, PropertySelection);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(PropertySelection).call(this, props));
    _this.state = {
      inputValue: '',
      options: (_this.props.defaultValue || {}).properties || [],
      isWorking: false
    };
    _this.asyncRef = _react.default.createRef();
    return _this;
  }

  _createClass(PropertySelection, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if ((this.props.defaultValue || {}).properties) {
        var defaultOptions = (this.props.defaultValue || {}).properties[0];
        this.props.handleFieldChanges('property_ids', [defaultOptions.id], {
          isValid: [defaultOptions.id].length > 0,
          value: [defaultOptions.id]
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var promiseOptions = function promiseOptions(inputValue) {
        return new Promise(function (resolve, reject) {
          _constants.request.get('{company}/property?search=' + inputValue).then(function (response) {
            resolve(response.data);
          }).catch(function () {
            return reject();
          });
        });
      };

      var DropdownIndicator = function DropdownIndicator(props) {
        return _react.default.createElement(_reactSelect.components.DropdownIndicator, props, _react.default.createElement(_Icon.default, {
          name: "fas fa-home"
        }));
      };

      var Option = function Option(props) {
        var address = props.data.address;
        var data = props.data || {};
        return _react.default.createElement(_reactSelect.components.Option, props, _react.default.createElement(_Text.default, null, _react.default.createElement("strong", null, data.name || data.code, data.name && data.code ? " (".concat(data.code, ")") : '')), address ? _react.default.createElement(_Text.default, {
          isSmall: true
        }, address.address, ", ", address.neighborhood, " - ", address.city, " / ", address.state) : null);
      };

      var SingleValue = function SingleValue(props) {
        var address = props.data.address;
        var data = props.data || {};
        return _react.default.createElement(_reactSelect.components.SingleValue, props, address ? _react.default.createElement(_Text.default, null, address.address, ", ", address.neighborhood, " - ", address.city, " / ", address.state) : 'Sem endereço');
      };

      var LoadingIndicator = function LoadingIndicator(props) {
        return null;
      };

      return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_Field.default, {
        label: "Selecione um im\xF3vel"
      }, _react.default.createElement(_Control.default, null, _react.default.createElement("div", {
        className: "react-select-wrapper"
      }, _react.default.createElement(_Async.default, {
        defaultOptions: true,
        ref: function ref(_ref) {
          return _this2.asyncRef = _ref;
        },
        classNamePrefix: "react-select",
        defaultValue: [],
        name: "property",
        isDisabled: this.state.isWorking,
        isLoading: this.state.isWorking,
        value: this.state.options,
        loadingMessage: function loadingMessage() {
          return 'Pesquisando por imóveis...';
        },
        noOptionsMessage: function noOptionsMessage() {
          return _react.default.createElement("span", null, "Nenhum im\xF3vel encontrado");
        },
        placeholder: "",
        menuPosition: "fixed",
        maxMenuHeight: 168,
        minMenuHeight: 100,
        onInputChange: this.handleInputChange.bind(this),
        inputValue: this.state.inputValue,
        onChange: function onChange(options) {
          _this2.setState({
            options: options,
            inputValue: ''
          });

          if (_this2.props.onSelect) _this2.props.onSelect(options);

          _this2.props.handleFieldChanges('property_ids', [options.id], {
            isValid: [options.id].length > 0,
            value: [options.id]
          });
        },
        getOptionLabel: function getOptionLabel(option) {
          return option.name;
        },
        getOptionValue: function getOptionValue(option) {
          return option.id;
        },
        components: {
          SingleValue: SingleValue,
          DropdownIndicator: DropdownIndicator,
          LoadingIndicator: LoadingIndicator,
          Option: Option
        },
        loadOptions: promiseOptions
      })))));
    }
  }, {
    key: "handleInputChange",
    value: function handleInputChange(inputValue) {
      this.setState({
        inputValue: inputValue
      });
      return inputValue;
    }
  }]);

  return PropertySelection;
}(_react.default.Component);

;

var _default = (0, _reactTimeout.default)(PropertySelection);

exports.default = _default;