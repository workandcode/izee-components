"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactQuill = _interopRequireWildcard(require("react-quill"));

require("react-quill/dist/quill.snow.css");

var _FormFields = require("../../../components/lib/FormFields");

var _Grid = _interopRequireDefault(require("../../../components/lib/Grid"));

var _Text = _interopRequireDefault(require("../../../components/lib/Text"));

var _formCreator = require("../formCreator");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var ContractEditor =
/*#__PURE__*/
function (_React$Component) {
  _inherits(ContractEditor, _React$Component);

  function ContractEditor(props) {
    var _this;

    _classCallCheck(this, ContractEditor);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(ContractEditor).call(this, props));
    _this.state = {
      editorState: _this.props.defaultValue['template'] || ''
    };
    _this.editorRef = null;

    var AlignStyle = _reactQuill.Quill.import('attributors/style/align');

    var BackgroundStyle = _reactQuill.Quill.import('attributors/style/background');

    var ColorStyle = _reactQuill.Quill.import('attributors/style/color');

    var DirectionStyle = _reactQuill.Quill.import('attributors/style/direction');

    var FontStyle = _reactQuill.Quill.import('attributors/style/font');

    var SizeStyle = _reactQuill.Quill.import('attributors/style/size');

    _reactQuill.Quill.register(AlignStyle, true);

    _reactQuill.Quill.register(BackgroundStyle, true);

    _reactQuill.Quill.register(ColorStyle, true);

    _reactQuill.Quill.register(DirectionStyle, true);

    _reactQuill.Quill.register(FontStyle, true);

    _reactQuill.Quill.register(SizeStyle, true);

    _this.modules = {
      toolbar: [['bold', 'italic', 'underline'], [{
        'background': []
      }], [{
        'header': 1
      }, {
        'header': 2
      }], [{
        'indent': '-1'
      }, {
        'indent': '+1'
      }], [{
        'align': []
      }], [{
        'size': ['small', false, 'large']
      }], [{
        'font': []
      }]]
    };
    _this.formats = ['header', 'font', 'background', 'color', 'size', 'bold', 'italic', 'underline', 'strike', 'list', 'bullet', 'indent', 'script', 'align'];
    return _this;
  }

  _createClass(ContractEditor, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.props.handleFieldChanges('template', this.state.editorState, {
        isValid: this.state.editorState.length > 0,
        value: this.state.editorState
      });
    }
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'name'), {
        label: "Modelo",
        info: "D\xEA um nome ao modelo do contrato"
      })))), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_reactQuill.default, {
        theme: "snow",
        tabIndex: 0,
        ref: function ref(editor) {
          return _this2.editorRef = editor;
        },
        value: this.state.editorState,
        placeholder: '',
        onChange: this.handleEditorChanges.bind(this),
        modules: this.modules,
        formats: this.formats
      }))), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldSelect, _extends({}, (0, _formCreator.fieldProps)(this.props, 'guarantee_id'), {
        label: "Garantia associada",
        info: "Seleciona o tipo de garantia em que este contrato dever\xE1 ser utilizado",
        valueKey: 'id',
        options: this.props.activeCompany.guarantees
      })))), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldTextArea, {
        label: "Tokens",
        info: "Copie e cole os tokens para substituir por dados reais no final do processo de loca\xE7\xE3o",
        valueDefault: "{!! $PROPRIETARIOS !!}\n{!! $LOCATARIOS !!}\n{!! $FIADORES !!}\n{!! $IMOVEL !!}\n{!! $DATA_POSSE !!}\n{!! $DATA_POSSE_EXT !!}\n{!! $DATA_FIM_VIGENCIA !!}\n{!! $DATA_FIM_VIGENCIA_EXT !!}\n{!! $VALOR_ALUGUEL !!}\n{!! $VALOR_ALUGUEL_EXT !!}\n{!! $VALOR_CAPITALIZACAO !!}\n{!! $VALOR_CAPITALIZACAO_EXT !!}\n{!! $PROTOCOLO_CAPITALIZACAO !!}\n{!! $CLAUSULA_ADICIONAL !!}\n{!! $ASSINATURAS !!}",
        readOnly: true,
        isDisabled: true,
        style: {
          height: 335
        }
      }))));
    }
  }, {
    key: "handleEditorChanges",
    value: function handleEditorChanges(editorState) {
      this.setState({
        editorState: editorState
      });
      this.props.handleFieldChanges('template', editorState, {
        isValid: editorState.length > 0,
        value: editorState
      });
    }
  }]);

  return ContractEditor;
}(_react.default.Component);

exports.default = ContractEditor;
;