"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _axios = _interopRequireDefault(require("axios"));

var _FormFields = require("../../../components/lib/FormFields");

var _formCreator = require("../formCreator");

var _Grid = _interopRequireDefault(require("../../../components/lib/Grid"));

var _Space = _interopRequireDefault(require("../../../components/lib/Space"));

var _Header = _interopRequireDefault(require("../../../components/lib/Header"));

var _utils = require("../../../utils/utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var ProfileSelection =
/*#__PURE__*/
function (_React$Component) {
  _inherits(ProfileSelection, _React$Component);

  function ProfileSelection(props) {
    var _this;

    _classCallCheck(this, ProfileSelection);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(ProfileSelection).call(this, props));
    _this.state = {
      currentProfiles: _this.props.defaultValue.profiles || []
    };
    return _this;
  }

  _createClass(ProfileSelection, [{
    key: "render",
    value: function render() {
      var _this2 = this;

      var availableProfiles = [{
        id: 3,
        name: 'Administrador(a)',
        info: 'Tem os mesmo poderes que o proprietário da conta'
      }, {
        id: 4,
        name: 'Gestor(a)',
        info: 'Pode apenas visualizar tudo do sistema'
      }, {
        id: 5,
        name: 'Consultor(a)',
        info: 'Cria propostas'
      }, {
        id: 6,
        name: 'Vistoriador(a)',
        info: 'Agenda e anexa o documento de vistoria da proposta'
      }, {
        id: 7,
        name: 'Jurídico',
        info: 'Edita o contrato de locação'
      }];
      return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_Space.default, null), _react.default.createElement(_Header.default, {
        isTiny: true,
        isUppercase: true,
        subtitle: _react.default.createElement("hr", {
          style: {
            marginTop: '1rem'
          }
        })
      }, "Pap\xE9is na empresa"), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, availableProfiles.map(function (item, index) {
        var isChecked = _this2.state.currentProfiles.filter(function (profile) {
          return profile.id === item.id;
        }).length;

        return _react.default.createElement(_FormFields.FormFieldCheckbox, {
          key: item.id,
          name: item.name,
          isDisabled: _this2.props.formIsWorking,
          onChange: _this2.handleCheckboxChanges.bind(_this2, item, index),
          description: item.name,
          info: item.info,
          checked: isChecked
        });
      }))));
    }
  }, {
    key: "handleCheckboxChanges",
    value: function handleCheckboxChanges(item, index, value, field) {
      // if (!field.wasFocused) return;
      if (value) {
        this.push(item, index);
      } else {
        this.remove(item, index);
      } // console.log(this.state.currentProfiles.length);


      this.props.handleFieldChanges('profile_ids', _toConsumableArray(this.state.currentProfiles).map(function (item) {
        return item.id;
      }), {
        isValid: this.state.currentProfiles.length > 0,
        value: _toConsumableArray(this.state.currentProfiles).map(function (item) {
          return item.id;
        })
      });
    }
  }, {
    key: "push",
    value: function push(item, index) {
      var list = this.state.currentProfiles;
      if (list.filter(function (listItem) {
        return listItem.id === item.id;
      }).length) return;
      list.push(item);
      this.setState({
        currentProfiles: list
      });
    }
  }, {
    key: "remove",
    value: function remove(item, index) {
      var list = this.state.currentProfiles;

      if (list.length === 1) {
        this.setState({
          currentProfiles: list
        });
        return;
      }

      list = list.filter(function (listItem) {
        return listItem.id !== item.id;
      });
      this.setState({
        currentProfiles: list
      });
    }
  }], [{
    key: "getDerivedStateFromProps",
    value: function getDerivedStateFromProps(props, state) {
      if (!props.fields.profile_ids && state.currentProfiles.length > 0 && !props.defaultValue.profiles) {
        return {
          currentProfiles: []
        };
      }

      return null;
    }
  }]);

  return ProfileSelection;
}(_react.default.Component);

exports.default = ProfileSelection;
;