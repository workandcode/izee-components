"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _SectionWrapper = _interopRequireDefault(require("../../../components/SectionWrapper"));

var _FormFields = require("../../../components/lib/FormFields");

var _Grid = _interopRequireDefault(require("../../../components/lib/Grid"));

var _formCreator = require("../formCreator");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

var PersonalDataSpouse =
/*#__PURE__*/
function (_React$Component) {
  _inherits(PersonalDataSpouse, _React$Component);

  function PersonalDataSpouse(props) {
    var _this;

    _classCallCheck(this, PersonalDataSpouse);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(PersonalDataSpouse).call(this, props));
    _this.handleSpouseStatus = _this.handleSpouseStatus.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.state = {
      spouse_status: false
    };
    return _this;
  }

  _createClass(PersonalDataSpouse, [{
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      var _this$props = this.props,
          defaultValue = _this$props.defaultValue,
          fields = _this$props.fields; // check spouse_status

      var spouse_status = fields.spouse_cpf ? fields.spouse_cpf.value : defaultValue.spouse_cpf ? true : false;

      if (!this.state.spouse_status && spouse_status) {
        this.setState({
          spouse_status: spouse_status
        });
      }
    }
  }, {
    key: "handleSpouseStatus",
    value: function handleSpouseStatus() {
      var _this$props2 = this.props,
          defaultValue = _this$props2.defaultValue,
          fields = _this$props2.fields;
      var spouse_status = this.state.spouse_status;
      this.setState({
        spouse_status: !spouse_status
      });

      if (spouse_status) {
        this.props.updateFieldValues([{
          fieldName: 'spouse_cpf',
          field: {
            value: "",
            isValid: true
          }
        }, {
          fieldName: 'spouse_birth_date',
          field: {
            value: "",
            isValid: true
          }
        }, {
          fieldName: 'spouse_email',
          field: {
            value: "",
            isValid: true
          }
        }, {
          fieldName: 'spouse_identity',
          field: {
            value: "",
            isValid: true
          }
        }, {
          fieldName: 'spouse_identity_issuing_body',
          field: {
            value: "",
            isValid: true
          }
        }, {
          fieldName: 'spouse_identity_type',
          field: {
            value: "",
            isValid: true
          }
        }, {
          fieldName: 'spouse_name',
          field: {
            value: "",
            isValid: true
          }
        }, {
          fieldName: 'spouse_nationality',
          field: {
            value: "",
            isValid: true
          }
        }, {
          fieldName: 'spouse_occupation',
          field: {
            value: "",
            isValid: true
          }
        }, {
          fieldName: 'spouse_phone',
          field: {
            value: "",
            isValid: true
          }
        }]);
      }
    }
  }, {
    key: "render",
    value: function render() {
      var spouse_status = this.state.spouse_status;
      var props = this.props;
      var fields = this.props.fields;
      return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_FormFields.FormFieldSwitch, {
        type: "checkbox",
        description: "Tem c\xF4njuge?",
        checked: spouse_status,
        onClick: this.handleSpouseStatus ? this.handleSpouseStatus : null
      }), spouse_status ? _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'spouse_name'), {
        label: "Nome",
        noValidate: true
      }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'spouse_email'), {
        label: "Email",
        noValidate: true
      }))), _react.default.createElement(_Grid.default.Col, {
        isNarrow: true
      }, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'spouse_phone'), {
        label: "Telefone",
        mask: ['phone'],
        style: {
          width: 150
        },
        noValidate: true
      })))), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, {
        isNarrow: true
      }, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'spouse_cpf'), {
        label: "CPF",
        mask: ['cpf'],
        noValidate: true
      }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldSelect, _extends({}, (0, _formCreator.fieldProps)(this.props, 'spouse_identity_type'), {
        label: "Documento",
        valueKey: "name",
        options: [{
          id: 1,
          code: 'DOCUMENT_TYPE_RG',
          name: 'RG'
        }, {
          id: 2,
          code: 'DOCUMENT_TYPE_CNH',
          name: 'CNH'
        }, {
          id: 3,
          code: 'DOCUMENT_TYPE_PASSPORT',
          name: 'Passaporte'
        }, {
          id: 4,
          code: 'DOCUMENT_TYPE_OTHER',
          name: 'Outro documento'
        }],
        noValidate: true
      }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'spouse_identity'), {
        label: "N\xFAmero do documento",
        mask: [{
          maxLength: 15
        }],
        noValidate: true
      }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'spouse_identity_issuing_body'), {
        label: "Org\xE3o emissor",
        noValidate: true
      })))), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, {
        isNarrow: true
      }, _react.default.createElement(_FormFields.FormFieldDate, _extends({}, (0, _formCreator.fieldProps)(this.props, 'spouse_birth_date'), {
        label: "Data de nascimento",
        noValidate: true
      }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'spouse_nationality'), {
        label: "Nacionalidade",
        noValidate: true
      }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'spouse_occupation'), {
        label: "Ocupa\xE7\xE3o",
        noValidate: true
      }))))) : null);
    }
  }]);

  return PersonalDataSpouse;
}(_react.default.Component);

var _default = PersonalDataSpouse;
exports.default = _default;