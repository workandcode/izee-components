"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _FormFields = require("../../../components/lib/FormFields");

var _FilePreview = _interopRequireDefault(require("../../../components/lib/FilePreview"));

var _Gravatar = _interopRequireDefault(require("../../../components/lib/Gravatar"));

var _Grid = _interopRequireDefault(require("../../../components/lib/Grid"));

var _Space = _interopRequireDefault(require("../../../components/lib/Space"));

var _Icon = _interopRequireDefault(require("../../../components/lib/Icon"));

var _formCreator = require("../formCreator");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var UserProfile = function UserProfile(props) {
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_Grid.default, {
    isCentered: true
  }, _react.default.createElement(_Grid.default.Col, {
    isNarrow: true
  }, _react.default.createElement(_FormFields.FormFieldFile, _extends({
    label: "Imagem do perfil"
  }, (0, _formCreator.fieldProps)(props, 'avatar'), {
    accept: "image/*",
    render: function render(fileProps) {
      if (fileProps.files[0]) {
        var file = fileProps.files[0];
        return _react.default.createElement(_FilePreview.default, {
          isRounded: true,
          source: file.preview(),
          filename: file.name,
          fallback: _react.default.createElement(_Icon.default, {
            name: "fas fa-file"
          }),
          ext: file.ext
        });
      }

      if (!props.defaultValue['avatar']) {
        return _react.default.createElement("figure", {
          className: "image is-128px has-border-rounded"
        }, _react.default.createElement(_Gravatar.default, {
          email: props.defaultValue['email'],
          alt: props.defaultValue['name'] || props.defaultValue['email']
        }));
      }

      ;

      if (props.defaultValue['avatar']) {
        return _react.default.createElement("figure", {
          className: "image is-128px has-border-rounded"
        }, _react.default.createElement("img", {
          src: props.defaultValue['avatar'],
          alt: ""
        }));
      }

      ;
    }
  }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, (0, _formCreator.fieldProps)(props, 'name')))), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(props, 'email'), {
    isDisabled: true
  })))))), _react.default.createElement(_Space.default, null));
};

var _default = UserProfile;
exports.default = _default;