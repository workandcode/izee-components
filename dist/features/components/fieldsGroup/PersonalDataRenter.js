"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _FormFields = require("../../../components/lib/FormFields");

var _formCreator = require("../formCreator");

var _Grid = _interopRequireDefault(require("../../../components/lib/Grid"));

var _SectionWrapper = _interopRequireDefault(require("../../../components/SectionWrapper"));

var _Button = _interopRequireDefault(require("../../../components/lib/Button"));

var _Icon = _interopRequireDefault(require("../../../components/lib/Icon"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var PersonalData =
/*#__PURE__*/
function (_React$Component) {
  _inherits(PersonalData, _React$Component);

  function PersonalData(props) {
    var _this;

    _classCallCheck(this, PersonalData);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(PersonalData).call(this, props));
    _this.state = {
      showSpouse: false
    };
    return _this;
  }

  _createClass(PersonalData, [{
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      var _this$props$fields = this.props.fields,
          _this$props$fields$sp = _this$props$fields.spouse_name,
          spouse_name = _this$props$fields$sp === void 0 ? {
        value: ""
      } : _this$props$fields$sp,
          _this$props$fields$sp2 = _this$props$fields.spouse_email,
          spouse_email = _this$props$fields$sp2 === void 0 ? {
        value: ""
      } : _this$props$fields$sp2;

      if (!this.state.showSpouse && spouse_name.value !== "" && spouse_email.value !== "") {
        this.setState({
          showSpouse: true
        });
      }
    }
  }, {
    key: "showSpouse",
    value: function showSpouse() {
      this.setState({
        showSpouse: true
      }, this.props.updateFieldValues([{
        fieldName: 'spouse_name',
        field: _objectSpread({}, this.props.fields.spouse_name)
      }, {
        fieldName: 'spouse_email',
        field: _objectSpread({}, this.props.fields.spouse_email)
      }]));
    }
  }, {
    key: "hideSpouse",
    value: function hideSpouse() {
      this.setState({
        showSpouse: false
      }, this.props.updateFieldValues([{
        fieldName: 'spouse_name',
        field: _objectSpread({}, this.props.fields.spouse_name, {
          value: "",
          isValid: null
        })
      }, {
        fieldName: 'spouse_email',
        field: _objectSpread({}, this.props.fields.spouse_email, {
          value: "",
          isValid: null
        })
      }, {
        fieldName: 'spouse_phone',
        field: _objectSpread({}, this.props.fields.spouse_phone, {
          value: ""
        })
      }, {
        fieldName: 'spouse_birth_date',
        field: _objectSpread({}, this.props.fields.spouse_birth_date, {
          value: ""
        })
      }, {
        fieldName: 'spouse_occupation',
        field: _objectSpread({}, this.props.fields.spouse_occupation, {
          value: ""
        })
      }, {
        fieldName: 'spouse_nationality',
        field: _objectSpread({}, this.props.fields.spouse_nationality, {
          value: ""
        })
      }, {
        fieldName: 'spouse_identity_type',
        field: _objectSpread({}, this.props.fields.spouse_identity_type, {
          value: ""
        })
      }, {
        fieldName: 'spouse_identity',
        field: _objectSpread({}, this.props.fields.spouse_identity, {
          value: ""
        })
      }, {
        fieldName: 'spouse_identity_issuing_body',
        field: _objectSpread({}, this.props.fields.spouse_identity_issuing_body, {
          value: ""
        })
      }, {
        fieldName: 'spouse_cpf',
        field: _objectSpread({}, this.props.fields.spouse_cpf, {
          value: ""
        })
      }]));
    }
  }, {
    key: "render",
    value: function render() {
      var showSpouse = this.state.showSpouse;
      var fields = this.props.fields;
      var spouse_active = fields.spouse_active;
      return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'name'), {
        label: "Nome"
      }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'email'), {
        isDisabled: this.props.disableEmail,
        label: "Email"
      }))), _react.default.createElement(_Grid.default.Col, {
        isNarrow: true
      }, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'phone'), {
        label: "Telefone",
        mask: ['phone'],
        style: {
          width: 150
        },
        noValidate: true
      })))), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, {
        isNarrow: true
      }, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'cpf'), {
        label: "CPF",
        mask: ['cpf'],
        noValidate: this.props.noValidate
      }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldSelect, _extends({}, (0, _formCreator.fieldProps)(this.props, 'identity_type'), {
        label: "Documento",
        valueKey: "name",
        options: [{
          id: 1,
          code: 'DOCUMENT_TYPE_RG',
          name: 'RG'
        }, {
          id: 2,
          code: 'DOCUMENT_TYPE_CNH',
          name: 'CNH'
        }, {
          id: 3,
          code: 'DOCUMENT_TYPE_PASSPORT',
          name: 'Passaporte'
        }, {
          id: 4,
          code: 'DOCUMENT_TYPE_OTHER',
          name: 'Outro documento'
        }],
        noValidate: true
      }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'identity'), {
        label: "N\xFAmero do documento",
        mask: [{
          maxLength: 15
        }],
        noValidate: true
      }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'identity_issuing_body'), {
        label: "Org\xE3o emissor",
        noValidate: true
      })))), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, {
        isNarrow: true
      }, _react.default.createElement(_FormFields.FormFieldDate, _extends({}, (0, _formCreator.fieldProps)(this.props, 'birth_date'), {
        label: "Data de nascimento",
        noValidate: true
      }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'nationality'), {
        label: "Nacionalidade",
        noValidate: true
      }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldSelect, _extends({}, (0, _formCreator.fieldProps)(this.props, 'marital_status'), {
        label: "Estado civil",
        valueKey: "id",
        options: [{
          id: 1,
          code: 'MARITAL_STATUS_SINGLE',
          name: 'Solteiro(a)'
        }, {
          id: 2,
          code: 'MARITAL_STATUS_MARRIED',
          name: 'Casado(a)'
        }, {
          id: 3,
          code: 'MARITAL_STATUS_DIVORCED',
          name: 'Divorciado(a)'
        }, {
          id: 4,
          code: 'MARITAL_STATUS_STABLE_UNION',
          name: 'União Estável'
        }, {
          id: 5,
          code: 'MARITAL_STATUS_WIDOWED',
          name: 'Viúvo(a)'
        }],
        noValidate: true
      })))), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'occupation'), {
        label: "Ocupa\xE7\xE3o",
        noValidate: true
      }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'income'), {
        mask: ['number'],
        label: "Renda",
        noValidate: true
      }))), this.props.hiddenFields && this.props.hiddenFields.indexOf('estimated_rent_value') === -1 && _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'estimated_rent_value'), {
        mask: ['number'],
        label: "Valor estimado do aluguel",
        noValidate: true
      }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldSwitch, _extends({}, (0, _formCreator.fieldProps)(this.props, 'is_student'), {
        label: "Estudante?",
        type: "checkbox",
        noValidate: true
      })))));
    }
  }]);

  return PersonalData;
}(_react.default.Component);

var _default = PersonalData;
exports.default = _default;