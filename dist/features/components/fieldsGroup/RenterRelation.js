"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactTimeout = _interopRequireDefault(require("react-timeout"));

var _Async = _interopRequireDefault(require("react-select/lib/Async"));

var _reactSelect = require("react-select");

var _Field = _interopRequireDefault(require("../../../components/lib/FormFields/components/Field"));

var _Control = _interopRequireDefault(require("../../../components/lib/FormFields/components/Control"));

var _Icon = _interopRequireDefault(require("../../../components/lib/Icon"));

var _Text = _interopRequireDefault(require("../../../components/lib/Text"));

var _constants = require("core/constants");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var RenterRelation =
/*#__PURE__*/
function (_React$Component) {
  _inherits(RenterRelation, _React$Component);

  function RenterRelation(props) {
    var _this;

    _classCallCheck(this, RenterRelation);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(RenterRelation).call(this, props));
    _this.state = {
      inputValue: '',
      options: (_this.props.defaultValue || {}).related_renters || [],
      isWorking: false,
      noOptions: false
    };
    _this.asyncRef = _react.default.createRef();
    return _this;
  }

  _createClass(RenterRelation, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      if ((this.props.defaultValue || {}).related_renters || [].length > 0) {
        var defaultOptions = (this.props.defaultValue || {}).related_renters || [];
        this.setState({
          options: defaultOptions
        });
        this.props.handleFieldChanges('related_renter_ids', _toConsumableArray(defaultOptions).map(function (item) {
          return item.id;
        }), {
          isValid: true,
          // defaultOptions.length > 0,
          value: _toConsumableArray(defaultOptions).map(function (item) {
            return item.id;
          })
        });
      }
    }
  }, {
    key: "getSnapshotBeforeUpdate",
    value: function getSnapshotBeforeUpdate(prevProps) {
      var defaultOptions = (this.props.defaultValue || {}).related_renters || [];
      var defaultOptionsPrev = (prevProps.defaultValue || {}).related_renters || [];
      if (this.state.options.length > 0) return null;
      if (defaultOptions.length < 1) return null;

      if ((this.props.defaultValue || {}).related_renters || [].length > 0) {
        this.setState({
          options: defaultOptions
        });
        this.props.handleFieldChanges('related_renter_ids', _toConsumableArray(defaultOptions).map(function (item) {
          return item.id;
        }), {
          isValid: true,
          // defaultOptions.length > 0,
          value: _toConsumableArray(defaultOptions).map(function (item) {
            return item.id;
          })
        });
      }

      return null;
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {}
  }, {
    key: "render",
    value: function render() {
      var _this2 = this;

      var promiseOptions = function promiseOptions(inputValue) {
        return new Promise(function (resolve, reject) {
          _constants.request.get('{company}/renter?search=' + inputValue).then(function (response) {
            _this2.setState({
              noOptions: false
            });

            resolve(response.data);
          }).catch(function () {
            return reject();
          });
        });
      };

      var DropdownIndicator = function DropdownIndicator(props) {
        return _react.default.createElement(_reactSelect.components.DropdownIndicator, props, _react.default.createElement(_Icon.default, {
          name: "fas fa-user"
        }));
      };

      var Option = function Option(props) {
        var data = props.data || {};
        return _react.default.createElement(_reactSelect.components.Option, props, _react.default.createElement(_Text.default, null, _react.default.createElement("strong", null, data.name || 'Sem nome')), _react.default.createElement(_Text.default, {
          isSmall: true
        }, data.email));
      };

      var MultiValueLabel = function MultiValueLabel(props) {
        var data = props.data || {};
        return _react.default.createElement(_reactSelect.components.MultiValueLabel, props, _react.default.createElement(_Text.default, null, _react.default.createElement("strong", null, data.name || data.email)));
      };

      var MultiValueRemove = function MultiValueRemove(props) {
        return _react.default.createElement("div", null, _react.default.createElement(_reactSelect.components.MultiValueRemove, props, _react.default.createElement(_Icon.default, {
          name: "far fa-times"
        })));
      };

      var LoadingIndicator = function LoadingIndicator(props) {
        return null;
      };

      return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_Field.default, {
        label: "Selecione um ou mais locat\xE1rios"
      }, _react.default.createElement(_Control.default, null, _react.default.createElement("div", {
        className: "react-select-wrapper"
      }, _react.default.createElement(_Async.default, {
        defaultOptions: true,
        ref: function ref(_ref) {
          return _this2.asyncRef = _ref;
        },
        classNamePrefix: "react-select",
        defaultValue: [],
        name: "renter",
        isMulti: true,
        isDisabled: this.state.isWorking,
        isLoading: this.state.isWorking,
        value: this.state.options,
        loadingMessage: function loadingMessage() {
          return 'Pesquisando por locatários...';
        },
        noOptionsMessage: function noOptionsMessage() {
          return _react.default.createElement("span", null, "Nenhum locat\xE1rio encontrado");
        },
        placeholder: "",
        menuPosition: "fixed",
        maxMenuHeight: 162,
        minMenuHeight: 100,
        onInputChange: this.handleInputChange.bind(this),
        inputValue: this.state.inputValue,
        onChange: function onChange(options) {
          _this2.setState({
            options: options,
            inputValue: ''
          });

          if (_this2.props.onSelect) _this2.props.onSelect(options);

          _this2.props.handleFieldChanges('related_renter_ids', _toConsumableArray(options).map(function (item) {
            return item.id;
          }), {
            isValid: true,
            //options.length > 0,
            value: _toConsumableArray(options).map(function (item) {
              return item.id;
            })
          });
        },
        isClearable: false,
        getOptionLabel: function getOptionLabel(option) {
          return option.name;
        },
        getOptionValue: function getOptionValue(option) {
          return option.id;
        },
        components: {
          MultiValueLabel: MultiValueLabel,
          MultiValueRemove: MultiValueRemove,
          DropdownIndicator: DropdownIndicator,
          LoadingIndicator: LoadingIndicator,
          Option: Option
        },
        loadOptions: promiseOptions
      })))));
    }
  }, {
    key: "handleInputChange",
    value: function handleInputChange(inputValue) {
      this.setState({
        inputValue: inputValue
      });
      return inputValue;
    }
  }]);

  return RenterRelation;
}(_react.default.Component);

;
RenterRelation.propTypes = {};
RenterRelation.defaultProps = {
  onClickItem: function onClickItem() {}
};

var _default = (0, _reactTimeout.default)(RenterRelation);

exports.default = _default;