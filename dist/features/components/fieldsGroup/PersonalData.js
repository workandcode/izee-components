"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _FormFields = require("../../../components/lib/FormFields");

var _formCreator = require("../formCreator");

var _Grid = _interopRequireDefault(require("../../../components/lib/Grid"));

var _utils = require("../../../utils/utils");

var _countries = _interopRequireDefault(require("./countries"));

var _this = void 0;

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var PersonalData = function PersonalData(props) {
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(props, 'name'), {
    label: "Nome"
  }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(props, 'email'), {
    isDisabled: props.disableEmail,
    label: "Email",
    noValidate: props.noValidate
  }))), _react.default.createElement(_Grid.default.Col, {
    isNarrow: true
  }, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(props, 'phone'), {
    label: "Telefone",
    mask: ['phone'],
    style: {
      width: 150
    },
    noValidate: true
  })))), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, {
    isNarrow: true
  }, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(props, 'cpf'), {
    label: "CPF",
    mask: ['cpf'],
    noValidate: props.noValidate
  })))), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldSelect, _extends({}, (0, _formCreator.fieldProps)(_this.props, 'identity_type'), {
    label: "Documento",
    valueKey: "name",
    options: [{
      id: 1,
      code: 'DOCUMENT_TYPE_RG',
      name: 'RG'
    }, {
      id: 2,
      code: 'DOCUMENT_TYPE_CNH',
      name: 'CNH'
    }, {
      id: 3,
      code: 'DOCUMENT_TYPE_PASSPORT',
      name: 'Passaporte'
    }, {
      id: 4,
      code: 'DOCUMENT_TYPE_OTHER',
      name: 'Outro documento'
    }],
    noValidate: true
  }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(props, 'identity'), {
    label: "N\xFAmero",
    mask: [{
      maxLength: 15
    }],
    noValidate: true
  }))), _react.default.createElement(_Grid.default.Col, {
    isNarrow: true
  }, _react.default.createElement(_FormFields.FormFieldDate, _extends({}, (0, _formCreator.fieldProps)(props, 'identity_expedition_date'), {
    label: "Data de expedi\xE7\xE3o",
    noValidate: true
  }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(props, 'identity_issuing_body'), {
    label: "Org\xE3o emissor",
    noValidate: true
  })))), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, {
    isNarrow: true
  }, _react.default.createElement(_FormFields.FormFieldDate, _extends({}, (0, _formCreator.fieldProps)(props, 'birth_date'), {
    label: "Data de nascimento",
    noValidate: true
  }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(props, 'occupation'), {
    label: "Ocupa\xE7\xE3o",
    noValidate: true
  }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(props, 'nationality'), {
    label: "Nacionalidade",
    noValidate: true
  }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldSelect, _extends({}, (0, _formCreator.fieldProps)(props, 'marital_status'), {
    label: "Estado civil",
    valueKey: "id",
    options: [{
      id: 1,
      code: 'MARITAL_STATUS_SINGLE',
      name: 'Solteiro(a)'
    }, {
      id: 2,
      code: 'MARITAL_STATUS_MARRIED',
      name: 'Casado(a)'
    }, {
      id: 3,
      code: 'MARITAL_STATUS_DIVORCED',
      name: 'Divorciado(a)'
    }, {
      id: 4,
      code: 'MARITAL_STATUS_STABLE_UNION',
      name: 'União Estável'
    }, {
      id: 5,
      code: 'MARITAL_STATUS_WIDOWED',
      name: 'Viúvo(a)'
    }],
    noValidate: true
  })))));
};

var _default = PersonalData;
exports.default = _default;