"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _jsonp = _interopRequireDefault(require("jsonp"));

var _FormFields = require("../../../../components/lib/FormFields");

var _formCreator = require("../formCreator");

var _Grid = _interopRequireDefault(require("../../../../components/lib/Grid"));

var _utils = require("../../../../utils/utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var FullAddress =
/*#__PURE__*/
function (_React$Component) {
  _inherits(FullAddress, _React$Component);

  function FullAddress(props) {
    var _this;

    _classCallCheck(this, FullAddress);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(FullAddress).call(this, props));
    _this.state = {
      addressByCEP: {},
      isSearchingForAddress: false
    };
    return _this;
  }

  _createClass(FullAddress, [{
    key: "render",
    value: function render() {
      var cepValidation = {
        isInvalid: this.state.addressByCEP.erro,
        message: 'CEP inválido ou inexistente'
      };
      return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, {
        isNarrow: true
      }, _react.default.createElement(_FormFields.FormFieldCEP, _extends({}, (0, _formCreator.fieldProps)(this.props, 'zip_code'), {
        forceInvalid: cepValidation.isInvalid,
        errorMessage: cepValidation.isInvalid && cepValidation.message,
        isLoading: this.state.isSearchingForAddress,
        info: "Se n\xE3o souber o seu CEP preencha os campos manualmente",
        onChange: this.handleCEP.bind(this),
        noValidate: this.props.noValidate
      }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'address.address'), {
        label: "Logradouro",
        noValidate: this.props.noValidate,
        valueDefault: this.state.addressByCEP.logradouro || this.props.defaultValue['address'],
        isDisabled: this.props.formIsWorking || this.state.isSearchingForAddress
      }))), _react.default.createElement(_Grid.default.Col, {
        isOneFifth: true
      }, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'address.number'), {
        valueDefault: this.props.defaultValue['number'] || '',
        label: "N\xFAmero",
        noValidate: this.props.noValidate,
        mask: ['onlyNumbers']
      })))), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'address.city'), {
        label: "Cidade",
        noValidate: this.props.noValidate,
        valueDefault: this.state.addressByCEP.localidade || this.props.defaultValue['city'],
        isDisabled: this.props.formIsWorking || this.state.isSearchingForAddress
      }))), _react.default.createElement(_Grid.default.Col, {
        isOneFifth: true
      }, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'address.state'), {
        label: "Estado",
        noValidate: this.props.noValidate,
        valueDefault: this.state.addressByCEP.uf || this.props.defaultValue['state'],
        isDisabled: this.props.formIsWorking || this.state.isSearchingForAddress
      }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'address.neighborhood'), {
        label: "Bairro",
        noValidate: this.props.noValidate,
        valueDefault: this.state.addressByCEP.bairro || this.props.defaultValue['neighborhood'],
        isDisabled: this.props.formIsWorking || this.state.isSearchingForAddress
      })))), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'address.additional_address'), {
        valueDefault: this.props.defaultValue['additional_address'],
        label: "Complemento (Opcional)",
        noValidate: true
      })))));
    }
  }, {
    key: "handleCEP",
    value: function handleCEP(value, field) {
      var _this2 = this;

      if (!field.wasFocused) return;
      this.setState({
        addressByCEP: {}
      });
      if (!value || value.length < 9) return;
      var searchForCEPDelay = (0, _utils.timeDelay)(1000);
      this.setState({
        isSearchingForAddress: true
      });
      (0, _jsonp.default)('https://viacep.com.br/ws/' + value.replace('-', '') + '/json', null, function (error, response) {
        if (response) {
          searchForCEPDelay(function () {
            _this2.props.handleFieldChanges('address.zip_code', value, field);

            _this2.setState({
              addressByCEP: response || {},
              isSearchingForAddress: false
            });
          });
          return;
        }

        _this2.props.handleFieldChanges('address.zip_code', value, field);

        _this2.setState({
          addressByCEP: {
            erro: true
          },
          isSearchingForAddress: false
        });
      });
    }
  }]);

  return FullAddress;
}(_react.default.Component);

exports.default = FullAddress;
;
FullAddress.defaultProps = {
  defaultValue: {}
};