"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _FormFields = require("../../../../components/lib/FormFields");

var _formCreator = require("../formCreator");

var _Grid = _interopRequireDefault(require("../../../../components/lib/Grid"));

var _utils = require("../../../../utils/utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var PersonalData = function PersonalData(props) {
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(props, 'name'), {
    label: "Nome"
  }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(props, 'email'), {
    label: "Email"
  }))), _react.default.createElement(_Grid.default.Col, {
    isNarrow: true
  }, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(props, 'phone'), {
    label: "Telefone",
    mask: ['phone'],
    style: {
      width: 150
    }
  })))), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, {
    isNarrow: true
  }, _react.default.createElement(_FormFields.FormFieldDate, _extends({}, (0, _formCreator.fieldProps)(props, 'birth_date'), {
    label: "Data de nascimento"
  }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(props, 'occupation'), {
    label: "Ocupa\xE7\xE3o"
  })))), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldSelect, _extends({}, (0, _formCreator.fieldProps)(props, 'marital_status'), {
    label: "Estado civil",
    valueKey: "id",
    options: [{
      id: 1,
      code: 'MARITAL_STATUS_SINGLE',
      name: 'Solteiro(a)'
    }, {
      id: 2,
      code: 'MARITAL_STATUS_MARRIED',
      name: 'Casado(a)'
    }, {
      id: 3,
      code: 'MARITAL_STATUS_DIVORCED',
      name: 'Divorciado(a)'
    }, {
      id: 4,
      code: 'MARITAL_STATUS_STABLE_UNION',
      name: 'União Estável'
    }, {
      id: 5,
      code: 'MARITAL_STATUS_WIDOWED',
      name: 'Viúvo(a)'
    }]
  }))), !props.hideIncome && _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(props, 'income'), {
    mask: ['number'],
    label: "Renda"
  }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(props, 'estimated_rent_value'), {
    mask: ['number'],
    label: "Valor estimado do aluguel"
  }))))), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(props, 'rg'), {
    label: "RG",
    mask: [{
      maxLength: 12
    }]
  }))), _react.default.createElement(_Grid.default.Col, {
    isNarrow: true
  }, _react.default.createElement(_FormFields.FormFieldDate, _extends({}, (0, _formCreator.fieldProps)(props, 'rg_expedition_date'), {
    label: "Data de expedi\xE7\xE3o"
  }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(props, 'cpf'), {
    label: "CPF",
    mask: ['cpf']
  }))), _react.default.createElement(_Grid.default.Col, {
    isNarrow: true
  }, _react.default.createElement(_FormFields.FormFieldSwitch, _extends({}, (0, _formCreator.fieldProps)(props, 'is_student'), {
    label: "Estudante?",
    type: "checkbox"
  })))));
};

var _default = PersonalData;
exports.default = _default;