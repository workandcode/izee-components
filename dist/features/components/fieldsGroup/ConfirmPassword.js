"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _FormFields = require("../../../components/lib/FormFields");

var _Grid = _interopRequireDefault(require("../../../components/lib/Grid"));

var _formCreator = require("../formCreator");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var ConfirmPassword = function ConfirmPassword(props) {
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({
    label: "Senha atual"
  }, (0, _formCreator.fieldProps)(props, 'password')))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({
    label: "Nova senha"
  }, (0, _formCreator.fieldProps)(props, 'new_password'))))));
};

var _default = ConfirmPassword;
exports.default = _default;