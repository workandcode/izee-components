"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _FormFields = require("../../../components/lib/FormFields");

var _Grid = _interopRequireDefault(require("../../../components/lib/Grid"));

var _formCreator = require("../formCreator");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var Negotiation = function Negotiation(props) {
  return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(props, 'rent_value'), {
    label: "Valor do aluguel",
    mask: ['number']
  })))), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldTextArea, _extends({}, (0, _formCreator.fieldProps)(props, 'negotiation'), {
    label: "Negocia\xE7\xE3o"
  })))));
};

var _default = Negotiation;
exports.default = _default;