"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _FormFields = require("../../../components/lib/FormFields");

var _formCreator = require("../formCreator");

var _Grid = _interopRequireDefault(require("../../../components/lib/Grid"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

var Company =
/*#__PURE__*/
function (_React$Component) {
  _inherits(Company, _React$Component);

  function Company(props) {
    var _this;

    _classCallCheck(this, Company);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Company).call(this, props));
    _this.handleCompanyStatus = _this.handleCompanyStatus.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.state = {
      company_status: false
    };
    return _this;
  }

  _createClass(Company, [{
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      var _this$props = this.props,
          defaultValue = _this$props.defaultValue,
          fields = _this$props.fields; // check company_status

      var company_status = fields.cnpj ? fields.cnpj.value : defaultValue.cnpj ? true : false;

      if (!this.state.company_status && company_status) {
        this.setState({
          company_status: company_status
        });
      }
    }
  }, {
    key: "handleCompanyStatus",
    value: function handleCompanyStatus() {
      var _this$props2 = this.props,
          defaultValue = _this$props2.defaultValue,
          fields = _this$props2.fields;
      var company_status = this.state.company_status;
      this.setState({
        company_status: !company_status
      });

      if (company_status) {
        this.props.updateFieldValues([{
          fieldName: 'cnpj',
          field: {
            value: "",
            isValid: true
          }
        }, {
          fieldName: 'trading_name',
          field: {
            value: "",
            isValid: true
          }
        }, {
          fieldName: 'state_enrollment',
          field: {
            value: "",
            isValid: true
          }
        }, {
          fieldName: 'municipal_enrollment',
          field: {
            value: "",
            isValid: true
          }
        }]);
      }
    }
  }, {
    key: "render",
    value: function render() {
      var company_status = this.state.company_status;
      var props = this.props;
      var fields = props.fields;
      return _react.default.createElement(_react.default.Fragment, null, !props.prefix ? _react.default.createElement(_FormFields.FormFieldSwitch, {
        checked: company_status,
        onClick: this.handleCompanyStatus ? this.handleCompanyStatus : null,
        description: "Tem empresa?",
        type: "checkbox"
      }) : null, props.prefix || company_status ? _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(props, props.prefix ? 'name' : 'trading_name'), {
        label: "Nome da empresa"
      }))), _react.default.createElement(_Grid.default.Col, {
        isOneThird: true
      }, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(props, 'cnpj'), {
        label: "CNPJ",
        mask: ['cnpj'],
        noValidate: fields.noValidate
      })))), _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(props, 'trading_name'), {
        label: "Raz\xE3o Social",
        noValidate: true
      }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(props, 'municipal_enrollment'), {
        label: "Inscri\xE7\xE3o Municipal",
        noValidate: true
      }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(props, 'state_enrollment'), {
        label: "Inscri\xE7\xE3o Estadual",
        noValidate: true
      }))), props.prefix ? _react.default.createElement(_Grid.default.Col, {
        isNarrow: true
      }, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(props, 'prefix'), {
        label: "Prefixo",
        info: "Este c\xF3digo aparece antes do ID da proposta",
        mask: [{
          'maxLength': 3
        }],
        validator: [{
          'minLength': 2
        }],
        style: {
          maxWidth: 58
        },
        noValidate: true
      }))) : null)) : null);
    }
  }]);

  return Company;
}(_react.default.Component);

var _default = Company;
exports.default = _default;