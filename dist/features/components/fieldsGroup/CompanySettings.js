"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _FormFields = require("../../../components/lib/FormFields");

var _formCreator = require("../formCreator");

var _Grid = _interopRequireDefault(require("../../../components/lib/Grid"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var CompanySettings = function CompanySettings(props) {
  return _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldSwitch, _extends({}, (0, _formCreator.fieldProps)(props, 'signs_contract'), {
    description: "A imobili\xE1ria assina o contrato no lugar do propriet\xE1rio",
    defaultChecked: props.defaultValue['signs_contract'],
    isLoading: props.formIsWorking
  })), _react.default.createElement(_FormFields.FormFieldSwitch, _extends({}, (0, _formCreator.fieldProps)(props, 'chooses_guarantee'), {
    description: "A imobili\xE1ria escolhe a garantia no lugar do locat\xE1rio",
    defaultChecked: props.defaultValue['chooses_guarantee'],
    isLoading: props.formIsWorking
  }))));
};

var _default = CompanySettings;
exports.default = _default;