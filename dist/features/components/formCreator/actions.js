"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.fakeSubmit = exports.submit = exports.formIsProcessing = void 0;

var _constants = require("core/constants");

var formIsProcessing = function formIsProcessing(is) {
  return function (dispatch) {
    dispatch({
      type: 'SUBMIT_FORM_IS_PROCESSING',
      payload: is
    });
  };
};

exports.formIsProcessing = formIsProcessing;

var submit = function submit(path, body, method, options) {
  return function (dispatch, store) {
    dispatch(formIsProcessing(true));

    if (path.match('{company}')) {
      var companyId = store().app.activeCompany.id;
      path = path.replace('{company}', "company/".concat(companyId));
    }

    _constants.request[method ? method : 'post'](path, body, options).then(function (response) {
      dispatch({
        type: 'SUBMIT_FORM_SUCCESS',
        payload: response.data
      });
    }).catch(function (error) {
      dispatch({
        type: 'SUBMIT_FORM_ERROR',
        payload: error
      });
    });
  };
};

exports.submit = submit;

var fakeSubmit = function fakeSubmit(payload) {
  return function (dispatch) {
    dispatch({
      type: 'SUBMIT_FORM_SUCCESS',
      payload: {
        success: true,
        payload: payload
      }
    });
  };
};

exports.fakeSubmit = fakeSubmit;