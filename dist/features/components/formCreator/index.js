"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.fieldProps = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactTimeout = _interopRequireDefault(require("react-timeout"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _lodash = require("lodash");

var _reactRedux = require("react-redux");

var _redux = require("redux");

var actions = _interopRequireWildcard(require("./actions"));

var _Form = _interopRequireDefault(require("../../../components/lib/Form"));

var _FormFields = require("../../../components/lib/FormFields");

var _parseApiErrors = _interopRequireDefault(require("../../../utils/parseApiErrors"));

var _utils = require("../../../utils/utils");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var formCreator = function formCreator(Component) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  var FormWrapper =
  /*#__PURE__*/
  function (_React$Component) {
    _inherits(FormWrapper, _React$Component);

    function FormWrapper(props) {
      var _this;

      _classCallCheck(this, FormWrapper);

      _this = _possibleConstructorReturn(this, _getPrototypeOf(FormWrapper).call(this, props));
      _this.state = {
        fields: {},
        fieldErrors: {},
        formIsWorking: false,
        formFocus: false
      };
      _this.form = null;
      _this.resetAfterSuccess = false;
      return _this;
    }

    _createClass(FormWrapper, [{
      key: "getSnapshotBeforeUpdate",
      value: function getSnapshotBeforeUpdate(prevProps) {
        var _this2 = this;

        if (prevProps.actionKey !== this.props.actionKey && this.state.formIsWorking) {
          this.setState({
            formIsWorking: false
          });
          this.props.setTimeout(function () {
            _this2.setState({
              formIsWorking: false,
              fieldErrors: (0, _parseApiErrors.default)(_this2.props.errors)
            });

            if (_this2.props.onSuccess) {
              if (_this2.resetAfterSuccess && _this2.props.isSuccess) _this2.resetForm();

              _this2.props.onSuccess({
                isSuccess: _this2.props.isSuccess,
                actionMessage: _this2.props.actionMessage,
                actionKey: _this2.props.actionKey,
                isProcessing: _this2.props.isProcessing,
                errors: _this2.props.errors,
                data: _this2.props.data
              });
            }
          }, 300);
        }

        return null;
      }
    }, {
      key: "componentDidUpdate",
      value: function componentDidUpdate() {}
    }, {
      key: "render",
      value: function render() {
        var _this3 = this;

        var formIsWorking = this.state.formIsWorking;
        var formIsReady = (0, _FormFields.invalidFields)(this.state.fields).length === 0;
        console.log("INVALID", (0, _FormFields.invalidFields)(this.state.fields));
        return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_Form.default, {
          getRef: function getRef(ref) {
            return _this3.form = ref;
          },
          onSubmit: this.submitForm.bind(this),
          onFocus: this.handleFieldFocus.bind(this),
          autoFocus: !this.props.disabledAutoFocus
        }, _react.default.createElement(Component, _extends({
          formIsWorking: formIsWorking,
          formIsReady: formIsReady,
          defaultValue: this.props.defaultValue || {},
          handleFieldChanges: this.handleFieldChanges.bind(this),
          updateFieldValues: this.updateFieldValues.bind(this),
          goto: this.goto.bind(this),
          submitForm: this.submitForm.bind(this),
          createRequest: this.createRequest.bind(this)
        }, this.props, this.state))));
      }
    }, {
      key: "updateFieldValues",
      value: function updateFieldValues(newFields, cb, cbParams) {
        var fields = this.state.fields;
        (0, _lodash.each)(newFields, function (it) {
          fields[it.fieldName] = it.field;
        });
        this.setState({
          fields: fields,
          hideAlert: true,
          formFocus: true
        }, typeof cb === 'function' ? cb(cbParams) : null);
      }
    }, {
      key: "handleFieldChanges",
      value: function handleFieldChanges(name, value, field) {
        var fields = this.state.fields;
        fields[name] = field;
        var fieldError = this.state.fieldErrors;
        delete fieldError[name];
        this.setState({
          fields: fields,
          hideAlert: true,
          formFocus: true,
          fieldErrors: fieldError
        });
        if (options.submitOnChange) this.submitForm();
      }
    }, {
      key: "handleFieldFocus",
      value: function handleFieldFocus(_ref) {
        var target = _ref.target;

        if (!this.state.formFocus) {
          this.setState({
            formFocus: true,
            formElement: target
          });
        }
      }
    }, {
      key: "goto",
      value: function goto(path) {
        this.props.history.push(path);
      }
    }, {
      key: "createRequest",
      value: function createRequest(request, resetAfterSuccess) {
        this.request = request;
        this.resetAfterSuccess = resetAfterSuccess;
      }
    }, {
      key: "submitForm",
      value: function submitForm() {
        if (this.state.formFocus) {
          this.setState({
            formIsWorking: true,
            formFocus: false
          });
          if (this.state.formElement) this.state.formElement.blur();
          if (typeof this.request === 'function') this.request((0, _FormFields.getValues)(this.state.fields), this.state.fields);
          if (typeof this.props.onSubmit === 'function') this.props.onSubmit((0, _FormFields.getValues)(this.state.fields), this.state.fields);
        }
      }
    }, {
      key: "resetForm",
      value: function resetForm() {
        this.form ? this.form.reset() : null;
        this.setState({
          fields: {},
          fieldErrors: {},
          formIsWorking: false,
          formFocus: false
        });
        (0, _utils.setFormInitialFocus)(this.form);
      }
    }]);

    return FormWrapper;
  }(_react.default.Component);

  ;
  FormWrapper.displayName = "FormWrapper(".concat(getDisplayName(Component), ")");

  function mapStateToProps(store) {
    return {
      isSuccess: store.features.isSuccess,
      actionMessage: store.features.actionMessage,
      actionKey: store.features.actionKey,
      isProcessing: store.features.isProcessing,
      errors: store.features.errors,
      data: store.features.data
    };
  }

  function mapDispatchToProps(dispatch) {
    return _objectSpread({}, (0, _redux.bindActionCreators)(actions, dispatch));
  }

  return (0, _reactRedux.connect)(mapStateToProps, mapDispatchToProps)((0, _reactTimeout.default)(FormWrapper));
};

function getDisplayName(Component) {
  return Component.displayName || Component.name || 'Component';
}

;

var fieldProps = function fieldProps(props, fieldName) {
  return {
    name: fieldName,
    valueDefault: props.defaultValue ? (0, _utils.deepKey)(props.defaultValue, fieldName) : '',
    value: props.fields ? (0, _utils.deepKey)(props.fields, "".concat(fieldName, ".value")) || '' : '',
    checkedDefault: props.defaultValue ? (0, _utils.deepKey)(props.defaultValue, fieldName) || false : false,
    checked: props.fields ? (0, _utils.deepKey)(props.fields, "".concat(fieldName, ".value")) || false : false,
    isDisabled: props.formIsWorking || props.isDisabled,
    forceInvalid: props.fieldErrors ? (0, _utils.deepKey)(props.fieldErrors, fieldName) : null,
    errorMessage: props.fieldErrors ? (0, _utils.deepKey)(props.fieldErrors, fieldName) : null,
    onChange: props.handleFieldChanges && props.handleFieldChanges.bind(null, fieldName)
  };
};

exports.fieldProps = fieldProps;
var _default = formCreator;
exports.default = _default;