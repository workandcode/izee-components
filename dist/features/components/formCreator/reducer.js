"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _utils = require("../../../utils/utils");

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var initialState = {
  isSuccess: null,
  actionMessage: '',
  actionKey: (0, _utils.uuid)(),
  isProcessing: false,
  errors: [],
  data: null
};

var _default = function _default() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  switch (action.type) {
    case 'SUBMIT_FORM_IS_PROCESSING':
      state = _objectSpread({}, state, {
        isProcessing: true
      });
      break;

    case 'SUBMIT_FORM_SUCCESS':
      state = _objectSpread({}, state, {
        isSuccess: true,
        actionMessage: action.payload.message,
        actionKey: (0, _utils.uuid)(),
        isProcessing: false,
        errors: [],
        data: action.payload
      });
      break;

    case 'SUBMIT_FORM_ERROR':
      state = _objectSpread({}, state, {
        isSuccess: false,
        actionMessage: action.payload.message,
        actionKey: (0, _utils.uuid)(),
        isProcessing: false,
        errors: action.payload.errors,
        data: action.payload
      });
      break;

    default:
      break;
  }

  return state;
};

exports.default = _default;