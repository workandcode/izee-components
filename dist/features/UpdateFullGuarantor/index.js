"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.UpdateFullGuarantor = void 0;

var _react = _interopRequireDefault(require("react"));

var _moment = _interopRequireDefault(require("moment"));

var _SubmitButton = _interopRequireDefault(require("../../components/SubmitButton"));

var _SectionWrapper = _interopRequireDefault(require("../../components/SectionWrapper"));

var _Grid = _interopRequireDefault(require("../../components/lib/Grid"));

var _FormFields = require("../../components/lib/FormFields");

var _formCreator = _interopRequireDefault(require("../components/formCreator"));

var _PersonalDataRenter = _interopRequireDefault(require("../components/fieldsGroup/PersonalDataRenter"));

var _PersonalDataSpouse = _interopRequireDefault(require("../components/fieldsGroup/PersonalDataSpouse"));

var _Company = _interopRequireDefault(require("../components/fieldsGroup/Company"));

var _FullAddress = _interopRequireDefault(require("../components/fieldsGroup/FullAddress"));

var _utils = require("../../utils/utils");

var _PersonTypes = require("../constants/PersonTypes");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var UpdateFullGuarantor =
/*#__PURE__*/
function (_React$Component) {
  _inherits(UpdateFullGuarantor, _React$Component);

  function UpdateFullGuarantor() {
    _classCallCheck(this, UpdateFullGuarantor);

    return _possibleConstructorReturn(this, _getPrototypeOf(UpdateFullGuarantor).apply(this, arguments));
  }

  _createClass(UpdateFullGuarantor, [{
    key: "componentDidUpdate",
    value: function componentDidUpdate() {
      this.props.createRequest(this.createRequest);
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          formIsWorking = _this$props.formIsWorking,
          formIsReady = _this$props.formIsReady,
          hideSubmitButton = _this$props.hideSubmitButton;
      return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_SectionWrapper.default, {
        header: "Dados do(a) fiador(a)"
      }, _react.default.createElement(_PersonalDataRenter.default, this.props)), _react.default.createElement(_SectionWrapper.default, {
        header: "Dados do(a) c\xF4njuge"
      }, _react.default.createElement(_PersonalDataSpouse.default, this.props)), _react.default.createElement(_SectionWrapper.default, {
        header: "Endere\xE7o do(a) fiador(a)"
      }, _react.default.createElement(_FullAddress.default, _extends({}, this.props, {
        noValidate: true
      }))), _react.default.createElement(_SectionWrapper.default, {
        header: "Dados da empresa fiadora"
      }, _react.default.createElement(_Company.default, _extends({}, this.props, {
        noValidate: true
      }))), _react.default.createElement(_SectionWrapper.default, null, !hideSubmitButton && _react.default.createElement(_SubmitButton.default, {
        label: "Salvar",
        isLoading: formIsWorking,
        isDisabled: !formIsReady || formIsWorking
      })));
    }
  }, {
    key: "createRequest",
    value: function createRequest(fields) {
      var goodFields = (0, _utils.goodObject)(fields, {
        'birth_date': {
          path: 'birth_date',
          format: function format(value) {
            return (0, _moment.default)(value, 'DD/MM/YYYY').format('YYYY-MM-DD');
          }
        },
        'identity_expedition_date': {
          path: 'identity_expedition_date',
          format: function format(value) {
            return (0, _moment.default)(value, 'DD/MM/YYYY').format('YYYY-MM-DD');
          }
        },
        'income': {
          path: 'income',
          format: function format(value) {
            return (0, _utils.rawNumber)(value);
          }
        },
        'marital_status_id': {
          path: 'marital_status',
          format: function format(value) {
            return parseInt(value);
          }
        },
        'spouse_birth_date': {
          path: 'spouse_birth_date',
          format: function format(value) {
            return (0, _moment.default)(value, 'DD/MM/YYYY').format('YYYY-MM-DD');
          }
        }
      });
      this.props.submit("{company}/guarantor/".concat(this.props.guarantorId), goodFields, 'put');
    }
  }]);

  return UpdateFullGuarantor;
}(_react.default.Component);

exports.UpdateFullGuarantor = UpdateFullGuarantor;
;

var _default = (0, _formCreator.default)(UpdateFullGuarantor);

exports.default = _default;