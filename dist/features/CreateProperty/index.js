"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.CreateProperty = void 0;

var _react = _interopRequireDefault(require("react"));

var _SubmitButton = _interopRequireDefault(require("../../components/SubmitButton"));

var _Grid = _interopRequireDefault(require("../../components/lib/Grid"));

var _formCreator = _interopRequireWildcard(require("../components/formCreator"));

var _FormFields = require("../../components/lib/FormFields");

var _FullAddress = _interopRequireDefault(require("../components/fieldsGroup/FullAddress"));

var _PropertyOwnerSelection = _interopRequireDefault(require("../components/fieldsGroup/PropertyOwnerSelection"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var CreateProperty =
/*#__PURE__*/
function (_React$Component) {
  _inherits(CreateProperty, _React$Component);

  function CreateProperty() {
    _classCallCheck(this, CreateProperty);

    return _possibleConstructorReturn(this, _getPrototypeOf(CreateProperty).apply(this, arguments));
  }

  _createClass(CreateProperty, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.props.createRequest(this.createRequest);
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          formIsWorking = _this$props.formIsWorking,
          formIsReady = _this$props.formIsReady;
      return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, {
        isNarrow: true
      }, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'code'), {
        label: "C\xF3digo",
        noValidate: true,
        style: {
          width: 100
        }
      }))), _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldText, _extends({}, (0, _formCreator.fieldProps)(this.props, 'name'), {
        label: "Apelido",
        noValidate: true
      })))), _react.default.createElement(_FullAddress.default, this.props), !this.props.ownerId && _react.default.createElement(_PropertyOwnerSelection.default, this.props), _react.default.createElement(_SubmitButton.default, {
        label: "Salvar",
        isLoading: formIsWorking,
        isDisabled: !formIsReady || formIsWorking
      }));
    }
  }, {
    key: "createRequest",
    value: function createRequest(fields) {
      var ids = [];

      if (this.props.ownerId) {
        ids.push(this.props.ownerId);
        fields['property_owner_ids'] = ids;
      }

      this.props.submit('{company}/property', fields);
    }
  }]);

  return CreateProperty;
}(_react.default.Component);

exports.CreateProperty = CreateProperty;
;

var _default = (0, _formCreator.default)(CreateProperty);

exports.default = _default;