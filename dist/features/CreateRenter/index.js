"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.CreateRenter = void 0;

var _react = _interopRequireDefault(require("react"));

var _moment = _interopRequireDefault(require("moment"));

var _SubmitButton = _interopRequireDefault(require("../../components/SubmitButton"));

var _formCreator = _interopRequireDefault(require("../components/formCreator"));

var _RenterData = _interopRequireDefault(require("../components/fieldsGroup/RenterData"));

var _FullAddress = _interopRequireDefault(require("../components/fieldsGroup/FullAddress"));

var _PropertySelection = _interopRequireDefault(require("../components/fieldsGroup/PropertySelection"));

var _utils = require("../../utils/utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var CreateRenter =
/*#__PURE__*/
function (_React$Component) {
  _inherits(CreateRenter, _React$Component);

  function CreateRenter() {
    _classCallCheck(this, CreateRenter);

    return _possibleConstructorReturn(this, _getPrototypeOf(CreateRenter).apply(this, arguments));
  }

  _createClass(CreateRenter, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.props.createRequest(this.createRequest, true);
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          formIsWorking = _this$props.formIsWorking,
          formIsReady = _this$props.formIsReady;
      return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_RenterData.default, this.props), _react.default.createElement(_SubmitButton.default, {
        label: "Salvar e continuar",
        isLoading: formIsWorking,
        isDisabled: !formIsReady || formIsWorking
      }));
    }
  }, {
    key: "createRequest",
    value: function createRequest(fields) {
      var goodFields = (0, _utils.goodObject)(fields, {
        'birth_date': {
          path: 'birth_date',
          format: function format(value) {
            return (0, _moment.default)(value, 'DD/MM/YYYY').format('YYYY-MM-DD');
          }
        },
        'identity_expedition_date': {
          path: 'identity_expedition_date',
          format: function format(value) {
            return (0, _moment.default)(value, 'DD/MM/YYYY').format('YYYY-MM-DD');
          }
        },
        'income': {
          path: 'income',
          format: function format(value) {
            return (0, _utils.rawNumber)(value);
          }
        },
        'estimated_rent_value': {
          path: 'estimated_rent_value',
          format: function format(value) {
            return (0, _utils.rawNumber)(value);
          }
        }
      });
      this.props.submit('{company}/renter', goodFields);
    }
  }]);

  return CreateRenter;
}(_react.default.Component);

exports.CreateRenter = CreateRenter;
;

var _default = (0, _formCreator.default)(CreateRenter);

exports.default = _default;