"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.UpdateFullRenter = void 0;

var _react = _interopRequireDefault(require("react"));

var _moment = _interopRequireDefault(require("moment"));

var _Grid = _interopRequireDefault(require("../../components/lib/Grid"));

var _SubmitButton = _interopRequireDefault(require("../../components/SubmitButton"));

var _SectionWrapper = _interopRequireDefault(require("../../components/SectionWrapper"));

var _Text = _interopRequireDefault(require("../../components/lib/Text"));

var _Icon = _interopRequireDefault(require("../../components/lib/Icon"));

var _formCreator = _interopRequireDefault(require("../components/formCreator"));

var _FormFields = require("../../components/lib/FormFields");

var _Company = _interopRequireDefault(require("../components/fieldsGroup/Company"));

var _FullAddress = _interopRequireDefault(require("../components/fieldsGroup/FullAddress"));

var _PersonalDataRenter = _interopRequireDefault(require("../components/fieldsGroup/PersonalDataRenter"));

var _PersonalDataSpouse = _interopRequireDefault(require("../components/fieldsGroup/PersonalDataSpouse"));

var _RenterRelation = _interopRequireDefault(require("../components/fieldsGroup/RenterRelation"));

var _utils = require("../../utils/utils");

var _PersonTypes = require("../constants/PersonTypes");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var UpdateFullRenter =
/*#__PURE__*/
function (_React$Component) {
  _inherits(UpdateFullRenter, _React$Component);

  function UpdateFullRenter() {
    _classCallCheck(this, UpdateFullRenter);

    return _possibleConstructorReturn(this, _getPrototypeOf(UpdateFullRenter).apply(this, arguments));
  }

  _createClass(UpdateFullRenter, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.props.createRequest(this.createRequest); // console.log(this.props);
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {}
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          defaultValue = _this$props.defaultValue,
          fields = _this$props.fields,
          formIsReady = _this$props.formIsReady,
          formIsWorking = _this$props.formIsWorking,
          hideSubmitButton = _this$props.hideSubmitButton,
          showRelation = _this$props.showRelation;
      var user = defaultValue;
      var guarantees = (user.guarantees || []).filter(function (item) {
        return item.is_active && item;
      });
      return _react.default.createElement(_react.default.Fragment, null, user.is_verified && _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement("div", null, _react.default.createElement(_Text.default, {
        isDefault: true
      }, user.is_verified && _react.default.createElement(_Icon.default, {
        name: "fas fa-shield-check",
        style: {
          marginLeft: -4
        },
        isSuccess: true
      }), "Garantias pr\xE9-aprovadas pela ", _react.default.createElement("strong", null, "Izee"), ": ", guarantees.map(function (guarantee) {
        return guarantee.name;
      }).join(', '), " ", guarantees.length === 0 && 'Todas'), _react.default.createElement("hr", null)))), _react.default.createElement(_SectionWrapper.default, {
        header: "Dados do(a) locat\xE1rio(a)"
      }, _react.default.createElement(_PersonalDataRenter.default, this.props)), _react.default.createElement(_SectionWrapper.default, {
        header: "Dados do(a) c\xF4njuge"
      }, _react.default.createElement(_PersonalDataSpouse.default, this.props)), _react.default.createElement(_SectionWrapper.default, {
        header: "Endere\xE7o do(a) locat\xE1rio(a)"
      }, _react.default.createElement(_FullAddress.default, _extends({}, this.props, {
        noValidate: true
      }))), _react.default.createElement(_SectionWrapper.default, {
        header: "Dados da empresa"
      }, _react.default.createElement(_Company.default, _extends({}, this.props, {
        noValidate: true
      }))), _react.default.createElement(_SectionWrapper.default, null, showRelation ? _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_RenterRelation.default, _extends({
        onSelect: this.selectedRentersHandler.bind(this)
      }, this.props)))) : null), _react.default.createElement(_SectionWrapper.default, null, !hideSubmitButton && _react.default.createElement(_SubmitButton.default, {
        label: "Salvar",
        isLoading: formIsWorking,
        isDisabled: !formIsReady || formIsWorking
      })));
    }
  }, {
    key: "fieldsToClear",
    value: function fieldsToClear() {
      var fields = this.props.fields;
      return [{
        fieldName: 'name',
        field: _objectSpread({}, fields.name)
      }, {
        fieldName: 'phone',
        field: _objectSpread({}, fields.phone, {
          value: ""
        })
      }, {
        fieldName: 'identity_type',
        field: _objectSpread({}, fields.identity_type, {
          value: ""
        })
      }, {
        fieldName: 'identity',
        field: _objectSpread({}, fields.identity, {
          value: ""
        })
      }, {
        fieldName: 'identity_expedition_date',
        field: _objectSpread({}, fields.identity_expedition_date, {
          value: ""
        })
      }, {
        fieldName: 'identity_issuing_body',
        field: _objectSpread({}, fields.identity_issuing_body, {
          value: ""
        })
      }, {
        fieldName: 'birth_date',
        field: _objectSpread({}, fields.birth_date, {
          value: ""
        })
      }, {
        fieldName: 'occupation',
        field: _objectSpread({}, fields.occupation, {
          value: ""
        })
      }, {
        fieldName: 'nationality',
        field: _objectSpread({}, fields.nationality, {
          value: ""
        })
      }, {
        fieldName: 'marital_status',
        field: _objectSpread({}, fields.marital_status, {
          value: ""
        })
      }, {
        fieldName: 'prefix',
        field: _objectSpread({}, fields.prefix, {
          value: ""
        })
      }, {
        fieldName: 'trading_name',
        field: _objectSpread({}, fields.trading_name, {
          value: ""
        })
      }, {
        fieldName: 'municipal_enrollment',
        field: _objectSpread({}, fields.municipal_enrollment, {
          value: ""
        })
      }, {
        fieldName: 'state_enrollment',
        field: _objectSpread({}, fields.state_enrollment, {
          value: ""
        })
      }, {
        fieldName: 'income',
        field: _objectSpread({}, fields.income, {
          value: ""
        })
      }, {
        fieldName: 'spouse_phone',
        field: _objectSpread({}, fields.spouse_phone, {
          value: ""
        })
      }, {
        fieldName: 'spouse_birth_date',
        field: _objectSpread({}, fields.spouse_birth_date, {
          value: ""
        })
      }, {
        fieldName: 'spouse_occupation',
        field: _objectSpread({}, fields.spouse_occupation, {
          value: ""
        })
      }, {
        fieldName: 'spouse_nationality',
        field: _objectSpread({}, fields.spouse_nationality, {
          value: ""
        })
      }, {
        fieldName: 'spouse_identity_type',
        field: _objectSpread({}, fields.spouse_identity_type, {
          value: ""
        })
      }, {
        fieldName: 'spouse_identity',
        field: _objectSpread({}, fields.spouse_identity, {
          value: ""
        })
      }, {
        fieldName: 'spouse_identity_issuing_body',
        field: _objectSpread({}, fields.spouse_identity_issuing_body, {
          value: ""
        })
      }, {
        fieldName: 'spouse_cpf',
        field: _objectSpread({}, fields.spouse_cpf, {
          value: ""
        })
      }];
    }
  }, {
    key: "selectedRentersHandler",
    value: function selectedRentersHandler(renters) {
      this.setState({
        renters: renters
      });
    }
  }, {
    key: "createRequest",
    value: function createRequest(fields) {
      var goodFields = (0, _utils.goodObject)(fields, {
        'birth_date': {
          path: 'birth_date',
          format: function format(value) {
            return (0, _moment.default)(value, 'DD/MM/YYYY').format('YYYY-MM-DD');
          }
        },
        'identity_expedition_date': {
          path: 'identity_expedition_date',
          format: function format(value) {
            return (0, _moment.default)(value, 'DD/MM/YYYY').format('YYYY-MM-DD');
          }
        },
        'income': {
          path: 'income',
          format: function format(value) {
            return (0, _utils.rawNumber)(value);
          }
        },
        'estimated_rent_value': {
          path: 'estimated_rent_value',
          format: function format(value) {
            return (0, _utils.rawNumber)(value);
          }
        },
        'marital_status_id': {
          path: 'marital_status',
          format: function format(value) {
            return parseInt(value);
          }
        },
        'spouse_birth_date': {
          path: 'spouse_birth_date',
          format: function format(value) {
            return (0, _moment.default)(value, 'DD/MM/YYYY').format('YYYY-MM-DD');
          }
        }
      });
      this.props.submit("{company}/renter/".concat(this.props.renterId), goodFields, 'put');
    }
  }]);

  return UpdateFullRenter;
}(_react.default.Component);

exports.UpdateFullRenter = UpdateFullRenter;
;

var _default = (0, _formCreator.default)(UpdateFullRenter);

exports.default = _default;