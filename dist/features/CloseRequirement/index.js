"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.CloseRequirement = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _FormFields = require("../../components/lib/FormFields");

var _Grid = _interopRequireDefault(require("../../components/lib/Grid"));

var _formCreator = _interopRequireDefault(require("../components/formCreator"));

var _Button = _interopRequireDefault(require("../../components/lib/Button"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var CloseRequirement =
/*#__PURE__*/
function (_React$Component) {
  _inherits(CloseRequirement, _React$Component);

  function CloseRequirement(props) {
    var _this;

    _classCallCheck(this, CloseRequirement);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(CloseRequirement).call(this, props));
    _this.state = {
      requirementIsClosed: false
    };
    return _this;
  }

  _createClass(CloseRequirement, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.props.createRequest(this.createRequest, true);
    }
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          formIsWorking = _this$props.formIsWorking,
          formIsReady = _this$props.formIsReady,
          buttonLabel = _this$props.buttonLabel,
          description = _this$props.description;
      var submitIsDisabled = !formIsReady || formIsWorking || !this.state.requirementIsClosed;
      return _react.default.createElement(_Grid.default, null, _react.default.createElement(_Grid.default.Col, null, _react.default.createElement(_FormFields.FormFieldCheckbox, {
        description: description || 'Declaro que esta etapa está concluída e desejo encerrá-la.',
        onChange: this.closeRequirement.bind(this)
      })), _react.default.createElement(_Grid.default.Col, {
        isRight: true
      }, _react.default.createElement(_Button.default, {
        hasThemeColor: true,
        isLoading: formIsWorking,
        isDisabled: submitIsDisabled,
        submit: true
      }, buttonLabel || 'Concluir')));
    }
  }, {
    key: "closeRequirement",
    value: function closeRequirement(value, field) {
      this.setState({
        requirementIsClosed: value
      });
    }
  }, {
    key: "createRequest",
    value: function createRequest() {
      var _this$props2 = this.props,
          processId = _this$props2.processId,
          requirement = _this$props2.requirement;
      this.props.submit("{company}/process/".concat(processId, "/").concat(requirement, "/close-requirement"));
    }
  }]);

  return CloseRequirement;
}(_react.default.Component);

exports.CloseRequirement = CloseRequirement;
;
CloseRequirement.defaultProps = {};
CloseRequirement.propTypes = {};

var _default = (0, _formCreator.default)(CloseRequirement);

exports.default = _default;