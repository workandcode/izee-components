"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.UpdatePassword = void 0;

var _react = _interopRequireDefault(require("react"));

var _SubmitButton = _interopRequireDefault(require("../../components/SubmitButton"));

var _formCreator = _interopRequireDefault(require("../components/formCreator"));

var _ConfirmPassword = _interopRequireDefault(require("../components/fieldsGroup/ConfirmPassword"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var UpdatePassword =
/*#__PURE__*/
function (_React$Component) {
  _inherits(UpdatePassword, _React$Component);

  function UpdatePassword() {
    _classCallCheck(this, UpdatePassword);

    return _possibleConstructorReturn(this, _getPrototypeOf(UpdatePassword).apply(this, arguments));
  }

  _createClass(UpdatePassword, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.props.createRequest(this.createRequest);
    }
  }, {
    key: "render",
    value: function render() {
      var formIsWorking = this.props.formIsWorking;
      return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_ConfirmPassword.default, this.props), _react.default.createElement(_SubmitButton.default, {
        label: "Alterar senha",
        isLoading: formIsWorking,
        isDisabled: !this.props.formIsReady || formIsWorking
      }));
    }
  }, {
    key: "createRequest",
    value: function createRequest(fields) {
      this.props.submit('user', fields, 'put');
    }
  }]);

  return UpdatePassword;
}(_react.default.Component);

exports.UpdatePassword = UpdatePassword;
;
UpdatePassword.defaultProps = {
  fields: {}
};

var _default = (0, _formCreator.default)(UpdatePassword);

exports.default = _default;