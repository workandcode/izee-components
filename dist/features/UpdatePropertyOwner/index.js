"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = exports.UpdatePropertyOwner = void 0;

var _react = _interopRequireDefault(require("react"));

var _moment = _interopRequireDefault(require("moment"));

var _SubmitButton = _interopRequireDefault(require("../../components/SubmitButton"));

var _SectionWrapper = _interopRequireDefault(require("../../components/SectionWrapper"));

var _Grid = _interopRequireDefault(require("../../components/lib/Grid"));

var _FormFields = require("../../components/lib/FormFields");

var _formCreator = _interopRequireDefault(require("../components/formCreator"));

var _PersonalData = _interopRequireDefault(require("../components/fieldsGroup/PersonalData"));

var _PersonalDataRenter = _interopRequireDefault(require("../components/fieldsGroup/PersonalDataRenter"));

var _PersonalDataSpouse = _interopRequireDefault(require("../components/fieldsGroup/PersonalDataSpouse"));

var _Company = _interopRequireDefault(require("../components/fieldsGroup/Company"));

var _FullAddress = _interopRequireDefault(require("../components/fieldsGroup/FullAddress"));

var _utils = require("../../utils/utils");

var _PersonTypes = require("../constants/PersonTypes");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

var UpdatePropertyOwner =
/*#__PURE__*/
function (_React$Component) {
  _inherits(UpdatePropertyOwner, _React$Component);

  function UpdatePropertyOwner(props) {
    var _this;

    _classCallCheck(this, UpdatePropertyOwner);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(UpdatePropertyOwner).call(this, props)); // const { proposal } = this.props;
    // const signMethod = deepKey(proposal, 'contract.sign_method');

    _this.chooseSpouse = _this.chooseSpouse.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.state = {
      spouseStatus: false
    };
    return _this;
  }

  _createClass(UpdatePropertyOwner, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      this.props.createRequest(this.createRequest);
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate() {}
  }, {
    key: "render",
    value: function render() {
      var _this$props = this.props,
          formIsWorking = _this$props.formIsWorking,
          formIsReady = _this$props.formIsReady;
      var spouseStatus = this.state.spouseStatus; // console.log(spouseStatus, "rerender");

      return _react.default.createElement(_react.default.Fragment, null, _react.default.createElement(_SectionWrapper.default, {
        header: "Dados do(a) propriet\xE1rio(a)"
      }, _react.default.createElement(_PersonalDataRenter.default, this.props)), _react.default.createElement(_SectionWrapper.default, {
        header: "Dados do(a) c\xF4njuge"
      }, _react.default.createElement(_PersonalDataSpouse.default, this.props)), _react.default.createElement(_SectionWrapper.default, {
        header: "Endere\xE7o do(a) propriet\xE1rio(a)"
      }, _react.default.createElement(_FullAddress.default, _extends({}, this.props, {
        noValidate: true
      }))), _react.default.createElement(_SectionWrapper.default, {
        header: "Dados da empresa propriet\xE1rio(a)"
      }, _react.default.createElement(_Company.default, _extends({}, this.props, {
        noValidate: true
      }))), _react.default.createElement(_SectionWrapper.default, {
        header: ""
      }, _react.default.createElement(_SubmitButton.default, {
        label: "Salvar",
        isLoading: formIsWorking,
        isDisabled: !formIsReady || formIsWorking
      })));
    }
  }, {
    key: "chooseSpouse",
    value: function chooseSpouse() {// const { spouseStatus } = this.state;
      // this.setState({ spouseStatus: !spouseStatus });
    }
  }, {
    key: "fieldsToClear",
    value: function fieldsToClear() {
      var fields = this.props.fields;
      return [{
        fieldName: 'phone',
        field: _objectSpread({}, fields.phone, {
          value: ""
        })
      }, {
        fieldName: 'identity_type',
        field: _objectSpread({}, fields.identity_type, {
          value: ""
        })
      }, {
        fieldName: 'identity',
        field: _objectSpread({}, fields.identity, {
          value: ""
        })
      }, {
        fieldName: 'identity_expedition_date',
        field: _objectSpread({}, fields.identity_expedition_date, {
          value: ""
        })
      }, {
        fieldName: 'identity_issuing_body',
        field: _objectSpread({}, fields.identity_issuing_body, {
          value: ""
        })
      }, {
        fieldName: 'birth_date',
        field: _objectSpread({}, fields.birth_date, {
          value: ""
        })
      }, {
        fieldName: 'occupation',
        field: _objectSpread({}, fields.occupation, {
          value: ""
        })
      }, {
        fieldName: 'nationality',
        field: _objectSpread({}, fields.nationality, {
          value: ""
        })
      }, {
        fieldName: 'marital_status',
        field: _objectSpread({}, fields.marital_status, {
          value: ""
        })
      }, {
        fieldName: 'prefix',
        field: _objectSpread({}, fields.prefix, {
          value: ""
        })
      }, {
        fieldName: 'trading_name',
        field: _objectSpread({}, fields.trading_name, {
          value: ""
        })
      }, {
        fieldName: 'municipal_enrollment',
        field: _objectSpread({}, fields.municipal_enrollment, {
          value: ""
        })
      }, {
        fieldName: 'state_enrollment',
        field: _objectSpread({}, fields.state_enrollment, {
          value: ""
        })
      }, {
        fieldName: 'spouse_phone',
        field: _objectSpread({}, fields.spouse_phone, {
          value: ""
        })
      }, {
        fieldName: 'spouse_birth_date',
        field: _objectSpread({}, fields.spouse_birth_date, {
          value: ""
        })
      }, {
        fieldName: 'spouse_occupation',
        field: _objectSpread({}, fields.spouse_occupation, {
          value: ""
        })
      }, {
        fieldName: 'spouse_nationality',
        field: _objectSpread({}, fields.spouse_nationality, {
          value: ""
        })
      }, {
        fieldName: 'spouse_identity_type',
        field: _objectSpread({}, fields.spouse_identity_type, {
          value: ""
        })
      }, {
        fieldName: 'spouse_identity',
        field: _objectSpread({}, fields.spouse_identity, {
          value: ""
        })
      }, {
        fieldName: 'spouse_identity_issuing_body',
        field: _objectSpread({}, fields.spouse_identity_issuing_body, {
          value: ""
        })
      }, {
        fieldName: 'spouse_cpf',
        field: _objectSpread({}, fields.spouse_cpf, {
          value: ""
        })
      }];
    }
  }, {
    key: "createRequest",
    value: function createRequest(fields) {
      var goodFields = (0, _utils.goodObject)(fields, {
        'birth_date': {
          path: 'birth_date',
          format: function format(value) {
            return (0, _moment.default)(value, 'DD/MM/YYYY').format('YYYY-MM-DD');
          }
        },
        'identity_expedition_date': {
          path: 'identity_expedition_date',
          format: function format(value) {
            return (0, _moment.default)(value, 'DD/MM/YYYY').format('YYYY-MM-DD');
          }
        },
        'marital_status_id': {
          path: 'marital_status',
          format: function format(value) {
            return parseInt(value);
          }
        },
        'spouse_birth_date': {
          path: 'spouse_birth_date',
          format: function format(value) {
            return (0, _moment.default)(value, 'DD/MM/YYYY').format('YYYY-MM-DD');
          }
        }
      });
      this.props.submit("{company}/property-owner/".concat(this.props.propertyOwnerId), goodFields, 'put');
    }
  }]);

  return UpdatePropertyOwner;
}(_react.default.Component);

exports.UpdatePropertyOwner = UpdatePropertyOwner;
;

var _default = (0, _formCreator.default)(UpdatePropertyOwner);

exports.default = _default;