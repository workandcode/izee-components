"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _utils = require("./utils");

var _default = function _default(errors) {
  if (!errors && (0, _utils.getTypeOf)(errors) !== 'Array' || errors.length < 1) return {};
  if ((0, _utils.getTypeOf)(errors) !== 'Array') return {};
  var errorsObject = {};
  errors.forEach(function (item) {
    errorsObject[item.field] = item.message;
  });
  return errorsObject;
};

exports.default = _default;