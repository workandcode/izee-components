"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createHelper = exports.notification = exports.requestState = void 0;

var requestState = function requestState(is, type) {
  return function (dispatch) {
    dispatch({
      type: 'IS_WORKING',
      payload: is,
      valueKey: type
    });
  };
};

exports.requestState = requestState;

var notification = function notification(_ref) {
  var isOpen = _ref.isOpen,
      message = _ref.message,
      status = _ref.status;
  return function (dispatch) {
    if (isOpen === false) {
      dispatch({
        type: 'HIDE_NOTIFICATION'
      });
      return;
    }

    dispatch({
      type: 'SHOW_NOTIFICATION',
      payload: {
        isOpen: isOpen === undefined ? true : isOpen,
        message: message,
        status: status || 'success'
      }
    });
  };
};

exports.notification = notification;

var createHelper = function createHelper(name, helper) {
  return function (dispatch) {
    dispatch({
      type: 'CREATE_HELPER',
      payload: {
        helper: helper,
        name: name
      }
    });
  };
};

exports.createHelper = createHelper;