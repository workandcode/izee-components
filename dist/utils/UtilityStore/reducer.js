"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var initialState = {
  isWorking: {},
  notification: {
    isOpen: false,
    message: '',
    status: null
  },
  helpers: {}
};

var _default = function _default() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : initialState;
  var action = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  switch (action.type) {
    case 'IS_WORKING':
      var isWorking = _objectSpread({}, state.isWorking);

      if (action.valueKey === 'all') {
        Object.keys(isWorking).forEach(function (item) {
          return isWorking[item] = action.payload;
        });
      } else {
        isWorking = _objectSpread({}, isWorking, _defineProperty({}, action.valueKey, action.payload));
      }

      state = _objectSpread({}, state, {
        isWorking: isWorking
      });
      break;

    case 'SHOW_NOTIFICATION':
      var _action$payload = action.payload,
          isOpen = _action$payload.isOpen,
          message = _action$payload.message,
          status = _action$payload.status;
      state = _objectSpread({}, state, {
        notification: {
          isOpen: isOpen,
          message: isOpen ? message : '',
          status: status
        }
      });
      break;

    case 'HIDE_NOTIFICATION':
      state = _objectSpread({}, state, {
        notification: {
          isOpen: false,
          message: '',
          status: null
        }
      });
      break;

    case 'CREATE_HELPER':
      var helpers = state.helpers;
      helpers[action.payload.name] = action.payload.helper;
      state = _objectSpread({}, state, {
        helpers: helpers
      });
      break;

    default:
      break;
  }

  return state;
};

exports.default = _default;