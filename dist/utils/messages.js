"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DictComponent = exports.messages = exports.dict = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var dict = {
  common: {
    welcome: 'Bem vindo'
  },
  formFields: {
    password: {
      minLength: 'A senha deve conter de 6 á 16 caracteres'
    }
  },
  login: {
    welcome: 'Olá, bem vindo!',
    processing: 'Efetuando login, aguarde.',
    success: 'Login efetuado com sucesso! Vamos redirecionar você...',
    error: 'Login e/ou senha inválidos.'
  },
  inviteUser: {
    alreadyInvited: 'Cadastrado como: ${name} Empresa: ${company}'
  },
  createCompany: {
    welcomeNewUser: 'Olá ${name} agora cadastre sua empresa!',
    invalidInvite: 'Este convite já foi utilizado ou é inválido.'
  }
};
exports.dict = dict;

var messages = function messages(messageKey, string, isReact) {
  var message = deepKey(dict, messageKey);
  string.forEach(function (item) {
    message = message.replace(/\$\{[a-z]+\}/, item);
  });

  if (isReact) {
    return _react.default.createElement("span", {
      dangerouslySetInnerHTML: {
        __html: message
      }
    });
  }

  return message;
};

exports.messages = messages;

var DictComponent = function DictComponent(props) {
  return _react.default.createElement(_react.default.Fragment, null, messages(props.path, props.string));
};

exports.DictComponent = DictComponent;

function deepKey(object, path, value) {
  var paths = path.split('.');
  var newPath = paths.slice(1);
  if (value) object[paths[0]] = object[paths[0]] || {};

  if (paths.length === 1) {
    if (value !== undefined) {
      if (value) return object[paths[0]] = value;
      return object[paths[0]];
    }

    if (!object) return null;
    if (object.hasOwnProperty(paths[0])) return object[paths[0]];
    return null;
  }

  return deepKey(object[paths[0]], newPath.join('.'), value);
}