"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _ChildRoutes = _interopRequireDefault(require("./ChildRoutes"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var RouteWrapper = function RouteWrapper(props) {
  if (props.beforeReturn) props.beforeReturn(props);
  return _react.default.createElement(_ChildRoutes.default, props);
};

RouteWrapper.defaultProps = {
  beforeReturn: null
};
RouteWrapper.propTypes = {
  beforeReturn: _propTypes.default.func
};
var _default = RouteWrapper;
exports.default = _default;