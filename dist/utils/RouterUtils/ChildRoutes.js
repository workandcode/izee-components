"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactRouterDom = require("react-router-dom");

var _RouterParams = _interopRequireDefault(require("./RouterParams"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var ChildRoutes = function ChildRoutes(props) {
  if (props.match) {
    Object.keys(props.match.params).forEach(function (paramKey) {
      _RouterParams.default.setParam(paramKey, props.match.params[paramKey]);
    });

    _RouterParams.default.setup();
  }

  var routes = (props.routes || []).map(function (route, index) {
    return _react.default.createElement(_reactRouterDom.Route, {
      key: route.key || index,
      location: props.location,
      exact: route.exact,
      path: route.path,
      render: function render(routeProps) {
        return _react.default.createElement(route.component, _extends({}, props, routeProps, route));
      }
    });
  });
  return _react.default.createElement(_reactRouterDom.Switch, {
    location: props.location
  }, routes);
};

var _default = ChildRoutes;
exports.default = _default;