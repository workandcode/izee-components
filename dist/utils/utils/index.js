"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.uuid = uuid;
exports.arrayMove = arrayMove;
exports.camelCaseToDash = camelCaseToDash;
exports.convertHex = convertHex;
exports.promiseWrapper = promiseWrapper;
exports.deepKey = deepKey;
exports.isInvalidNumber = isInvalidNumber;
exports.buildQuery = buildQuery;
exports.filterArrayBy = filterArrayBy;
exports.setFormInitialFocus = setFormInitialFocus;
exports.copyToClipboard = copyToClipboard;
exports.currency = exports.debounce = exports.prettyNumber = exports.objectIsEmpty = exports.rawNumber = exports.fixNumber = exports.goodObject = exports.objectHasKeys = exports.checkObjectKeys = exports.timeDelay = exports.timeNow = exports.getTypeOf = exports.isFloat = exports.isInt = exports.isMobile = void 0;

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * Check if userAgent match a mobile device
 * @return {String}
 */
var isMobile = {
  Android: function Android() {
    return navigator.userAgent.match(/Android/i);
  },
  BlackBerry: function BlackBerry() {
    return navigator.userAgent.match(/BlackBerry/i);
  },
  iOS: function iOS() {
    return navigator.userAgent.match(/iPhone|iPad|iPod/i);
  },
  Opera: function Opera() {
    return navigator.userAgent.match(/Opera Mini/i);
  },
  Windows: function Windows() {
    return navigator.userAgent.match(/IEMobile/i);
  },
  any: function any() {
    return isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows();
  }
};
/**
 * Return a unique id
 * @return {String}
 */

exports.isMobile = isMobile;

function uuid() {
  var time = function time() {
    return new Date();
  };

  var uuid = function uuid() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1) + time().getTime();
  };

  return uuid();
}

;
/**
 * Return if a number is integer
 * @param  {Number} number
 * @return {Boolean}
 */

var isInt = function isInt(number) {
  return Number(number) === number && number % 1 === 0;
};
/**
 * Return if a number is float
 * @param  {Number} number
 * @return {Boolean}
 */


exports.isInt = isInt;

var isFloat = function isFloat(number) {
  return Number(number) === number && number % 1 !== 0;
};
/**
 * Move an array item to another position
 * @param  {Array} array
 * @param  {Number} currentIndex
 * @param  {Number} offset
 * @return {Array}
 */


exports.isFloat = isFloat;

function arrayMove(array, currentIndex, offset) {
  var removedItem = null;
  var index = currentIndex;
  var newIndex = index + offset;
  if (newIndex > -1 && newIndex < array.length) removedItem = array.splice(index, 1)[0];
  array.splice(newIndex, 0, removedItem);
  return array;
}

;
/**
 * Convert a camelCase string to dash-string
 * @param  {String}
 * @return {String}
 */

function camelCaseToDash(string) {
  if (!string) return string;
  return string.replace(/([A-Z]|[0-9]{1,3})/g, '-$1').toLowerCase();
}

;
/**
 * Return a timestamp from now
 * @param  {Any} any
 * @return {String}
 */

var getTypeOf = function getTypeOf(any) {
  return toString.call(any).slice(8, -1);
};
/**
 * @return {Date} Datetime
 */


exports.getTypeOf = getTypeOf;

var timeNow = Date.now || function () {
  return new Date().getTime();
};
/**
 * Return the rgba value for the hex color
 * This function was founded on stackoverflow
 * @param  {String} hex
 * @param  {Number} opacity 0 - 100
 * @return {String}
 */


exports.timeNow = timeNow;

function convertHex(hex, opacity) {
  hex = hex.replace('#', '');
  var r = parseInt(hex.substring(0, 2), 16);
  var g = parseInt(hex.substring(2, 4), 16);
  var b = parseInt(hex.substring(4, 6), 16);
  var result = "rgba(".concat(r, ", ").concat(g, ", ").concat(b, ", ").concat(opacity / 100, ")");
  return result;
}
/**
 * Set a timer and create a function to wrapp a callback for delayed execution
 * @author @cspilhere
 * @param  {Number} minTime
 * @return {Function} Return a function to wrapp the callback
 * @example
 * const delay = timeDelay(130);
 * delay(() => {});
 */


var timeDelay = function timeDelay(minTime) {
  var stamp = timeNow();
  var timer = setTimeout;
  return function (callback) {
    if (timeNow() - stamp < minTime) {
      timer(function () {
        callback();
        timer = null;
      }, minTime - (timeNow() - stamp));
    } else {
      callback();
      timer = null;
    }
  };
};
/**
 * Wrap a Promise with timeDelay
 * @author @cspilhere
 * @param  {Function} callback
 * @param  {Number} time
 * @return {Promise}
 */


exports.timeDelay = timeDelay;

function promiseWrapper(callback) {
  var time = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 130;
  var delay = timeDelay(time);
  return new Promise(function (resolve, reject) {
    return callback(resolve, reject, delay);
  });
}

;
/**
 * Create or change a value of a key, even if has several levels
 * @author @cspilhere
 * @param  {Object} object
 * @param  {String} path
 * @param  {Any} value
 * @return {Object}/{Any}
 */

function deepKey() {
  var object = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
  var path = arguments.length > 1 ? arguments[1] : undefined;
  var value = arguments.length > 2 ? arguments[2] : undefined;
  var paths = path.split('.');
  var newPath = paths.slice(1);
  if (value !== undefined && value !== null) object[paths[0]] = object[paths[0]] || {};

  if (paths.length === 1) {
    if (value !== undefined && value !== null) {
      if (value !== undefined && value !== null) return object[paths[0]] = value;
      return object[paths[0]];
    }

    if (!object) return null;
    if (object.hasOwnProperty(paths[0])) return object[paths[0]];
    return null;
  }

  return deepKey(object[paths[0]], newPath.join('.'), value);
}
/**
 * Check if the object has all keys present in the array
 * @author @cspilhere
 * @param  {Object} object
 * @param  {Array} keys
 * @return {Boolean}
 */


var checkObjectKeys = function checkObjectKeys(object, keys) {
  var filteredKeys = keys.filter(function (key) {
    return deepKey(object, key) === null;
  });
  return filteredKeys.length < 1;
};

exports.checkObjectKeys = checkObjectKeys;
var objectHasKeys = checkObjectKeys; // New name for the checkObjectKeys function

/**
 * @author @cspilhere
 */

exports.objectHasKeys = objectHasKeys;

var goodObject = function goodObject(object, mappedKeys) {
  var filteredKeys = {};
  Object.keys(mappedKeys).forEach(function (key) {
    if (getTypeOf(mappedKeys[key]) === 'Object') {
      var value = deepKey(object, mappedKeys[key].path);
      if (!value) return null;

      if (getTypeOf(mappedKeys[key].format) === 'Function') {
        value = mappedKeys[key].format(value);
      }

      filteredKeys[key] = value;
      return;
    }

    if (deepKey(object, mappedKeys[key]) !== null) {
      filteredKeys[key] = deepKey(object, mappedKeys[key]);
    }
  });
  return _objectSpread({}, object, filteredKeys);
};
/**
 * Return number as string for presentation only
 * @author @cspilhere
 * @param  {Number} number
 * @param  {Number} float
 * @param  {String} locale
 * @return {String} Formated number
 */


exports.goodObject = goodObject;

var fixNumber = function fixNumber(number, float) {
  var locale = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'en-US';
  if (isInvalidNumber(number)) return number;

  if (float >= 0) {
    var newNumber = number.toLocaleString(locale, {
      maximumFractionDigits: float,
      minimumFractionDigits: 0
    });
    return newNumber;
  }

  return number;
};
/**
 * Return any formated number as raw
 * @author @cspilhere
 * @param  {String} number
 * @return {Number} Raw number
 * @todo: Parse english numbers like 1,000.00
 */


exports.fixNumber = fixNumber;

var rawNumber = function rawNumber(number) {
  var regex = /^\s*(?:(\-|)[1-9]\d{0,2}(?:(\.|)\d{3})*|0)(?:,\d{1,2})?$/;

  if (isInvalidNumber(number) && regex.test(number)) {
    number = number.replace(/\./g, '');
    number = number.replace(/,/, '.');
  }

  if (isInvalidNumber(number)) return number;
  number = number * 1;
  return number;
};
/**
 * Return true if the object is empty
 * @param  {Any} object
 * @return {Boolean}
 */


exports.rawNumber = rawNumber;

var objectIsEmpty = function objectIsEmpty(object) {
  return Object.keys(object).length === 0 && object.constructor === Object;
};
/**
 * Return true if some value was expected as a number but is not
 * @author @cspilhere
 * @param  {Number} number
 * @return {Boolean}
 */


exports.objectIsEmpty = objectIsEmpty;

function isInvalidNumber(number) {
  return isNaN(number) || !isFinite(number) || number === Infinity || number === 'Infinity' || number === -Infinity || number === '-Infinity' || number === NaN || number === 'NaN' || number === '∞' || number === '-∞' || number === undefined || number === 'undefined' || number === null || number === 'null';
}

;
/**
 * Return an object with the original number, a formated version, the value and token
 * @author @cspilhere
 * @param  {Number} number
 * @param  {Number} float
 * @param  {String} locale
 * @return {Object} Object with all parts
 */

var prettyNumber = function prettyNumber(number, float, locale) {
  var thousand = 1000;
  var million = 1000000;
  var billion = 1000000000;
  var trillion = 1000000000000;
  var parsedNumber = {};
  var original = number;
  parsedNumber.original = original;

  if (number < thousand) {
    number = String(number);
    if (float) number = fixNumber(number, float, locale);
    parsedNumber.formatted = number;
    parsedNumber.value = number;
    parsedNumber.token = null;
    return parsedNumber;
  }

  if (number >= thousand && number <= 1000000) {
    number = number / thousand;
    if (float) number = fixNumber(number, float, locale);
    parsedNumber.formatted = number + 'k';
    parsedNumber.value = number;
    parsedNumber.token = 'k';
    return parsedNumber;
  }

  if (number >= million && number <= billion) {
    number = number / million;
    if (float) number = fixNumber(number, float, locale);
    parsedNumber.formatted = number + 'MM';
    parsedNumber.value = number;
    parsedNumber.token = 'MM';
    return parsedNumber;
  }

  if (number >= billion && number <= trillion) {
    number = number / billion;
    if (float) number = fixNumber(number, float, locale);
    parsedNumber.formatted = number + 'B';
    parsedNumber.value = number;
    parsedNumber.token = 'B';
    return parsedNumber;
  } else {
    number = number / trillion;
    if (float) number = fixNumber(number, float, locale);
    parsedNumber.formatted = number + 'T';
    parsedNumber.value = number;
    parsedNumber.token = 'T';
    return parsedNumber;
  }
};
/**
 * Parse an object and transform into a string with url query
 * @author @cspilhere
 * @param  {Object} params
 * @param  {Function} customParser
 * @return {String} Url query style
 */


exports.prettyNumber = prettyNumber;

function buildQuery(params, customParser) {
  if (getTypeOf(params) !== 'Object') return '';
  var pathArray = [];
  Object.keys(params).forEach(function (item) {
    if (params[item] !== 'undefined' && params[item] !== 'null' && params[item] !== undefined && params[item] !== null) {
      var query = "".concat(item, "=").concat(params[item]);

      if (customParser && typeof customParser === 'function') {
        // If customParser return null, will use the current query value
        query = customParser(params[item], item) || query;
      }

      pathArray.push(query);
    }
  });
  return "?".concat(pathArray.join('&'));
}

;
/**
 */

var debounce = function debounce(callback, wait, immediate) {
  var timeout, timestamp, result;

  var later = function later() {
    var last = timeNow() - timestamp;

    if (last < wait && last >= 0) {
      timeout = setTimeout(later, wait - last);
    } else {
      timeout = null;

      if (!immediate) {
        result = callback();
      }
    }
  };

  return function () {
    timestamp = timeNow();
    var callNow = immediate && !timeout;
    if (!timeout) timeout = setTimeout(later, wait);

    if (callNow) {
      result = callback();
    }

    return result;
  };
};
/**
 */


exports.debounce = debounce;

function filterArrayBy(array, filterBy, exact) {
  if (typeof filterBy !== 'string') return [];
  var text = (filterBy || '').split(',');
  var newData = [];

  function matchExact(text, string) {
    text = text.trim();
    string = string.trim();
    var match = string.match(text);
    return match != null && string == match[0];
  }

  text.forEach(function (test) {
    test = test.trim(); // if (!test) return;

    newData = newData.concat(array.filter(function (item) {
      return Object.keys(item).some(function (key) {
        if (typeof item[key] === 'string') {
          if (exact) {
            return matchExact(test.toLowerCase(), item[key].toLowerCase());
          }

          return item[key].toLowerCase().indexOf(test.toLowerCase()) > -1;
        }
      });
    }));
  });
  return newData;
}

;
/**
 */

function setFormInitialFocus(formElement) {
  var form = formElement;

  if (form) {
    var formElements = form.elements;

    for (var i = 0; i < formElements.length; i++) {
      if (formElements[i].nodeName !== 'BUTTON') {
        formElements[i].focus();
        break;
      }
    }
  }
}
/**
 */


function copyToClipboard(string) {
  var element = document.createElement('textarea');
  element.value = string;
  element.setAttribute('readonly', '');
  element.style.position = 'absolute';
  element.style.left = '-9999px';
  document.body.appendChild(element);
  element.select();
  document.execCommand('copy');
  document.body.removeChild(element);
}
/**
 */


var currency = function currency(number) {
  if (!isFloat(number) && !isInvalidNumber(number)) {
    number = fixNumber(number, 2, 'pt-BR') + ',00';
  } else {
    number = fixNumber(number, 2, 'pt-BR');
  }

  return number;
};

exports.currency = currency;