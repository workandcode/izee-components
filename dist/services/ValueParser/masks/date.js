"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = value;

function value(value) {
  var locale = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'pt-BR';

  if (locale == 'pt-BR') {
    var split = value.split('-');
    value = split[2] + '/' + split[1] + '/' + split[0];
  }

  if (locale == 'en-EN') {
    var _split = value.split('/');

    value = _split[2] + '-' + _split[1] + '-' + _split[0];
  }

  value = value.toString();
  value = value.replace(/\D/g, '');

  if (locale && locale.trim() == 'pt-BR') {
    if (value.length > 2) value = value.substring(0, 2) + '/' + value.substring(2);
    if (value.length > 5) value = value.substring(0, 5) + '/' + value.substring(5, 9);
  }

  if (locale && locale.trim() == 'en-EN') {
    if (value.length > 4) value = value.substring(0, 4) + '-' + value.substring(4);
    if (value.length > 7) value = value.substring(0, 7) + '-' + value.substring(7, 9);
  }

  return value;
}

;