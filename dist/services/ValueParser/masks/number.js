"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = numbers;

var _utils = require("../../../utils/utils");

function numbers(value) {
  var float = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2;
  var locale = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'pt-BR';
  if (!(0, _utils.isFloat)(value) && !(0, _utils.isInvalidNumber)(value)) value = value + '00';
  if (value.length < 1) return value;
  value = value.toString().replace(/\D/g, '');
  value = decimal(value, float);
  value = value * 1;
  var number = value.toLocaleString(locale && locale.trim() != 'en-EN' ? 'de-DE' : 'en-EN', {
    minimumFractionDigits: float
  });
  return number;
}

;

function decimal(number) {
  var size = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 2;
  var numberString = number + '';
  var regex = new RegExp('([0-9]{' + size + '})$', 'g');

  function zeros() {
    zeros = '';
    var l = size + 1 - numberString.length;

    for (var i = 0; i < l; i++) {
      zeros += '0';
    }

    return zeros;
  }

  numberString = zeros() + numberString;
  numberString = numberString.replace(regex, '.' + '$1');
  return numberString;
}

;