"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = maxLength;

function maxLength(data, maxLenght) {
  if (!data) return;
  return data.toString().substring(0, maxLenght);
}

;