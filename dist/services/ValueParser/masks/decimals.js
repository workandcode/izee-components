"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = decimals;

function decimals(number, decimalsLength, separator) {
  if (!number) return '';
  var numberString = number + '';

  if (numberString.match(/\.[0-9]{5}$/g)) {
    return numberString;
  }

  var regex = new RegExp("([0-9]{".concat(decimalsLength, "})$"), 'g');
  numberString = numberString.replace(/\D/g, '');
  numberString = numberString.replace(/^(0)+/, '');

  function zeros() {
    zeros = '';
    var l = decimalsLength + 1 - numberString.length;

    for (var i = 0; i < l; i++) {
      zeros += '0';
    }

    return zeros;
  }

  numberString = zeros() + numberString;
  numberString = numberString.replace(regex, (separator || '.') + '$1');
  return numberString;
} // decimals