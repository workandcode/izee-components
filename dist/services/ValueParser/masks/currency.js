"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = currency;

function currency(number, separator, type, decimalsLength) {
  if (!number) return '';

  if (separator === '.,' || separator === ',.') {
    var _zeros = function zeros() {
      _zeros = '';
      var l = (decimalsLength || 3) - numberString.length;

      for (var i = 0; i < l; i++) {
        _zeros += '0';
      }

      return _zeros;
    };

    var numberString = number + '';
    numberString = numberString.replace(/\D/g, '');
    numberString = numberString.replace(/^(0)+/, '');
    numberString = _zeros() + numberString;
    numberString = numberString.replace(/([0-9]{2})$/g, separator[1] + '$1');
    numberString = numberString.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1' + separator[0]);
    return (type || '') + ' ' + numberString;
  }
} // currency