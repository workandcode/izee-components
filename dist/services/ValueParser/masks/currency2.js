"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = currency;

function currency(number, type) {
  if (!number) return '';

  var clearSeparators = function clearSeparators(value) {
    value = value.toString();
    return parseFloat(value.replace(/,/g, '.').replace(/\.(?![^.]*$)/g, ''));
  };

  var toIntCents = function toIntCents(number) {
    return Math.abs(parseInt(clearSeparators(number) * 100));
  };

  var toFloatString = function toFloatString(number) {
    return number.toFixed(2);
  };

  function formatDecimal(number, separator) {
    var value = number + '';
    if (separator !== '.,' || separator !== ',.') separator = '.,';
    value = value.replace(/\D/g, '');
    value = value.replace(/([0-9])([0-9]{14})$/, '$1' + separator[0] + '$2');
    value = value.replace(/([0-9])([0-9]{11})$/, '$1' + separator[0] + '$2');
    value = value.replace(/([0-9])([0-9]{8})$/, '$1' + separator[0] + '$2');
    value = value.replace(/([0-9])([0-9]{5})$/, '$1' + separator[0] + '$2');
    value = value.replace(/([0-9])([0-9]{2})$/, '$1' + separator[1] + '$2');
    return value;
  }

  if (type === 'Real') {
    return 'R$ ' + formatDecimal(toIntCents(number));
  }
} // currency