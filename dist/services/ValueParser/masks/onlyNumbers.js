"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = onlyNumbers;

function onlyNumbers(string) {
  if (!string) return false;
  return string.toString().replace(/\D/g, '');
} // onlyNumbers