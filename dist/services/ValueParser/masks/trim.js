"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = trim;

function trim(data) {
  if (!data) return;
  if (typeof data !== 'string') return;
  data = data.replace(/\s/g, '');
  if (data == '') return;
  return data;
} // trim