"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = formatRaw;

function formatRaw(value, type, extra) {
  if (type == 'number') {
    if (value && value.length < 1) return value;
    if (value == undefined) return value;
    value = value.toString();

    if (value.match(/\,/g)) {
      value = value.replace(/\,/g, '!');
      value = value.replace(/\./g, '?');
      value = value.replace(/\!/g, '.');
      value = value.replace(/\?/g, '');
    }

    value = value * 1;
    value = value.toFixed(2);
  }

  if (type == 'date') {
    if (extra == 'pt-BR') {
      var split = value.split('-');
      value = split[2] + '/' + split[1] + '/' + split[0];
    }

    if (extra == 'en-EN') {
      var _split = value.split('/');

      value = _split[2] + '-' + _split[1] + '-' + _split[0];
    }
  }

  if (type == 'datetime') {
    if (extra == 'pt-BR') {
      value = value.toString();
      value = value.trim();

      var _split2 = value.split('-');

      var last = _split2[2].split(' ');

      value = last[0].trim() + '/' + _split2[1] + '/' + _split2[0] + ' ' + last[1];
    }

    if (extra == 'en-EN') {
      var _split3 = value.split('/');

      value = _split3[2] + '-' + _split3[1] + '-' + time[0] + ' ' + time[1];
    }
  }

  return value;
}

;