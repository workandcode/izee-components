"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = camelCaseToDash;

function camelCaseToDash(string) {
  return string.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
} // camelCaseToDash