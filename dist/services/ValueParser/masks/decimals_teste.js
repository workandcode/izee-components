"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = decimals;

function decimals(number, decimalsLength, separator) {
  var maxLength = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 6;
  if (!number) return '';
  var numberString = number + '';
  var part1 = '';
  var part2 = '';

  if (numberString.length > maxLength) {
    if (!numberString.startsWith('0')) {
      return numberString.substring(0, maxLength);
    }

    return numberString.substring(1, maxLength);
  }

  var parts = numberString.split('.');
  if (parts.length < 2) return '0.' + parts[0].replace(/\D/g, '');
  part1 = parts[0].replace(/\D/g, '');
  part2 = parts[1].replace(/\D/g, '');

  if (part2.length > decimalsLength) {
    part1 += part2.charAt(0);
    part2 = part2.slice(1);
  }

  var newValue = part1 + '.' + part2;
  return newValue.replace('.', separator || '.');
} // decimals