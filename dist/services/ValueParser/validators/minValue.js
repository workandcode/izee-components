"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = minValue;

var _Format = _interopRequireDefault(require("../../../Format"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function minValue(value, min) {
  value = (0, _Format.default)().number(value);
  if (!value && value != 0) return false;
  if (value * 1 < min * 1) return false;
  return true;
} // minValue