"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.checkLength = checkLength;

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function checkLength(data, lenght, type) {
  if (_typeof(data) !== type) return false;
  data = data.replace(/[^\d]+/g, '');
  if (data == '') return false;
  if (data.length != lenght) return false;
  return true;
} // checkLength