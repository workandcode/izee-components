"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = equalsTo;

function equalsTo(value, valueToCompare) {
  return value === valueToCompare;
}