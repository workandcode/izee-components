"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = phone;

function phone(phone) {
  var regex = /^(\([0-9]{2}\)\s*|[0-9]{4,5}\-)[0-9]{4,5}-[0-9]{4}$/;
  return regex.test(phone);
} // phone