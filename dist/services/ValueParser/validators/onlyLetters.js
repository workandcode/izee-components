"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = onlyLetters;

function onlyLetters(data) {
  if (!data) return false;
  if (typeof data !== 'string') return false;
  return /^[a-zA-Z]+$/.test(data);
} // onlyLetters