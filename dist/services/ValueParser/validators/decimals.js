"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = decimals;

function decimals(number, max, separator) {
  separator = separator.replace(/\'/g, '');
  if (separator === 'comma') separator = '.';else separator = '.';
  var regex1 = new RegExp("^[0-9]+(".concat(separator, ")?[0-9]{1,").concat(max ? max : 2, "}$"));
  var regex2 = new RegExp('[0-9]');

  if (number && (number.match(regex1) || number.match(regex2))) {
    return true;
  }

  return false;
} // decimals