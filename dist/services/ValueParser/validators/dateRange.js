"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = dateRange;

var _moment = _interopRequireDefault(require("moment"));

var _momentRange = require("moment-range");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var moment = (0, _momentRange.extendMoment)(_moment.default);

function dateRange(date, min, max) {
  function clear(string) {
    string = string + '';
    return string.replace(/\'/g, '');
  }

  function isValidDate(dateString) {
    var regEx = /^\d{4}-\d{2}-\d{2}$/;
    return dateString.match(regEx) != null;
  }

  if (isValidDate(max)) {
    var transformMaxDate = max.split('-');
    max = clear(transformMaxDate[2]) + '/' + clear(transformMaxDate[1]) + '/' + clear(transformMaxDate[0]);
  }

  var parsedDate = null;
  var newMinDate = null;

  if (/^((\d{4})-(\d{2})-(\d{2}))$/.test(min)) {
    parsedDate = min.split('-');
    newMinDate = new Date(clear(parsedDate[0]), clear(parsedDate[1] - 1), clear(parsedDate[2]));
  } else {
    parsedDate = min.split('/');
    newMinDate = new Date(clear(parsedDate[2]), clear(parsedDate[1] - 1), clear(parsedDate[0]));
  }

  var newMaxDate = null;

  if (/^((\d{4})-(\d{2})-(\d{2}))$/.test(max)) {
    parsedDate = max.split('-');
    newMaxDate = new Date(clear(parsedDate[0]), clear(parsedDate[1] - 1), clear(parsedDate[2]));
  } else {
    parsedDate = max.split('/');
    newMaxDate = new Date(clear(parsedDate[2]), clear(parsedDate[1] - 1), clear(parsedDate[0]));
  }

  var start = moment(newMinDate);
  var end = moment(newMaxDate);
  var check = null;
  var range = null;

  if (date.length == 10) {
    var _parsedDate = date.split('/');

    var newDate = new Date(clear(_parsedDate[2]), clear(_parsedDate[1] - 1), clear(_parsedDate[0]));
    check = moment(newDate);
    range = moment.range(start, end);
    return range.contains(check);
  }

  return false;
} // function dateRange