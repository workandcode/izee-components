"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = onlyNumbers;

function onlyNumbers(data) {
  if (!data) return false;
  if (typeof data !== 'string') return false;
  return /^[\d]+$/.test(data);
}

;