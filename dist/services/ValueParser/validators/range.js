"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = range;

function range(data, start, end) {
  var startLength = start || 0;
  var endLength = end || 1000000;
  if (!data) return false;
  if (data.length - 1 < endLength && data.length + 1 > startLength) return true;
  return false;
} // range