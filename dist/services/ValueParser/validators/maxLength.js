"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = maxLenght;

function maxLenght(data, maxLenght) {
  if (!data || !maxLenght) return false;
  data = data.toString();
  if (data.length > maxLenght) return false;
  return true;
} // maxLenght