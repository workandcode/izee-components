"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = isValidvalue;

var _moment = _interopRequireDefault(require("moment"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function isValidvalue(value) {
  var locale = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'pt-BR';
  var isValid = null;

  if (locale && locale.trim() == 'pt-BR') {
    value = (0, _moment.default)(value, 'DD/MM/YYYY HH:mm');
    return (0, _moment.default)(value).isValid();
  }

  if (locale && locale.trim() == 'en-EN') {
    value = (0, _moment.default)(value, 'YYYY-MM-DD HH:mm');
    return (0, _moment.default)(value).isValid();
  }
}

;