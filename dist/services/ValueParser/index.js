"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _Mask = _interopRequireDefault(require("./Mask"));

var _Validator = _interopRequireDefault(require("./Validator"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var defaultOptions = {
  mask: [],
  validator: []
};

var Parser =
/*#__PURE__*/
function () {
  function Parser() {
    _classCallCheck(this, Parser);
  }

  _createClass(Parser, [{
    key: "parse",
    value: function parse(value) {
      var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : defaultOptions;
      var parsedValue = value;
      var isValid = null;
      parsedValue = (0, _Mask.default)(value, options.mask);
      isValid = (0, _Validator.default)(parsedValue, options.validator);
      return {
        value: parsedValue,
        isValid: isValid,
        isEmpty: value ? value.length < 1 : null
      };
    }
  }]);

  return Parser;
}();

;
var ValueParser = new Parser();
var _default = ValueParser;
exports.default = _default;