"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var validators = _interopRequireWildcard(require("./validators"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Validator = function Validator(value, validator) {
  var validatorObject = _objectSpread({}, validators);

  var isValid = null;
  var validations = [];

  var checkValidations = function checkValidations() {
    return validations.filter(function (item) {
      return item === false;
    });
  };

  if (!validator) return isValid;
  validator.forEach(function (item) {
    var validatorResponse = null;

    if (_typeof(item) == 'object') {
      var key = Object.keys(item)[0];
      if (!validatorObject.hasOwnProperty(key)) return;
      validatorResponse = validatorObject[key](value, item[key]);
    }

    if (typeof item == 'string') {
      if (!validatorObject.hasOwnProperty(item)) return;
      validatorResponse = validatorObject[item](value);
    }

    validations.push(validatorResponse);
  });
  isValid = validations.length ? checkValidations().length < 1 : null;
  return isValid;
};

var _default = Validator;
exports.default = _default;