"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var masks = _interopRequireWildcard(require("./masks"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; var ownKeys = Object.keys(source); if (typeof Object.getOwnPropertySymbols === 'function') { ownKeys = ownKeys.concat(Object.getOwnPropertySymbols(source).filter(function (sym) { return Object.getOwnPropertyDescriptor(source, sym).enumerable; })); } ownKeys.forEach(function (key) { _defineProperty(target, key, source[key]); }); } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Mask = function Mask(value, mask) {
  var maskObject = _objectSpread({}, masks);

  var newValue = value;
  if (!mask) return newValue;
  mask.forEach(function (item) {
    if (_typeof(item) == 'object') {
      var key = Object.keys(item)[0];
      if (!maskObject.hasOwnProperty(key)) return;
      var params = [];
      params.push(newValue);

      if (item[key].pop) {
        params = params.concat(item[key]);
      } else {
        params.push(item[key]);
      }

      newValue = maskObject[key].apply(null, params) || '';
    }

    if (typeof item == 'string') {
      if (!maskObject.hasOwnProperty(item)) return;
      newValue = maskObject[item](newValue) || '';
    }
  });
  return newValue;
};

var _default = Mask;
exports.default = _default;