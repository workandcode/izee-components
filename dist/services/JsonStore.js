"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * @class JsonStore
 * @author @cspilhere
 */
var JsonStore =
/*#__PURE__*/
function () {
  function JsonStore() {
    _classCallCheck(this, JsonStore);

    this.storageName = 'json-store';
  }

  _createClass(JsonStore, [{
    key: "createStore",
    value: function createStore(storageName) {
      this.storageName = storageName || this.storageName;

      if (!localStorage.getItem(this.storageName)) {
        localStorage.setItem(this.storageName, serialize({}));
      }
    }
  }, {
    key: "destroy",
    value: function destroy() {
      if (localStorage.getItem(this.storageName)) {
        localStorage.removeItem(this.storageName);
      }
    }
  }, {
    key: "set",
    value: function set(itemKey, value) {
      var store = deserialize(localStorage.getItem(this.storageName));
      store[itemKey] = value;
      localStorage.setItem(this.storageName, serialize(store));
      return store[itemKey];
    }
  }, {
    key: "get",
    value: function get(itemKey) {
      var store = deserialize(localStorage.getItem(this.storageName));
      return store[itemKey];
    }
  }]);

  return JsonStore;
}();

;

function serialize(value) {
  return JSON.stringify(value);
}

function deserialize(value) {
  if (typeof value != 'string') return undefined;

  try {
    return JSON.parse(value);
  } catch (error) {
    return value || undefined;
  }
}

; // Export JsonStore

var Instance = null;
if (!(Instance instanceof JsonStore)) Instance = new JsonStore();
var _default = Instance;
exports.default = _default;