"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getTaskListStatus = getTaskListStatus;
exports.default = getTask;

function getTaskListStatus() {
  var taskList = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var taskCode = arguments.length > 1 ? arguments[1] : undefined;
  var filter = taskList.filter(function (item) {
    return item.code === taskCode;
  });
  var statusArray = [null];
  filter = filter[0];

  if (filter && filter.tasks) {
    filter = filter.tasks.filter(function (item) {
      return !item.status;
    });
    statusArray = filter;
  }

  return statusArray.length === 0;
}

;

function getTask() {
  var taskList = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];
  var taskCode = arguments.length > 1 ? arguments[1] : undefined;
  var subTaskCode = arguments.length > 2 ? arguments[2] : undefined;
  var filter = taskList.filter(function (item) {
    return item.code === taskCode;
  });
  filter = filter[0];

  if (subTaskCode && filter && filter.tasks) {
    filter = filter.tasks.filter(function (item) {
      return item.code === subTaskCode;
    });
    filter = filter[0];
  }

  return filter || {};
}

;