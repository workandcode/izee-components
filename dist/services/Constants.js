"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

/**
 * @class Constants
 * @author @cspilhere
 */
var Constants =
/*#__PURE__*/
function () {
  function Constants() {
    _classCallCheck(this, Constants);

    this.constants = {};
  }

  _createClass(Constants, [{
    key: "get",
    value: function get(key) {
      if (!this) return undefined;
      if (typeof this.constants[key] === 'function') return this.constants[key](this.params);else return this.constants[key];
    }
  }, {
    key: "set",
    value: function set(newConstantKey, value) {
      this.constants[newConstantKey] = value;
    }
  }, {
    key: "setup",
    value: function setup() {
      var _this = this;

      this.constant = function (key) {
        return _this.get(key);
      };
    }
  }]);

  return Constants;
}();

; // Export Constants

var Instance = null;
if (!(Instance instanceof Constants)) Instance = new Constants();
var _default = Instance;
exports.default = _default;