"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _utils = require("../utils/utils");

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var JsonParse =
/*#__PURE__*/
function () {
  function JsonParse() {
    _classCallCheck(this, JsonParse);

    this.json = undefined;
  }

  _createClass(JsonParse, [{
    key: "create",
    value: function create(string) {
      this.json = deserialize(string) || {};
      if ((0, _utils.getTypeOf)(this.json) !== 'Object') this.json = {};
      return this.json;
    }
  }, {
    key: "createInstance",
    value: function createInstance() {
      return new JsonParse();
    } // setProp(prop) {
    //   this.json = {
    //     ...this.json,
    //     ...prop
    //   };
    //   return this.json;
    // };

  }, {
    key: "set",
    value: function set(path, value) {
      (0, _utils.deepKey)(this.json, path, value);
      return this.json;
    }
  }, {
    key: "get",
    value: function get(path) {
      return (0, _utils.deepKey)(this.json, path);
    }
  }, {
    key: "all",
    value: function all() {
      return this.json;
    }
  }, {
    key: "toString",
    value: function toString() {
      return serialize(this.json);
    }
  }]);

  return JsonParse;
}();

;

function serialize(value) {
  return JSON.stringify(value);
}

function deserialize(value) {
  if (typeof value !== 'string') return undefined;

  try {
    return JSON.parse(value);
  } catch (error) {
    return value || undefined;
  }
}

; // Export JsonParse
// let Instance = null;
// if (!(Instance instanceof JsonParse)) Instance = new JsonParse();

var _default = new JsonParse();

exports.default = _default;