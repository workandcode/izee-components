/**
 * @class JsonStore
 * @author @cspilhere
 */

class JsonStore {

  constructor() {
    this.storageName = 'json-store';
  }

  createStore(storageName) {
    this.storageName = storageName || this.storageName;
    if (!localStorage.getItem(this.storageName)) {
      localStorage.setItem(this.storageName, serialize({}));
    }
  }

  destroy() {
    if (localStorage.getItem(this.storageName)) {
      localStorage.removeItem(this.storageName);
    }
  }

  set(itemKey, value) {
    const store = deserialize(localStorage.getItem(this.storageName));
    store[itemKey] = value;
    localStorage.setItem(this.storageName, serialize(store));
    return store[itemKey];
  };

  get(itemKey) {
    const store = deserialize(localStorage.getItem(this.storageName));
    return store[itemKey];
  };

};

function serialize(value) {
  return JSON.stringify(value);
}

function deserialize(value) {
  if (typeof value != 'string') return undefined;
  try {
    return JSON.parse(value);
  } catch (error) {
    return value || undefined;
  }
};

// Export JsonStore
let Instance = null;
if (!(Instance instanceof JsonStore)) Instance = new JsonStore();
export default Instance;
