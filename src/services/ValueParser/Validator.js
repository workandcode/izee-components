import * as validators from './validators';

const Validator = (value, validator) => {

  const validatorObject = { ...validators };

  let isValid = null;

  let validations = [];
  let checkValidations = () => validations.filter((item) => item === false);

  if (!validator) return isValid;

  validator.forEach((item) => {
    let validatorResponse = null;
    if (typeof item == 'object') {
      const key = Object.keys(item)[0];
      if (!validatorObject.hasOwnProperty(key)) return;
      validatorResponse = validatorObject[key](value, item[key]);
    }
    if (typeof item == 'string') {
      if (!validatorObject.hasOwnProperty(item)) return;
      validatorResponse = validatorObject[item](value);
    }
    validations.push(validatorResponse);
  });

  isValid = validations.length ? checkValidations().length < 1 : null;

  return isValid;

};

export default Validator;
