import * as masks from './masks';

const Mask = (value, mask) => {

  const maskObject = { ...masks };

  let newValue = value;

  if (!mask) return newValue;

  mask.forEach((item) => {
    if (typeof item == 'object') {
      const key = Object.keys(item)[0];
      if (!maskObject.hasOwnProperty(key)) return;
      let params = [];
      params.push(newValue);
      if (item[key].pop) {
        params = params.concat(item[key]);
      } else {
        params.push(item[key]);
      }
      newValue = maskObject[key].apply(null, params) || '';
    }
    if (typeof item == 'string') {
      if (!maskObject.hasOwnProperty(item)) return;
      newValue = maskObject[item](newValue) || '';
    }
  });

  return newValue;

};

export default Mask;
