export default function userType(hashData, object) {

  let user = {};
  let userType = null;

  if (object === 'Renter') {
    userType = 'renter';
    user = hashData.renter;
    user.type = userType;
  }

  if (object === 'PropertyOwner') {
    userType = 'property-owner';
    user = hashData['property_owner'];
    user.type = userType;
  }

  if (object === 'Guarantor') {
    userType = 'guarantor';
    user = hashData['guarantor'];
    user.type = userType;
  }

  return user;
}
