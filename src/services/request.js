import axios from 'axios';
import { baseURL } from 'core/constants';

export const request = axios.create({
  baseURL: baseURL
});

request.interceptors.response.use((response) => {
  return response.data;
}, (error) => {
  return Promise.reject(error.response.data);
});
