import React from 'react';
import ReactDOM from 'react-dom';

class CreateTag extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      color: props.color || '#2670FF'
    };
  }

  static getDerivedStateFromProps(props, state) {
    if (props.color !== state.color && !!props.color) {
      return {
        color: props.color
      };
    }
    return null;
  }

  render() {
    return (
      <React.Fragment>
        .has-theme-color {'{'}
          background-color: {this.state.color} !important
        {'}'}
      </React.Fragment>
    );
  }

};

const body = document.body;

class Portal extends React.Component {
  constructor(props) {
    super(props);
    this.element = document.createElement('style');
  }

  componentDidMount() {
    body.appendChild(this.element);
  }

  componentWillUnmount() {
    body.removeChild(this.element);
  }

  render() {
    return ReactDOM.createPortal(
      this.props.children,
      this.element
    );
  }
}

const SetThemeColor = (props) => <Portal><CreateTag {...props} /></Portal>;

export default SetThemeColor;
