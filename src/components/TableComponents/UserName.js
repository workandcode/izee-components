import React from 'react';

import Button from 'components/lib/Button';
import Icon from 'components/lib/Icon';
import Tooltip from 'components/lib/Tooltip';

const UserName = (props) => (
  <div>
    {props.content.name ? (
      <React.Fragment>
        <strong>{props.content.name}</strong>{props.content.is_verified ? (
          <Tooltip description={<span>Locatário<br />pré-aprovado<br />pela Izee</span>}>
            <Icon name="fas fa-sm fa-shield-check" isSuccess />
          </Tooltip>
        ) : null}<br />
      </React.Fragment>
    ) : null}
    <span className="text has-text-grey">{props.content.email}</span>
  </div>
);

UserName.defaultProps = {
  content: {}
};

export default UserName;
