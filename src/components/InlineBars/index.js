import React from 'react';
import PropTypes from 'prop-types';

const InlineBars = (props) => {

  const colors = ['#D678F7', '#7CEC87', '#F3AC76', '#63ADF5', '#BCBFC5', '#D678F7', '#7CEC87', '#F3AC76'];

  let total = 0;

  const length = props.items.length;

  props.items.forEach((item) => total += item.value);

  const items = (props.items || []).map((item, index) => {

    const percent = (item.value * 100) / total;

    return (
      <div className="inline-bars-item" style={{ width: percent + '%' }} key={index}>
        <div className="inline-bars-content">
          <div className="inline-bars-title">{item.name || '-'}</div>
          <div className="inline-bars-value" title={`${Math.round(percent)}% (${item.value})`}>
            {`${Math.round(percent)}% (${item.value})`}
          </div>
        </div>
        <div className="inline-bars-progress" style={{ backgroundColor: colors[index] }} />
      </div>
    );

  });

  return (
    <div className="inline-bars">
      {items}
    </div>
  );

};

InlineBars.propTypes = {};

export default InlineBars;
