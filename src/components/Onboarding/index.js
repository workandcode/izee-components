import React from 'react';
import PropTypes from 'prop-types';

import Swiper from 'swiper';

export default class Onboarding extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      isOpen: false
    };
    this.swiper = null;
  }

  static getDerivedStateFromProps(props, state) {
    if (state.isOpen !== props.isOpen) {
      return {
        isOpen: props.isOpen
      };
    }
    return null;
  }

  componentDidMount() {
    if (
      this.props.isOpen === undefined ||
      this.props.isOpen === true
    ) {
      this.buildSwiper().then((instance) => {
        this.swiper = instance;
        this.forceUpdate();
      });
    }
  }

  componentDidUpdate() {
    if (this.swiper) this.swiper.update();
  }

  componentWillUnmount() {
    if (this.swiper) this.swiper.destroy();
    // delete this.swiper;
  }

  render() {

    let slides = React.Children.toArray(this.props.children).map((slide, index) => {

      const { children, template, isHidden } = slide.props;

      if (isHidden || !this.swiper) return null;

      return (
        <div className="swiper-slide" key={slide.key}>
          <div className="onboarding-step-content">
            {template ? template({
              isActive: this.swiper.activeIndex === index,
              index,
              next: this.swiper.slideNext.bind(this.swiper),
              prev: this.swiper.slidePrev.bind(this.swiper),
              goto: this.swiper.slideTo.bind(this.swiper)
            }) : children}
          </div>
        </div>
      );
    });

    // const isContainerHidden = !this.state.isOpen;
    const isContainerHidden = this.state.isOpen !== undefined && this.state.isOpen !== true;

    if (isContainerHidden) slides = null;

    const onboardingCSSClass = `onboarding${
      !isContainerHidden ? ' is-active' : ''
    }`;

    return (
      <div className={onboardingCSSClass}>

        <div className="onboarding-container">

          <div className="swiper-container">

            <div className="swiper-wrapper">{slides}</div>

            <div className="swiper-pagination swiper-pagination-onboarding" />

          </div>

          {this.props.footer}

        </div>

        <div className="onboarding-background" />

      </div>
    );
  }

  handleClose() {
    this.setState({ isOpen: false });
    if (this.props.onClose) this.props.onClose();
  }

  buildSwiper() {
    return new Promise((resolve) => {
      const swiper = new Swiper('.swiper-container', {
        preventInteractionOnTransition: true,
        shortSwipes: false,
        longSwipes: false,
        simulateTouch: false,
        allowTouchMove: false,
        pagination: {
          el: '.swiper-pagination',
        }
      });
      resolve(swiper);
    });
  }

};

Onboarding.defaultProps = {
  onClose: () => {}
};

Onboarding.propTypes = {
  onClose: PropTypes.func
};

export const Step = (props) => props.children;

Step.displayName = 'Onboarding(Step)';

Onboarding.propTypes = {
  children: PropTypes.arrayOf((propValue, key, componentName, location, propFullName) => {
    if (propValue[key].type.displayName !== Step.displayName) {
      return new Error(
        'Invalid prop `' + propFullName + '` supplied to' +
        ' `' + componentName + '`. Validation failed.'
      );
    }
  }).isRequired
};
