import React from 'react';
import PropTypes from 'prop-types';

const ProcessCircle = (props) => {

  let total = 0;

  props.items.forEach((item) => total += item.count);

  const items = (props.items || []).map((item, index) => {

    const percent = (item.count * 100) / total;

    return (
      <div
        className="progress-component"
        onClick={props.onClick.bind(null, item)}
        onKeyDown={props.onClick.bind(null, item)}
        style={{ cursor: 'pointer' }}
        tabIndex="0"
        role="button"
        key={index}>
        <div id="progress-circle-1" className="progress-circle">
          <svg viewBox="0 0 100 100" className="progress-circle-bar">
            <path
              className="progress-circle-bg"
              d="M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94"
              strokeWidth="3"
              fillOpacity="0"
            />
            <path
              className="progress-circle-fill"
              d="M 50,50 m 0,-47 a 47,47 0 1 1 0,94 a 47,47 0 1 1 0,-94"
              strokeWidth="3"
              fillOpacity="0"
              style={{
                strokeDasharray: '295.416, 295.416',
                strokeDashoffset: 295.416 - (percent * 295.416 / 100)
              }}
            />
          </svg>
          <div className="progress-circle-content">
            <div className="progress-circle-info">
              <h2 className="progress-circle-heading">{item.count}</h2>
            </div>
          </div>
        </div>
        <h3 className="progress-circle-title">
          {item.name || '-'} - <span className="progress-circle-percentual">{item.perc}%</span>
        </h3>
      </div>
    );

  });

  return (
    <div className="progress-container">
      {items}
    </div>
  );

};

ProcessCircle.defaultProps = {
  onClick: () => {}
};

ProcessCircle.propTypes = {
  items: PropTypes.array.isRequired,
  onClick: PropTypes.func
};

export default ProcessCircle;
