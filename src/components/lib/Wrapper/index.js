import React from 'react';
import PropTypes from 'prop-types';

import parsePropsAsBulmaClasses from 'components/lib/_utils/parsePropsAsBulmaClasses';

const Wrapper = (props) => {
  const wrapperCSSClass = `wrapper ${
    parsePropsAsBulmaClasses(props)
  }`;
  return (<div className={wrapperCSSClass}>{props.children}</div>);
};

Wrapper.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.element.isRequired,
    PropTypes.arrayOf(PropTypes.element).isRequired
  ])
};

export default Wrapper;
