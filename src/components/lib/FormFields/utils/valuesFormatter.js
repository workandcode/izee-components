const getValues = (fieldsObject) => {
  let newObject = {};
  const fieldsArray = Object.keys(fieldsObject);
  fieldsArray.forEach((field) => {
    newObject[field] = fieldsObject[field].value;
  });
  return newObject;
};

export default getValues;
