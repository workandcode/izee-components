const invalidFields = (fields) => {
  if (!fields) return;
  let fieldsArray = Object.keys(fields);
  fieldsArray = fieldsArray.filter((field) => fields[field].isValid !== null && !fields[field].isValid);
  return fieldsArray;
};

export default invalidFields;
