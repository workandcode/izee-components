import React from 'react';

import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import moment from 'moment';

import Input, { propsObject as defaultProps } from 'components/lib/FormFields/components/Input';
import Field from 'components/lib/FormFields/components/Field';
import Button from 'components/lib/Button';
import Control from 'components/lib/FormFields/components/Control';
import Icon from 'components/lib/Icon';

import fieldCreator from './fieldCreator';

class FormFieldDate extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      // selected: moment()
    };
    this.inputContainer = React.createRef();
  }

  static getDerivedStateFromProps(props, state) {
    if (props.value.length === 10 && state.selected && state.selected.format('DD/MM/YYYY') !== moment(props.value, 'DD/MM/YYYY').format('DD/MM/YYYY')) {
      return {
        selected: moment(props.value, 'DD/MM/YYYY')
      };
    }
    return null;
  }

  render() {

    moment.locale('pt-BR', { longDateFormat: { L: 'DD/MM/YYYY' } });

    const isValid = this.props.isValid !== null && !this.props.isValid && !this.props.isEmpty;

    return (
      <Field {...this.props} hasAddons style={{ width: 141 }}>
        <Control {...this.props}>
          <span ref={this.inputContainer}>
            <Input
              {...defaultProps(this.props)}
              isDanger={isValid || this.props.forceInvalid}
              isDisabled={this.props.disabledInput}
              style={{ width: 108, borderTopLeftRadius: 4, borderBottomLeftRadius: 4 }}
            />
          </span>
        </Control>
        <DatePicker
          dateFormat="DD/MM/YYYY"
          minDate={this.props.minDate}
          maxDate={this.props.maxDate}
          selected={this.state.selected}
          customInput={(
            <Button
              type="button"
              style={{ borderTopLeftRadius: 0, borderBottomLeftRadius: 0 }}
              isStatic>
              <Icon name="fas fa-calendar-alt" />
            </Button>
          )}
          onChange={this.handleDateChanges.bind(this)}
          className="button is-featured"
          disabled={this.props.isDisabled}
          popperModifiers={{
            offset: {
              enabled: true,
              escapeWithReference: true,
              boundariesElement: 'viewport',
              offset: `${
                this.inputContainer.current ? -this.inputContainer.current.getBoundingClientRect().width + 'px' : null
              }, 0`
            }
          }}
        />
        {this.props.forceInvalid && this.props.errorMessage ? <p className="help is-danger">{this.props.errorMessage}</p> : null}
      </Field>
    );

  }

  handleDateChangesInput(date) {
    this.props.onChange({
      target: {
        value: date.target.value
      }
    });
  }

  handleDateChanges(date) {
    console.log(date);
    this.setState({
      selected: date
    });
    this.props.onChange({
      target: {
        value: moment(date).format('DD/MM/YYYY')
      }
    });
  }

};

export default fieldCreator(FormFieldDate, {
  masks: ['date'],
  validators: [{ 'minLength': 1 }]
});
