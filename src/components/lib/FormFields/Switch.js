import React from 'react';

import Switch, { propsObject as defaultProps } from 'components/lib/FormFields/components/Switch';
import Field from 'components/lib/FormFields/components/Field';
import Control from 'components/lib/FormFields/components/Control';

import fieldCreator from './fieldCreator';

const FormFieldText = (props) => {

  const isValid = props.isValid !== null && !props.isValid && !props.isEmpty;

  return (
    <Field label={props.label}>
      <Control {...props} isLoading={false} hasSwitch>
        <Switch
          {...defaultProps(props)}
          description={props.description}
          info={props.info}
          isLoading={props.isLoading}
          isDanger={isValid || props.forceInvalid}
        />
      </Control>
      {props.forceInvalid && props.errorMessage ? <p className="help is-danger">{props.errorMessage}</p> : null}
    </Field>
  );
};

export default fieldCreator(FormFieldText, {
  masks: [],
  validators: [{ 'minLength': 1 }],
  valueType: 'boolean'
});
