import React from 'react';

import _propsObject from './propsObject';

import parsePropsAsBulmaClasses from 'components/lib/_utils/parsePropsAsBulmaClasses';

const Input = (props) => {
  const inputCSSClass = `input ${
    parsePropsAsBulmaClasses(props)
  }`;
  return (
    <input
      className={inputCSSClass}
      {..._propsObject(props)}
      pattern={'[0-9]{2}/[0-9]{2}/[0-9]{4}'}
    />
  );
};

export const propsObject = _propsObject;

export default Input;
