import React from 'react';
import PropTypes from 'prop-types';

import parsePropsAsBulmaClasses from 'components/lib/_utils/parsePropsAsBulmaClasses';

const Level = (props) => {
  const levelCSSClass = `level ${
    parsePropsAsBulmaClasses(props)
  }`;
  return (
    <div className={levelCSSClass}>
      {props.children}
    </div>
  );
};

Level.Item = (props) => {
  const levelCSSClass = `level-item ${
    parsePropsAsBulmaClasses(props)
  }`;
  return (
    <div className={levelCSSClass}>
      <div>
        {props.children}
      </div>
    </div>
  );
};

Level.propTypes = {
  children: PropTypes.any.isRequired
};

export default Level;
