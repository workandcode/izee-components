import React from 'react';

// Utils
import parsePropsAsBulmaClasses from 'components/lib/_utils/parsePropsAsBulmaClasses';

const FlexBar = (props) => {
  let appBarCSSClass = `appbar ${
    parsePropsAsBulmaClasses(props)
  }${
    props.className ? ' ' + props.className : ''
  }`;
  return (
    <div data-spec-selector={props.specSelector} className={appBarCSSClass}>
      {props.children}
    </div>
  );
};

FlexBar.Child = (props) => {
  let appBarChildCSSClass = `appbar-child ${
    parsePropsAsBulmaClasses(props)
  }${
    props.className ? ' ' + props.className : ''
  }`;
  return (
    <div
      data-spec-selector={props.specSelector}
      className={appBarChildCSSClass}
      style={props.style}>
      {props.children}
    </div>
  );
};

export default FlexBar;
