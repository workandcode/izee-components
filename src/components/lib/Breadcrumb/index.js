import React from 'react';
import PropTypes from 'prop-types';

const Breadcrumb = (props) => {
  const breadcrumbItems = props.path.map((item, index, array) => (
    <li className={index === array.length ? ' is-active' : ''} key={index}>
      <a onClick={props.onClick.bind(null, item)}>{item}</a>
    </li>
  ));
  return (
    <nav className="breadcrumb" aria-label="breadcrumbs">
      <ul>{breadcrumbItems}</ul>
    </nav>
  );
};

Breadcrumb.propTypes = {
  onClick: PropTypes.func,
  path: PropTypes.array
};

Breadcrumb.defaultProps = {
  onClick: () => {},
  path: []
};

export default Breadcrumb;
