import React from 'react';
import PropTypes from 'prop-types';

import Button from 'components/lib/Button';
import Icon from 'components/lib/Icon';

import parsePropsAsBulmaClasses from 'components/lib/_utils/parsePropsAsBulmaClasses';

export default class ContextMenu extends React.Component {

  constructor() {
    super();
    this.state = {
      isActive: false
    };
    this.container = React.createRef();
    this.handleClickOutside = this.handleClickOutside.bind(this);
  }

  componentDidMount() {
    document.addEventListener('click', this.handleClickOutside, true);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClickOutside, true);
  }

  render() {
    const contextMenuCSSClass = `context-menu is-visible${
      this.state.isActive ? ' is-active' : ''
    }${
      ' ' + parsePropsAsBulmaClasses(this.props)
    }`;
    return (
      <div className={contextMenuCSSClass} ref={this.container} role="menu">
        <button className="context-menu-button has-theme-color"
          onClick={this.handleToggleState.bind(this)}>
          <Icon name="far fa-md fa-plus" />
        </button>
        <div className="context-menu-content">
          {this.props.children}
        </div>
      </div>
    );
  }

  handleClickOutside(event) {
    const container = this.container.current;
    if (container && !container.contains(event.target)) this.closeContextMenu();
  }

  handleToggleState() {
    if (React.Children.toArray(this.props.children).length < 1) {
      if (this.props.onClick) this.props.onClick();
      return;
    }
    this.setState({ isActive: !this.state.isActive });
  }

  closeContextMenu() {
    if (this.state.isActive) this.setState({ isActive: false });
  }

};

ContextMenu.defaultProps = {
  children: null,
  onClick: null
};

ContextMenu.propTypes = {
  onClick: PropTypes.func,
  children: PropTypes.any
};
