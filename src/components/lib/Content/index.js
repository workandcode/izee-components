import React from 'react';
import PropTypes from 'prop-types';

import { classProps } from 'utils/utils';

const Content = (props) => (
  <div className="content-wrapper">
    {props.children}
  </div>
);

Content.Header = (props) => (
  <header className="content-header">
    {props.children}
  </header>
);

Content.Body = (props) => (
  <div className={'content-body ' + props.className}>
    {props.children}
  </div>
);

Content.propTypes = {
  children: PropTypes.any.isRequired
};

export default Content;
