import React from 'react';

import Header from 'components/lib/Header';
import Container from 'components/lib/Container';

const SectionWrapper = (props) => (
  <section className="section" style={props.style}>
    <Container isMedium={!props.isMedium}>
      {props.header ? (
        <Header isTiny isUppercase subtitle={<hr style={{ marginTop: '1rem' }} />}>
          {props.header}
          <div>{props.headerRight}</div>
        </Header>
      ) : null}
      {props.children}
    </Container>
  </section>
);

export default SectionWrapper;
