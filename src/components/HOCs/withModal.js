import React from 'react';
import ReactTimeout from 'react-timeout';

import Modal from 'components/lib/Modal';

export default function withModal(Component) {

  class WithModalWrapper extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        modalTitle: '',
        modalIsOpen: false,
        component: null
      };
    }

    render() {
      return (
        <React.Fragment>

          <Component
            {...this.props}
            openModal={this.openModal.bind(this)}
            closeModal={this.closeModal.bind(this)}
          />

          <Modal
            title={this.state.modalTitle}
            isOpen={this.state.modalIsOpen}
            onClose={this.closeModal.bind(this)}>
            {this.state.component}
          </Modal>

        </React.Fragment>
      );
    }

    openModal(description, component, isOpen = true) {
      this.setState({
        modalTitle: description,
        modalIsOpen: isOpen,
        component: component
      });
    }

    closeModal() {
      this.setState({
        modalTitle: null,
        modalIsOpen: false,
        component: null
      });
    }

  };

  WithModalWrapper.displayName = `WithModalWrapper(${getDisplayName(Component)})`;

  return ReactTimeout(WithModalWrapper);

};

function getDisplayName(Component) {
  return Component.displayName || Component.name || 'Component';
};
