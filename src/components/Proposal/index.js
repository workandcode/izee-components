import React from 'react';

// Components
import Grid from 'components/lib/Grid';
import Text from 'components/lib/Text';
import Space from 'components/lib/Space';

import Address from './components/Address';
import PropertyInfo from './components/PropertyInfo';
import PersonalData from './components/PersonalData';
import PersonalDataSpouse from './components/PersonalDataSpouse';
import CompanyData from './components/CompanyData';
import Evaluation from './components/Evaluation';
import Documents from './components/Documents';
import Negotiation from './components/Negotiation';
import UserName from './components/UserName';
import Section from './components/Section';

import { currency, date } from './components/utils';

import withFilePreview from 'components/HOCs/withFilePreview';

class Proposal extends React.Component {

  getExpirationDateTitle(item) {
    if (!item || !item.operation_tokens[0]) return null;

    return `Data de Expiração: ${item.operation_tokens[0].expiration_date}`
  }

  render() {

    let current = this.props.data;
    if (!current) return null;
    if (!current.properties) return null;

    const requestPath = this.props.requestPath;

    const hasClick = typeof this.props.onClickResend === 'function' && typeof this.props.onClickCopy === 'function';

    const rentValue = currency(current['rent_value']);
    const ownershipDate = date(current['ownership_date']);
    const negotiation = current['negotiation'];
    const guarantee = current['guarantee'];
    const guaranteeName = guarantee ? guarantee.name : 'A garantia ainda não foi escolhida';
    const property = current.properties[0];

    const owners = current['property_owners'].map((item, index) => {
      const shoResend = hasClick && item.evaluation != 2;

      return (
        <React.Fragment key={item.id}>
          <div style={{ borderTop: index > 0 && '1px solid #f2f2f2', margin: index > 0 && '2px 0 15px' }} />
          <Grid>
            <Grid.Col>
              <UserName
                {...item}
                onClickResend={shoResend && this.props.onClickResend.bind(null, 'property_owner_ids', item)}
                onClickCopy={shoResend && this.props.onClickCopy.bind(this, item)}
                title={this.getExpirationDateTitle(item)}
              />
              {item.cnpj && item.cnpj !== "" ? (
                <div>

                  <PersonalData {...item} brief />

                  {item.spouse_name && item.spouse_name !== "" && (
                    <div>
                      <PersonalDataSpouse {...item} brief />
                    </div>)}

                  <Space isSmall />

                  <Text><strong><span style={{ fontSize: 14 }}>{item.trading_name}</span></strong></Text>
                  <CompanyData {...item} brief />

                </div>) :
                (<div>
                  <PersonalData {...item} brief />

                  {item.spouse_name && item.spouse_name !== "" && (
                    <div>
                      <PersonalDataSpouse {...item} brief />
                    </div>)}

                </div>)
              }
            </Grid.Col>
            <Grid.Col>
              <Address address={item.address} />
            </Grid.Col>
          </Grid>
          <Grid>
            <Grid.Col>
              <Evaluation status={item.evaluation} comment={item.denial_comment} />
            </Grid.Col>
          </Grid>
        </React.Fragment>
      );
    });

    const renters = current.renters.map((item, index) => (
      <React.Fragment key={item.id}>
        <div style={{ borderTop: index > 0 && '1px solid #f2f2f2', margin: index > 0 && '2px 0 15px' }} />
        <Grid>
          <Grid.Col>
            <UserName
              {...item}
              onClickResend={hasClick && this.props.onClickResend.bind(null, 'renter_ids', item)}
              onClickCopy={hasClick && this.props.onClickCopy.bind(this, item)}
              title={this.getExpirationDateTitle(item)}
            />
            {item.cnpj && item.cnpj !== "" ? (
                <div>

                  <PersonalData {...item} brief />

                  {item.spouse_name && item.spouse_name !== "" && (
                    <div>
                      <PersonalDataSpouse {...item} brief />
                    </div>)}

                  <Space isSmall />

                  <Text><strong><span style={{ fontSize: 14 }}>{item.trading_name}</span></strong></Text>
                  <CompanyData {...item} brief />

                </div>) :
                (<div>
                  <PersonalData {...item} brief />

                  {item.spouse_name && item.spouse_name !== "" && (
                    <div>
                      <PersonalDataSpouse {...item} brief />
                    </div>)}

                </div>)
              }
          </Grid.Col>
          <Grid.Col>
            <Address address={item.address} />
          </Grid.Col>
        </Grid>
        <Grid>
          <Grid.Col>
            <Documents requestPath={requestPath} list={item.documents} onClickFile={this.props.openFile} />
            {item.main && <Evaluation status={item.evaluation} comment={item.denial_comment} />}
          </Grid.Col>
        </Grid>
      </React.Fragment>
    ));

    const guarantors = (current.guarantors || []).map((item, index) => (
      <React.Fragment key={item.id}>
        <div style={{ borderTop: index > 0 && '1px solid #f2f2f2', margin: index > 0 && '2px 0 15px' }} />
        <Grid>
          <Grid.Col>
            <UserName
              {...item}
              onClickResend={hasClick && this.props.onClickResend.bind(null, 'renter_ids', item)}
              onClickCopy={hasClick && this.props.onClickCopy.bind(this, item)}
              title={this.getExpirationDateTitle(item)}
            />
            {item.cnpj && item.cnpj !== "" ? (
                <div>

                  <PersonalData {...item} brief />

                  {item.spouse_name && item.spouse_name !== "" && (
                    <div>
                      <PersonalDataSpouse {...item} brief />
                    </div>)}

                  <Space isSmall />

                  <Text><strong><span style={{ fontSize: 14 }}>{item.trading_name}</span></strong></Text>
                  <CompanyData {...item} brief />

                </div>) :
                (<div>
                  <PersonalData {...item} brief />

                  {item.spouse_name && item.spouse_name !== "" && (
                    <div>
                      <PersonalDataSpouse {...item} brief />
                    </div>)}

                </div>)
              }
          </Grid.Col>
          <Grid.Col>
            <Address address={item.address} />
          </Grid.Col>
        </Grid>
        <Grid>
          <Grid.Col>
            <Documents requestPath={requestPath} list={item.documents} onClickFile={this.props.openFile} />
            <Evaluation status={item.evaluation} comment={item.denial_comment} />
          </Grid.Col>
        </Grid>
      </React.Fragment>
    ));

    return (
      <div ref={this.props.ref}>

        <Section
          title={
            `Imóvel ${property.type ? `(${property.type.name})` : ''}${property.name ? ` // Apelido: ${property.name}` : ''}`
          }
          iconName="home">
          <Address address={property.address} />
        </Section>

        <Section title="Proprietários" iconName="user">{owners}</Section>

        <Section title="Locatários" iconName="user">{renters}</Section>

        <Section title={'Garantia ' + (guaranteeName && `(${guaranteeName})`)} iconName="shield-check">
          {guarantors.length ? guarantors : null}
        </Section>

        <Section title={`Valor do aluguel: R$ ${rentValue}`} iconName="file-invoice-dollar" >
          <PropertyInfo property={property} />
        </Section>

        {ownershipDate && <Section title={`Data da posse: ${ownershipDate}`} iconName="calendar-alt" />}

        <Section title="Negociação" iconName="asterisk">
          <Negotiation>{negotiation}</Negotiation>
        </Section>

      </div>
    );

  }

};

export default withFilePreview(Proposal);
