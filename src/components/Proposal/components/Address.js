import React from 'react';
import Text from 'components/lib/Text';
import { area } from './utils';

const Address = ({ address, inline }) => {

  if (!address) return null;

  if (inline) {
    return (
      <div>
        <Text>
          <strong>Endereço:</strong>&nbsp;
          {address.address}, {address.number}&nbsp;
          {address.neighborhood} - {address.zip_code}&nbsp;
          {address.city}/{address.state}&nbsp;
          - {address.additional_address}
        </Text>
      </div>
    );
  }

  return (
    <div>
      <Text><strong><span style={{ fontSize: 14 }}>Endereço</span></strong></Text>
      <Text>{address.address}, {address.number}</Text>
      <Text>{address.neighborhood} - {address.zip_code}</Text>
      <Text>{address.city} / {address.state}</Text>
      <Text>{address.additional_address}</Text>
      {address.area && <Text>Área: {area(address.area)} m²</Text>}
    </div>
  );
};

export default Address;
