import React from 'react';
import Text from 'components/lib/Text';
import { currency, date } from './utils';

const PersonalData = (props) => {

  const income = currency(props['income']);
  const estimatedRentValue = currency(props['estimated_rent_value']);

  const birthDate = date(props['birth_date']);
  const expeditionDate = date(props['rg_expedition_date']);

  const { brief } = props;

  return (
    <React.Fragment>
      {props.is_student && !brief && (
        <div style={{ marginBottom: 5 }}><span className="tag is-warning is-small">Estudante</span></div>
      )}
      <Text>Email: {props.email} / Telefone: {props.phone}</Text>
      <Text>Documento: {props.identity_type} / Número: {props.identity}</Text>
      <Text>CPF: {props['cpf']}</Text>
      {!brief && <Text>Data de nascimento: {birthDate}</Text>}
      {!brief && income && <Text>Renda: R$ {income}</Text>}
      {!brief && estimatedRentValue && <Text>Valor estimado do aluguel: R$ {estimatedRentValue}</Text>}
      {!brief && <Text>Estado civil: {(props['marital_status'] || {}).name}</Text>}
      {!brief && <Text>Ocupação: {props['occupation']}</Text>}
      {!brief && props['nationality'] && <Text>Nacionalidade: {props['nationality']}</Text>}
    </React.Fragment>
  );
};

export default PersonalData;
