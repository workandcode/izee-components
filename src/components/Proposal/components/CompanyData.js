import React from 'react';
import Text from 'components/lib/Text';

const CompanyData = (props) => {

  const { brief } = props;

  return (
    <React.Fragment>
      {props.is_student && !brief && (
        <div style={{ marginBottom: 5 }}><span className="tag is-warning is-small">Estudante</span></div>
      )}
      <Text>Email: {props.email}</Text>
      <Text>CNPJ: {props['cnpj']} </Text>
      {!brief && <Text>Razão Social: {props ['trading_name']}</Text>}
      {!brief && <Text>Inscrição Municipal: {props ['municipal_enrollment']}</Text>}
      {!brief && <Text>Inscrição Estadual: {props ['state_enrollment']}</Text>}
    </React.Fragment>
  );
};

export default CompanyData;
