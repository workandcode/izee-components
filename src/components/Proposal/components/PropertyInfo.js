import React from 'react';
import Text from 'components/lib/Text';
import { currency } from './utils';

const PropertyInfo = ({ property }) => {

  if (!property) return null;

  let condoFee = currency(property.condoFee);
  let iptu = currency(property.iptu);

  return (
    <div>
      {condoFee && <Text>Condomínio (Mensal aproximado): R$ {condoFee}</Text>}
      {iptu && <Text>IPTU (Aproximado): R$ {iptu}</Text>}
      {property.parking_spaces && property.parking_spaces.length ? <Text>Vagas de garagem: {property.parking_spaces.join(', ')}</Text> : null}
      {property.hobby_boxes && property.hobby_boxes.length ? <Text>Hobby Box: {property.hobby_boxes.join(', ')}</Text> : null}
    </div>
  );
};

export default PropertyInfo;
