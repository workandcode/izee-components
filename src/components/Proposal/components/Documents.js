import React from 'react';
import Icon from 'components/lib/Icon';
import Button from 'components/lib/Button';
import Text from 'components/lib/Text';

const Documents = ({ list, onClickFile, requestPath }) => {

  let documents = null;

  documents = list.map((file, index) => (
    <React.Fragment key={file.id}>
      <Button
        isLink
        onClick={onClickFile.bind(null, file, requestPath)}>
        {file.name}
      </Button>{(index + 1) < list.length ? ', ' : ''}
    </React.Fragment>
  ));

  return (
    <div style={{ marginLeft: -4, marginTop: -8 }}>
      <Text>
        <Icon name="fas fa-sm fa-id-card" />
        Documentos:&nbsp;
        <span style={{ bottom: 0, position: 'relative' }}>
          {documents.length ? documents : 'Nenhum documento foi enviado.'}
        </span>
      </Text>
    </div>
  );
};

export default Documents;
