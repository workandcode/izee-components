import React from 'react';
import Text from 'components/lib/Text';
import Button from 'components/lib/Button';

class ButtonWrapper extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      showMessage: false
    }
  }

  onClick() {
    if(this.props.onClick()) {
      this.setState({showMessage: true}); 
      setTimeout(() => { this.setState({showMessage: false}) }, 1000);
    }
  }

  render() {
    return (
      <Button
        isBare
        onClick={this.onClick.bind(this)}
        style={{ lineHeight: 1.35 }}>
        <span style={{ color: '#363636', marginLeft: 0 }}>{this.props.children}</span>&nbsp;
        <span title={this.props.title} style={{ color: '#2b71eb', fontSize: 12  }}><strong>{this.props.onClick && this.props.value}{this.state.showMessage && this.props.message}</strong></span>
      </Button>
    );
  };
};

const Name = (props) => {

  const isMain = props.main;

  const hasCallback = props.onClickResend;
  const hasToken = props.onClickCopy;

  const formatedName = (
    <strong>
      <span style={{ fontSize: 14 }}>{props.name} {isMain && '(Principal)'}</span>
    </strong>
  );

  const nameComponent = hasCallback ? <ButtonWrapper onClick={props.onClickResend} value="(Reenviar)">{formatedName}</ButtonWrapper> : formatedName;
  const copyComponent = hasToken ? <ButtonWrapper onClick={props.onClickCopy} title={props.title} value="(Copiar Link)" message=" Copiado!"></ButtonWrapper> : ''

  return (
    <Text>{nameComponent}&nbsp;{copyComponent}&nbsp;{props.children}</Text>
  );
};

export default Name;
