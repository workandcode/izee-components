import React from 'react';
import Text from 'components/lib/Text';
import Space from 'components/lib/Space';
import { currency, date } from './utils';

const PersonalDataSpouse = (props) => {

    const birthDate = date(props['birth_date']);

    const { brief } = props;

    return (
        <React.Fragment>
            <Space isSmall />

            <Text><strong><span style={{ fontSize: 14 }}>{props.spouse_name} (Cônjuge)</span></strong></Text>
            <Text>Email: {props.spouse_email} / Telefone: {props.spouse_phone}</Text>
            <Text>Documento: {props.spouse_identity_type} / Número: {props.spouse_identity}</Text>
            <Text>CPF: {props['spouse_cpf']}</Text>
            {!brief && <Text>Data de nascimento: {spouse_birthDate}</Text>}
            {!brief && <Text>Ocupação: {props['spouse_occupation']}</Text>}
            {!brief && props['spouse_nationality'] && <Text>Nacionalidade: {props['nationality']}</Text>}
        </React.Fragment>
    );
};

export default PersonalDataSpouse;