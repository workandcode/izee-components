import React from 'react';
// import PropTypes from 'prop-types';

import AsyncSelect from 'react-select/lib/Async';
import { components } from 'react-select';

import Icon from 'components/lib/Icon';

// https://deploy-preview-2289--react-select.netlify.com/home

import { request } from 'core/constants';

export default class GlobalSearch extends React.Component {

  constructor(props) {
    super(props);
    this.state = { inputValue: '' };
  }

  render() {

    const promiseOptions = (inputValue) => new Promise((resolve) => {
      request.get('{company}/property?search=' + inputValue)
        .then((response) => {
          console.log(response);
          resolve([]);
        })
        .catch(({ response }) => {
          // console.log(response);
        });
    });

    const DropdownIndicator = (props) => (
      <components.DropdownIndicator {...props}>
        <Icon name="fas fa-search" />
      </components.DropdownIndicator>
    );

    const Option = (props) => (
      <div>
        <components.Option {...props} />
      </div>
    );

    return (
      <React.Fragment>
        <div className="react-select-wrapper">
          <AsyncSelect
            classNamePrefix="react-select"
            cacheOptions
            defaultOptions
            components={{ DropdownIndicator, Option }}
            loadOptions={promiseOptions}
          />
        </div>
      </React.Fragment>
    );
  }

  handleInputChange(newValue) {
    const inputValue = newValue.replace(/\W/g, '');
    this.setState({ inputValue });
    return inputValue;
  };

};

GlobalSearch.propTypes = {};
