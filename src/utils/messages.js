import React from 'react';

export const dict = {
  common: {
    welcome: 'Bem vindo'
  },
  formFields: {
    password: {
      minLength: 'A senha deve conter de 6 á 16 caracteres'
    }
  },
  login: {
    welcome: 'Olá, bem vindo!',
    processing: 'Efetuando login, aguarde.',
    success: 'Login efetuado com sucesso! Vamos redirecionar você...',
    error: 'Login e/ou senha inválidos.'
  },
  inviteUser: {
    alreadyInvited: 'Cadastrado como: ${name} Empresa: ${company}'
  },
  createCompany: {
    welcomeNewUser: 'Olá ${name} agora cadastre sua empresa!',
    invalidInvite: 'Este convite já foi utilizado ou é inválido.'
  }
};

export const messages = (messageKey, string, isReact) => {
  let message = deepKey(dict, messageKey);
  string.forEach((item) => {
    message = message.replace(/\$\{[a-z]+\}/, item);
  });
  if (isReact) {
    return (
      <span dangerouslySetInnerHTML={{ __html: message }} />
    );
  }
  return message;
};

export const DictComponent = (props) => (
  <React.Fragment>
    {messages(props.path, props.string)}
  </React.Fragment>
);

function deepKey(object, path, value) {
  let paths = path.split('.');
  let newPath = paths.slice(1);
  if (value) object[paths[0]] = object[paths[0]] || {};
  if (paths.length === 1) {
    if (value !== undefined) {
      if (value) return object[paths[0]] = value;
      return object[paths[0]];
    }
    if (!object) return null;
    if (object.hasOwnProperty(paths[0])) return object[paths[0]];
    return null;
  }
  return deepKey(object[paths[0]], newPath.join('.'), value);
}
