import React from 'react';
import { Switch, Route } from 'react-router-dom';

import RouterParams from './RouterParams';

const ChildRoutes = (props) => {
  if (props.match) {
    Object.keys(props.match.params).forEach((paramKey) => {
      RouterParams.setParam(paramKey, props.match.params[paramKey]);
    });
    RouterParams.setup();
  }
  const routes = (props.routes || []).map((route, index) => (
    <Route
      key={route.key || index}
      location={props.location}
      exact={route.exact}
      path={route.path}
      render={(routeProps) => (
        <route.component
          {...props}
          {...routeProps}
          {...route}
        />
      )}
    />
  ));
  return (
    <Switch location={props.location}>
      {routes}
    </Switch>
  );
};

export default ChildRoutes;
