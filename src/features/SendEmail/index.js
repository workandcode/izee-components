import React from 'react';
import PropTypes from 'prop-types';

// Components
import {
  FormFieldText
} from 'components/lib/FormFields';
import Grid from 'components/lib/Grid';
import Icon from 'components/lib/Icon';
import SubmitButton from 'components/SubmitButton';
import formCreator, { fieldProps } from '../components/formCreator';
import Button from 'components/lib/Button';
import UserSelection from 'features/components/fieldsGroup/UserSelection';

export class RejectDocument extends React.Component {

  componentDidMount() {
    this.props.createRequest(this.createRequest, true);
  }

  render() {

    const { formIsWorking, formIsReady } = this.props;

    return (
      <React.Fragment>

        <Grid>
          <Grid.Col>
            <UserSelection {...this.props} />
          </Grid.Col>
          <Grid.Col isNarrow isFlexBottom>
            <Button
              hasThemeColor
              isLoading={formIsWorking}
              isDisabled={!formIsReady || formIsWorking}
              style={{ height: 42 }}
              submit>
              Enviar
            </Button>
          </Grid.Col>
          <Grid.Col isNarrow isFlexBottom>
            <Button
              isTab
              isLoading={formIsWorking}
              isDisabled={!formIsReady || formIsWorking}
              onClick={this.props.onClickLog}
              style={{ height: 42, width: 40 }}>
              <Icon name="fas fa-clipboard-list" />
            </Button>
          </Grid.Col>
        </Grid>

      </React.Fragment>
    );
  }

  createRequest(fields) {
    // fields.email = fields.users[0].email;
    // delete fields.users;
    this.props.submit(`{company}/proposal/${this.props.proposalId}/send-public-token`, fields);
  }

};

RejectDocument.defaultProps = {};

RejectDocument.propTypes = {};

export default formCreator(RejectDocument);
