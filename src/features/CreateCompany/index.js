import React from 'react';

// Components
import Button from 'components/lib/Button';
import Field from 'components/lib/FormFields/components/Field';
import Control from 'components/lib/FormFields/components/Control';
import Space from 'components/lib/Space';

import formCreator from '../components/formCreator';
import CompanyName from '../components/fieldsGroup/CompanyName';

export class CreateCompany extends React.Component {

  componentDidMount() {
    this.props.createRequest(this.createRequest);
  }

  render() {

    const { formIsWorking, formIsReady } = this.props;

    return (
      <React.Fragment>

        <CompanyName {...this.props} />

        <Space isSmall />

        <Field isCenter>
          <Control>
            <Button
              isFeatured
              isWide
              isLarge
              isLoading={formIsWorking}
              isDisabled={!formIsReady || formIsWorking}
              submit>
              Continuar
            </Button>
          </Control>
        </Field>

      </React.Fragment>
    );
  }

  createRequest(fields) {
    this.props.submit('company', fields);
  }

};

export default formCreator(CreateCompany);
