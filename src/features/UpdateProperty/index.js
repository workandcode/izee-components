import React from 'react';

import moment from 'moment';

// Components
import SubmitButton from 'components/SubmitButton';
import Grid from 'components/lib/Grid';
import { FormFieldText } from 'components/lib/FormFields';
import formCreator from '../components/formCreator';
import PersonalData from '../components/fieldsGroup/PersonalData';
import FullAddress from '../components/fieldsGroup/FullAddress';
import PropertyMeta from '../components/fieldsGroup/PropertyMeta';
import PropertySelection from '../components/fieldsGroup/PropertySelection';
import { fieldProps } from '../components/formCreator';

// Utils
import { goodObject, rawNumber } from 'utils/utils';

export class UpdateProperty extends React.Component {

  componentDidMount() {
    this.props.createRequest(this.createRequest);
  }

  render() {

    const { formIsWorking, formIsReady } = this.props;

    return (
      <React.Fragment>

        <Grid>
          <Grid.Col isNarrow>
            <FormFieldText
              {...fieldProps(this.props, 'code')}
              label="Código"
              noValidate
              style={{ width: 100 }}
            />
          </Grid.Col>
          <Grid.Col>
            <FormFieldText
              {...fieldProps(this.props, 'name')}
              label="Apelido"
              noValidate
            />
          </Grid.Col>
        </Grid>

        <FullAddress {...this.props} />

        <PropertyMeta {...this.props} noValidate />

        <SubmitButton
          label="Salvar"
          isLoading={formIsWorking}
          isDisabled={!formIsReady || formIsWorking}
        />

      </React.Fragment>
    );
  }

  createRequest(fields) {
    console.log(fields);
    let goodFields = goodObject(fields, {
      'condo_fee': {
        path: 'condo_fee',
        format: (value) => rawNumber(value)
      },
      'iptu': {
        path: 'iptu',
        format: (value) => rawNumber(value)
      }
    });
    this.props.submit(`{company}/property/${this.props.defaultValue.id}`, goodFields, 'put');
  }

};

export default formCreator(UpdateProperty);



  // //

  // const string = 'Lets build something';
  // ['awesome', 'modern', 'nasty'].map((word) => {
  //   return `${str} ${word}`;
  // });

  // //

  // const array = ['watermellon', 'banana', 'kiwi', 'strawberry'];
  // const suruba = array.join('+');

  // //

  // const coffee = new Promise((resolve) => {
  //   const hasSuggar = false;
  //   setTimeout(() => resolve(hasSuggar), 50000);
  // });
  // coffee.then((hasSuggar) => {
  //   if (!hasSuggar) return 'Ummmmmm...';
  // });
