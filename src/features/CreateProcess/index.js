import React from 'react';

// Components
import SubmitButton from 'components/SubmitButton';
import Grid from 'components/lib/Grid';

import formCreator from 'features/components/formCreator';
import { fieldProps } from 'features/components/formCreator';

import { FormFieldText } from 'components/lib/FormFields';
import PropertySelection from 'features/components/fieldsGroup/PropertySelection';
import PropertyOwnerSelection from 'features/components/fieldsGroup/PropertyOwnerSelection';


export class CreateProcess extends React.Component {

  componentDidMount() {
    this.props.createRequest(this.createRequest, true);
  }

  render() {

    const { formIsWorking, formIsReady } = this.props;

    return (
      <React.Fragment>

        <Grid>
          <Grid.Col>
            <PropertySelection />
          </Grid.Col>
          <Grid.Col>
            <PropertyOwnerSelection />
          </Grid.Col>
        </Grid>

        <SubmitButton
          label="Salvar"
          isLoading={formIsWorking}
          isDisabled={!formIsReady || formIsWorking}
        />

      </React.Fragment>
    );
  }

  createRequest(fields) {
    // this.props.submit('{company}/property-owner', fields);
  }

};

export default formCreator(CreateProcess);
