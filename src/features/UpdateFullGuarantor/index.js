import React from 'react';
import moment from 'moment';
import SubmitButton from 'components/SubmitButton';
import SectionWrapper from 'components/SectionWrapper';
import Grid from 'components/lib/Grid';
import { FormFieldSelect } from 'components/lib/FormFields';
import formCreator from '../components/formCreator';
import PersonalDataRenter from '../components/fieldsGroup/PersonalDataRenter';
import PersonalDataSpouse from 'features/components/fieldsGroup/PersonalDataSpouse'
import Company from '../components/fieldsGroup/Company';
import FullAddress from '../components/fieldsGroup/FullAddress';
import { goodObject, rawNumber } from 'utils/utils';
import { LEGAL_PERSON, NATURAL_PERSON } from '../constants/PersonTypes';

export class UpdateFullGuarantor extends React.Component {
  componentDidUpdate() { 
    this.props.createRequest(this.createRequest);
  }
  render() {
    const { formIsWorking, formIsReady, hideSubmitButton } = this.props;
    return (
      <React.Fragment>

        <SectionWrapper header="Dados do(a) fiador(a)">
          <PersonalDataRenter {...this.props} />
        </SectionWrapper>

        <SectionWrapper header="Dados do(a) cônjuge">
          <PersonalDataSpouse {...this.props} />
        </SectionWrapper>

        <SectionWrapper header="Endereço do(a) fiador(a)">
          <FullAddress {...this.props} noValidate />
        </SectionWrapper>

        <SectionWrapper header="Dados da empresa fiadora">
          <Company {...this.props} noValidate />
        </SectionWrapper>

        <SectionWrapper>
          {!hideSubmitButton && (
            <SubmitButton
              label="Salvar"
              isLoading={formIsWorking}
              isDisabled={!formIsReady || formIsWorking}
            />
          )}
        </SectionWrapper>

      </React.Fragment>
    );
  }
  createRequest(fields) {
    let goodFields = goodObject(fields, {
      'birth_date': {
        path: 'birth_date',
        format: (value) => moment(value, 'DD/MM/YYYY').format('YYYY-MM-DD')
      },
      'identity_expedition_date': {
        path: 'identity_expedition_date',
        format: (value) => moment(value, 'DD/MM/YYYY').format('YYYY-MM-DD')
      },
      'income': {
        path: 'income',
        format: (value) => rawNumber(value)
      },
      'marital_status_id': {
        path: 'marital_status',
        format: (value) => parseInt(value)
      },
      'spouse_birth_date': {
        path: 'spouse_birth_date',
        format: (value) => moment(value, 'DD/MM/YYYY').format('YYYY-MM-DD')
      },
    });
    this.props.submit(`{company}/guarantor/${this.props.guarantorId}`, goodFields, 'put');
  }

};

export default formCreator(UpdateFullGuarantor);
