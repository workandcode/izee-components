import React from 'react';
import ReactTimeout from 'react-timeout';
// import PropTypes from 'prop-types';

import AsyncSelect from 'react-select/lib/Async';
import { components } from 'react-select';

// import Input, { propsObject as defaultProps } from 'components/lib/FormFields/components/Input';
import Field from 'components/lib/FormFields/components/Field';
// import Form from 'components/lib/Form';
// import Button from 'components/lib/Button';
import Control from 'components/lib/FormFields/components/Control';
import Icon from 'components/lib/Icon';
import Text from 'components/lib/Text';

import { request } from 'core/constants';

class PropertySelection extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      inputValue: '',
      options: [],
      isWorking: false,
      noOptions: false
    };
    this.asyncRef = React.createRef();
  }

  render() {

    const promiseOptions = (inputValue) => new Promise((resolve) => {
      request.get('{company}/renter?search=' + inputValue)
        .then((response) => {
          this.setState({ noOptions: false });
          resolve(response.data);
        })
        .catch(({ response }) => {
          // console.log(response);
        });
    });

    const DropdownIndicator = (props) => (
      <components.DropdownIndicator {...props}>
        <Icon name="fas fa-home" />
      </components.DropdownIndicator>
    );

    const Option = (props) => {
      const data = props.data || {};
      return (
        <components.Option {...props}>
          <Text><strong>{data.name || 'Sem nome'}</strong></Text>
          <Text isSmall>{data.email}</Text>
        </components.Option>
      );
    };

    const MultiValueLabel = (props) => {
      const data = props.data || {};
      return (
        <components.MultiValueLabel {...props}>
          <Text><strong>{data.name || data.email}</strong></Text>
        </components.MultiValueLabel>
      );
    };

    const MultiValueRemove = (props) => (
      <div>
        <components.MultiValueRemove {...props}>
          <Icon name="far fa-times" />
        </components.MultiValueRemove>
      </div>
    );

    const LoadingIndicator = (props) => null;

    return (
      <React.Fragment>
        <Field label="Selecione um ou mais locatários">
          <Control>
            <div className="react-select-wrapper">
              <AsyncSelect
                defaultOptions
                ref={(ref) => this.asyncRef = ref}
                classNamePrefix="react-select"
                defaultValue={[]}
                name="renter"
                isMulti
                isDisabled={this.state.isWorking}
                isLoading={this.state.isWorking}
                value={this.state.options}
                loadingMessage={() => 'Pesquisando por locatários...'}
                noOptionsMessage={({ inputValue }) => {
                  if (!this.state.noOptions) this.setState({ noOptions: true });
                  return (
                    <span>Nenhum locatário encontrado</span>
                  );
                }}
                onKeyDown={(event) => {
                  if (
                    event.keyCode === 13 &&
                    this.state.noOptions
                  ) {
                    this.setState({ requestState: true });
                    this.asyncRef.blur();
                    request.post('{company}/renter', {
                      name: event.target.value,
                      'type_id': 1
                    })
                      .then((response) => {
                        const optionsState = [ ...this.state.options ];
                        optionsState.push(response.data);
                        this.props.setTimeout(() => {
                          this.asyncRef.focus();
                        }, 60);
                        this.setState({
                          noOptions: false,
                          options: optionsState,
                          inputValue: '',
                          isWorking: false
                        });
                      })
                      .catch((error) => {
                        console.log(error);
                      });
                  }
                }}
                placeholder="Selecione um ou mais imóveis"
                menuPosition="fixed"
                maxMenuHeight={168}
                minMenuHeight={100}
                onInputChange={this.handleInputChange.bind(this)}
                inputValue={this.state.inputValue}
                onChange={(options) => {
                  this.setState({
                    options, inputValue: ''
                  });
                  if (this.props.onSelect) this.props.onSelect(options);
                  this.props.handleFieldChanges(
                    'property_ids',
                    [...options].map((item) => item.id),
                    {
                      isValid: options.length > 0,
                      value: [...options].map((item) => item.id)
                    }
                  );
                }}
                isClearable={false}
                getOptionLabel={(option) => option.name}
                getOptionValue={(option) => option.id}
                components={{ MultiValueLabel, MultiValueRemove, DropdownIndicator, LoadingIndicator, Option }}
                loadOptions={promiseOptions}
              />
            </div>
          </Control>
        </Field>
      </React.Fragment>
    );
  }

  handleInputChange(inputValue) {
    this.setState({ inputValue });
    return inputValue;
  };

};

PropertySelection.defaultProps = {
  handleFieldChanges: () => {}
};

PropertySelection.propTypes = {};

export default ReactTimeout(PropertySelection);
