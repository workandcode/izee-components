import React from 'react';
import ReactTimeout from 'react-timeout';

import AsyncSelect from 'react-select/lib/Async';
import { components } from 'react-select';

import Field from 'components/lib/FormFields/components/Field';
import Control from 'components/lib/FormFields/components/Control';
import Icon from 'components/lib/Icon';
import Text from 'components/lib/Text';

import { request } from 'core/constants';

class PropertySelection extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      inputValue: '',
      options: (this.props.defaultValue || {}).properties || [],
      isWorking: false
    };
    this.asyncRef = React.createRef();
  }

  componentDidMount() {
    if ((this.props.defaultValue || {}).properties) {
      const defaultOptions = (this.props.defaultValue || {}).properties[0];
      this.props.handleFieldChanges(
        'property_ids',
        [defaultOptions.id],
        {
          isValid: [defaultOptions.id].length > 0,
          value: [defaultOptions.id]
        }
      );
    }
  }

  render() {

    const promiseOptions = (inputValue) => new Promise((resolve, reject) => {
      request.get('{company}/property?search=' + inputValue)
        .then((response) => {
          resolve(response.data);
        })
        .catch(() => reject());
    });

    const DropdownIndicator = (props) => (
      <components.DropdownIndicator {...props}>
        <Icon name="fas fa-home" />
      </components.DropdownIndicator>
    );

    const Option = (props) => {
      const address = props.data.address;
      const data = props.data || {};
      return (
        <components.Option {...props}>
          <Text><strong>{data.name || data.code}{data.name && data.code ? ` (${data.code})` : ''}</strong></Text>
          {address ? (
            <Text isSmall>{address.address}, {address.neighborhood} - {address.city} / {address.state}</Text>
          ) : null}
        </components.Option>
      );
    };

    const SingleValue = (props) => {
      const address = props.data.address;
      const data = props.data || {};
      return (
        <components.SingleValue {...props}>
          {address ? (
            <Text>{address.address}, {address.neighborhood} - {address.city} / {address.state}</Text>
          ) : 'Sem endereço'}
        </components.SingleValue>
      );
    };

    const LoadingIndicator = (props) => null;

    return (
      <React.Fragment>
        <Field label="Selecione um imóvel">
          <Control>
            <div className="react-select-wrapper">
              <AsyncSelect
                defaultOptions
                ref={(ref) => this.asyncRef = ref}
                classNamePrefix="react-select"
                defaultValue={[]}
                name="property"
                isDisabled={this.state.isWorking}
                isLoading={this.state.isWorking}
                value={this.state.options}
                loadingMessage={() => 'Pesquisando por imóveis...'}
                noOptionsMessage={() => <span>Nenhum imóvel encontrado</span>}
                placeholder=""
                menuPosition="fixed"
                maxMenuHeight={168}
                minMenuHeight={100}
                onInputChange={this.handleInputChange.bind(this)}
                inputValue={this.state.inputValue}
                onChange={(options) => {
                  this.setState({ options, inputValue: '' });
                  if (this.props.onSelect) this.props.onSelect(options);
                  this.props.handleFieldChanges(
                    'property_ids',
                    [options.id],
                    {
                      isValid: [options.id].length > 0,
                      value: [options.id]
                    }
                  );
                }}
                getOptionLabel={(option) => option.name}
                getOptionValue={(option) => option.id}
                components={{ SingleValue, DropdownIndicator, LoadingIndicator, Option }}
                loadOptions={promiseOptions}
              />
            </div>
          </Control>
        </Field>
      </React.Fragment>
    );
  }

  handleInputChange(inputValue) {
    this.setState({ inputValue });
    return inputValue;
  };

};

export default ReactTimeout(PropertySelection);
