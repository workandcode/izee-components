import React from 'react';
import ReactTimeout from 'react-timeout';

import AsyncSelect from 'react-select/lib/Async';
import { components } from 'react-select';

import Field from 'components/lib/FormFields/components/Field';
import Control from 'components/lib/FormFields/components/Control';
import Icon from 'components/lib/Icon';
import Text from 'components/lib/Text';

import { request } from 'core/constants';

class RenterRelation extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      inputValue: '',
      options: (this.props.defaultValue || {}).related_renters || [],
      isWorking: false,
      noOptions: false
    };
    this.asyncRef = React.createRef();
  }

  componentDidMount() {
    if ((this.props.defaultValue || {}).related_renters || [].length > 0) {
      const defaultOptions = (this.props.defaultValue || {}).related_renters || [];
      this.setState({ options: defaultOptions });
      this.props.handleFieldChanges(
        'related_renter_ids',
        [...defaultOptions].map((item) => item.id),
        {
          isValid: true,// defaultOptions.length > 0,
          value: [...defaultOptions].map((item) => item.id)
        }
      );
    }
  }

  getSnapshotBeforeUpdate(prevProps) {
    const defaultOptions = (this.props.defaultValue || {}).related_renters || [];
    const defaultOptionsPrev = (prevProps.defaultValue || {}).related_renters || [];

    if (this.state.options.length > 0) return null;
    if (defaultOptions.length < 1) return null;

    if ((this.props.defaultValue || {}).related_renters || [].length > 0) {
      this.setState({ options: defaultOptions });
      this.props.handleFieldChanges(
        'related_renter_ids',
        [...defaultOptions].map((item) => item.id),
        {
          isValid: true, // defaultOptions.length > 0,
          value: [...defaultOptions].map((item) => item.id)
        }
      );
    }

    return null;
  }

  componentDidUpdate() {}

  render() {

    const promiseOptions = (inputValue) => new Promise((resolve, reject) => {
      request.get('{company}/renter?search=' + inputValue)
        .then((response) => {
          this.setState({ noOptions: false });
          resolve(response.data);
        })
        .catch(() => reject());
    });

    const DropdownIndicator = (props) => (
      <components.DropdownIndicator {...props}>
        <Icon name="fas fa-user" />
      </components.DropdownIndicator>
    );

    const Option = (props) => {
      const data = props.data || {};
      return (
        <components.Option {...props}>
          <Text><strong>{data.name || 'Sem nome'}</strong></Text>
          <Text isSmall>{data.email}</Text>
        </components.Option>
      );
    };

    const MultiValueLabel = (props) => {
      const data = props.data || {};
      return (
        <components.MultiValueLabel {...props}>
          <Text>
            <strong>{data.name || data.email}</strong>
          </Text>
        </components.MultiValueLabel>
      );
    };

    const MultiValueRemove = (props) => (
      <div>
        <components.MultiValueRemove {...props}>
          <Icon name="far fa-times" />
        </components.MultiValueRemove>
      </div>
    );

    const LoadingIndicator = (props) => null;

    return (
      <React.Fragment>
        <Field label="Selecione um ou mais locatários">
          <Control>
            <div className="react-select-wrapper">
              <AsyncSelect
                defaultOptions
                ref={(ref) => this.asyncRef = ref}
                classNamePrefix="react-select"
                defaultValue={[]}
                name="renter"
                isMulti
                isDisabled={this.state.isWorking}
                isLoading={this.state.isWorking}
                value={this.state.options}
                loadingMessage={() => 'Pesquisando por locatários...'}
                noOptionsMessage={() => <span>Nenhum locatário encontrado</span>}
                placeholder=""
                menuPosition="fixed"
                maxMenuHeight={162}
                minMenuHeight={100}
                onInputChange={this.handleInputChange.bind(this)}
                inputValue={this.state.inputValue}
                onChange={(options) => {
                  this.setState({
                    options, inputValue: ''
                  });
                  if (this.props.onSelect) this.props.onSelect(options);
                  this.props.handleFieldChanges(
                    'related_renter_ids',
                    [...options].map((item) => item.id),
                    {
                      isValid: true, //options.length > 0,
                      value: [...options].map((item) => item.id)
                    }
                  );
                }}
                isClearable={false}
                getOptionLabel={(option) => option.name}
                getOptionValue={(option) => option.id}
                components={{ MultiValueLabel, MultiValueRemove, DropdownIndicator, LoadingIndicator, Option }}
                loadOptions={promiseOptions}
              />
            </div>
          </Control>
        </Field>
      </React.Fragment>
    );
  }

  handleInputChange(inputValue) {
    this.setState({ inputValue });
    return inputValue;
  };

};

RenterRelation.propTypes = {};

RenterRelation.defaultProps = {
  onClickItem: () => {}
};

export default ReactTimeout(RenterRelation);
