import React from 'react';

// Components
import {
  FormFieldText
} from 'components/lib/FormFields';
import { fieldProps } from '../formCreator';
import Grid from 'components/lib/Grid';

const CompanyName = (props) => (
  <Grid>
    <Grid.Col>
      <FormFieldText
        placeholder="Minha empresa top"
        {...fieldProps(props, 'name')}
      />
    </Grid.Col>
  </Grid>
);

export default CompanyName;
