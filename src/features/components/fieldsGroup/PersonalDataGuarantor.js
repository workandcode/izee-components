import React from 'react';

// Components
import {
  FormFieldText,
  FormFieldDate,
  FormFieldSelect,
  FormFieldSwitch
} from 'components/lib/FormFields';
import { fieldProps } from '../formCreator';
import Grid from 'components/lib/Grid';
import { isMobile } from 'utils/utils';

const PersonalData = (props) => (
  <React.Fragment>
    <Grid>
      <Grid.Col>
        <FormFieldText
          {...fieldProps(props, 'name')}
          label="Nome"
        />
      </Grid.Col>
      <Grid.Col>
        <FormFieldText
          {...fieldProps(props, 'email')}
          label="Email"
        />
      </Grid.Col>
      <Grid.Col isNarrow>
        <FormFieldText
          {...fieldProps(props, 'phone')}
          label="Telefone"
          mask={['phone']}
          style={{ width: 150 }}
          noValidate
        />
      </Grid.Col>
    </Grid>
    <Grid>
      <Grid.Col isNarrow>
        <FormFieldDate
          {...fieldProps(props, 'birth_date')}
          label="Data de nascimento"
          noValidate
        />
      </Grid.Col>
      <Grid.Col>
        <FormFieldText
          {...fieldProps(props, 'occupation')}
          label="Ocupação"
          noValidate
        />
      </Grid.Col>
    </Grid>
    <Grid>
      <Grid.Col>
        <FormFieldSelect
          {...fieldProps(props, 'marital_status')}
          label="Estado civil"
          valueKey="id"
          options={[{
            id: 1,
            code: 'MARITAL_STATUS_SINGLE',
            name: 'Solteiro(a)'
          }, {
            id: 2,
            code: 'MARITAL_STATUS_MARRIED',
            name: 'Casado(a)'
          }, {
            id: 3,
            code: 'MARITAL_STATUS_DIVORCED',
            name: 'Divorciado(a)'
          }, {
            id: 4,
            code: 'MARITAL_STATUS_STABLE_UNION',
            name: 'União Estável'
          }, {
            id: 5,
            code: 'MARITAL_STATUS_WIDOWED',
            name: 'Viúvo(a)'
          }]}
          noValidate
        />
      </Grid.Col>
      <Grid.Col>
        <FormFieldText
          {...fieldProps(props, 'income')}
          mask={['number']}
          label="Renda"
          noValidate
        />
      </Grid.Col>
      <Grid.Col>
        <FormFieldText
          {...fieldProps(props, 'estimated_rent_value')}
          mask={['number']}
          label="Valor estimado do aluguel"
          noValidate
        />
      </Grid.Col>
    </Grid>
    <Grid>
      <Grid.Col>
        <FormFieldSelect
          {...fieldProps(this.props, 'identity_type')}
          label="Documento"
          valueKey="name"
          options={[{
            id: 1,
            code: 'DOCUMENT_TYPE_RG',
            name: 'RG'
          }, {
            id: 2,
            code: 'DOCUMENT_TYPE_CNH',
            name: 'CNH'
          }, {
            id: 3,
            code: 'DOCUMENT_TYPE_PASSPORT',
            name: 'Passaporte'
          }, {
            id: 4,
            code: 'DOCUMENT_TYPE_OTHER',
            name: 'Outro documento'
          }
          ]}
          noValidate
        />
      </Grid.Col>
      <Grid.Col>
        <FormFieldText
          {...fieldProps(props, 'identity')}
          label="Número"
          mask={[{ maxLength: 15 }]}
          noValidate
        />
      </Grid.Col>
      <Grid.Col isNarrow>
        <FormFieldDate
          {...fieldProps(props, 'identity_expedition_date')}
          label="Data de expedição"
          noValidate
        />
      </Grid.Col>
      <Grid.Col>
        <FormFieldText
          {...fieldProps(props, 'identity_issuing_body')}
          label="Orgão emissor"
          noValidate
        />
      </Grid.Col>
    </Grid>
    <Grid>
      <Grid.Col isNarrow>
        <FormFieldText
          {...fieldProps(props, 'cpf')}
          label="CPF"
          mask={['cpf']}
          noValidate
        />
      </Grid.Col>
      <Grid.Col isNarrow>
        <FormFieldSwitch
          {...fieldProps(props, 'is_student')}
          label="Estudante?"
          type="checkbox"
          noValidate
        />
      </Grid.Col>
    </Grid>
  </React.Fragment>
);

export default PersonalData;
