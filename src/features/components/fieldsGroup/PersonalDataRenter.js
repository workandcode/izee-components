import React from 'react';

// Components
import {
  FormFieldText,
  FormFieldDate,
  FormFieldSelect,
  FormFieldSwitch
} from 'components/lib/FormFields';
import { fieldProps } from '../formCreator';
import Grid from 'components/lib/Grid';
import SectionWrapper from '../../../components/SectionWrapper';
import Button from '../../../components/lib/Button';
import Icon from 'components/lib/Icon';

class PersonalData extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showSpouse: false
    }
  }

  componentDidUpdate() {
    const { spouse_name = { value: "" }, spouse_email = { value: "" } } = this.props.fields;
    if (!this.state.showSpouse && spouse_name.value !== "" && spouse_email.value !== "") {
      this.setState({ showSpouse: true })
    }
  }

  showSpouse() {
    this.setState({ showSpouse: true },
      this.props.updateFieldValues([
        { fieldName: 'spouse_name', field: { ...this.props.fields.spouse_name } },
        { fieldName: 'spouse_email', field: { ...this.props.fields.spouse_email } }
      ])
    )
  }

  hideSpouse() {
    this.setState({ showSpouse: false },
      this.props.updateFieldValues([
        { fieldName: 'spouse_name', field: { ...this.props.fields.spouse_name, value: "", isValid: null } },
        { fieldName: 'spouse_email', field: { ...this.props.fields.spouse_email, value: "", isValid: null } },
        { fieldName: 'spouse_phone', field: { ...this.props.fields.spouse_phone, value: "" } },
        { fieldName: 'spouse_birth_date', field: { ...this.props.fields.spouse_birth_date, value: "" } },
        { fieldName: 'spouse_occupation', field: { ...this.props.fields.spouse_occupation, value: "" } },
        { fieldName: 'spouse_nationality', field: { ...this.props.fields.spouse_nationality, value: "" } },
        { fieldName: 'spouse_identity_type', field: { ...this.props.fields.spouse_identity_type, value: "" } },
        { fieldName: 'spouse_identity', field: { ...this.props.fields.spouse_identity, value: "" } },
        { fieldName: 'spouse_identity_issuing_body', field: { ...this.props.fields.spouse_identity_issuing_body, value: "" } },
        { fieldName: 'spouse_cpf', field: { ...this.props.fields.spouse_cpf, value: "" } }
      ])
    )
  }

  render() {
    const { showSpouse } = this.state;
    const { fields } = this.props;
    const { spouse_active } = fields;
    return (
      <React.Fragment>
        <Grid>
          <Grid.Col>
            <FormFieldText
              {...fieldProps(this.props, 'name')}
              label="Nome"
            />
          </Grid.Col>
          <Grid.Col>
            <FormFieldText
              {...fieldProps(this.props, 'email')}
              isDisabled={this.props.disableEmail}
              label="Email"
            />
          </Grid.Col>
          <Grid.Col isNarrow>
            <FormFieldText
              {...fieldProps(this.props, 'phone')}
              label="Telefone"
              mask={['phone']}
              style={{ width: 150 }}
              noValidate
            />
          </Grid.Col>
        </Grid>
        <Grid>
          <Grid.Col isNarrow>
            <FormFieldText
              {...fieldProps(this.props, 'cpf')}
              label="CPF"
              mask={['cpf']}
              noValidate={this.props.noValidate}
            />
          </Grid.Col>
          <Grid.Col>
            <FormFieldSelect
              {...fieldProps(this.props, 'identity_type')}
              label="Documento"
              valueKey="name"
              options={[{
                id: 1,
                code: 'DOCUMENT_TYPE_RG',
                name: 'RG'
              }, {
                id: 2,
                code: 'DOCUMENT_TYPE_CNH',
                name: 'CNH'
              }, {
                id: 3,
                code: 'DOCUMENT_TYPE_PASSPORT',
                name: 'Passaporte'
              }, {
                id: 4,
                code: 'DOCUMENT_TYPE_OTHER',
                name: 'Outro documento'
              }
              ]}
              noValidate
            />
          </Grid.Col>
          <Grid.Col>
            <FormFieldText
              {...fieldProps(this.props, 'identity')}
              label="Número do documento"
              mask={[{ maxLength: 15 }]}
              noValidate
            />
          </Grid.Col>
          <Grid.Col>
            <FormFieldText
              {...fieldProps(this.props, 'identity_issuing_body')}
              label="Orgão emissor"
              noValidate
            />
          </Grid.Col>
        </Grid>
        <Grid>
          <Grid.Col isNarrow>
            <FormFieldDate
              {...fieldProps(this.props, 'birth_date')}
              label="Data de nascimento"
              noValidate
            />
          </Grid.Col>
          <Grid.Col>
            <FormFieldText
              {...fieldProps(this.props, 'nationality')}
              label="Nacionalidade"
              noValidate
            />
          </Grid.Col>
          <Grid.Col>
            <FormFieldSelect
              {...fieldProps(this.props, 'marital_status')}
              label="Estado civil"
              valueKey="id"
              options={[{
                id: 1,
                code: 'MARITAL_STATUS_SINGLE',
                name: 'Solteiro(a)'
              }, {
                id: 2,
                code: 'MARITAL_STATUS_MARRIED',
                name: 'Casado(a)'
              }, {
                id: 3,
                code: 'MARITAL_STATUS_DIVORCED',
                name: 'Divorciado(a)'
              }, {
                id: 4,
                code: 'MARITAL_STATUS_STABLE_UNION',
                name: 'União Estável'
              }, {
                id: 5,
                code: 'MARITAL_STATUS_WIDOWED',
                name: 'Viúvo(a)'
              }]}
              noValidate
            />
          </Grid.Col>
        </Grid>
        <Grid>
          <Grid.Col>
            <FormFieldText
              {...fieldProps(this.props, 'occupation')}
              label="Ocupação"
              noValidate
            />
          </Grid.Col>
          <Grid.Col>
            <FormFieldText
              {...fieldProps(this.props, 'income')}
              mask={['number']}
              label="Renda"
              noValidate
            />
          </Grid.Col>
          {this.props.hiddenFields && this.props.hiddenFields.indexOf('estimated_rent_value') === -1 && (
            <Grid.Col>
              <FormFieldText
                {...fieldProps(this.props, 'estimated_rent_value')}
                mask={['number']}
                label="Valor estimado do aluguel"
                noValidate
              />
            </Grid.Col>
          )}
          <Grid.Col>
            <FormFieldSwitch
              {...fieldProps(this.props, 'is_student')}
              label="Estudante?"
              type="checkbox"
              noValidate
            />
          </Grid.Col>
        </Grid>
   
      </React.Fragment>
    )
  };
}

export default PersonalData;
