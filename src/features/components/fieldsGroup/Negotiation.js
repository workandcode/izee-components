import React from 'react';

// Components
import {
  FormFieldDate,
  FormFieldText,
  FormFieldTextArea
} from 'components/lib/FormFields';
import Grid from 'components/lib/Grid';
import { fieldProps } from '../formCreator';

const Negotiation = (props) => (
  <React.Fragment>
    <Grid>
      <Grid.Col>
        <FormFieldText
          {...fieldProps(props, 'rent_value')}
          label="Valor do aluguel"
          mask={['number']}
        />
      </Grid.Col>
      {/* <Grid.Col isNarrow>
        <FormFieldDate
          {...fieldProps(props, 'ownership_date')}
          label="Data da posse"
        />
      </Grid.Col> */}
    </Grid>
    <Grid>
      <Grid.Col>
        <FormFieldTextArea
          {...fieldProps(props, 'negotiation')}
          label="Negociação"
        />
      </Grid.Col>
    </Grid>
  </React.Fragment>
);

export default Negotiation;
