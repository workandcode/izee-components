import React from 'react';
import jsonp from 'jsonp';

// Components
import {
  FormFieldCEP,
  FormFieldText
} from 'components/lib/FormFields';
import { fieldProps } from '../formCreator';
import Grid from 'components/lib/Grid';

// Utils
import { timeDelay } from 'utils/utils';

export default class FullAddress extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      addressByCEP: {},
      isSearchingForAddress: false
    };
  }

  render() {

    const cepValidation = {
      isInvalid: this.state.addressByCEP.erro,
      message: 'CEP inválido ou inexistente'
    };

    return (
      <React.Fragment>
        <Grid>
          <Grid.Col isNarrow>
            <FormFieldCEP
              {...fieldProps(this.props, 'zip_code')}
              forceInvalid={cepValidation.isInvalid}
              errorMessage={cepValidation.isInvalid && cepValidation.message}
              isLoading={this.state.isSearchingForAddress}
              info="Se não souber o seu CEP preencha os campos manualmente"
              onChange={this.handleCEP.bind(this)}
              noValidate={this.props.noValidate}
            />
          </Grid.Col>
          <Grid.Col>
            <FormFieldText
              {...fieldProps(this.props, 'address.address')}
              label="Logradouro"
              noValidate={this.props.noValidate}
              valueDefault={this.state.addressByCEP.logradouro || this.props.defaultValue['address']}
              isDisabled={this.props.formIsWorking || this.state.isSearchingForAddress}
            />
          </Grid.Col>
          <Grid.Col isOneFifth>
            <FormFieldText
              {...fieldProps(this.props, 'address.number')}
              valueDefault={this.props.defaultValue['number'] || ''}
              label="Número"
              noValidate={this.props.noValidate}
              mask={['onlyNumbers']}
            />
          </Grid.Col>
        </Grid>
        <Grid>
          <Grid.Col>
            <FormFieldText
              {...fieldProps(this.props, 'address.city')}
              label="Cidade"
              noValidate={this.props.noValidate}
              valueDefault={this.state.addressByCEP.localidade || this.props.defaultValue['city']}
              isDisabled={this.props.formIsWorking || this.state.isSearchingForAddress}
            />
          </Grid.Col>
          <Grid.Col isOneFifth>
            <FormFieldText
              {...fieldProps(this.props, 'address.state')}
              label="Estado"
              noValidate={this.props.noValidate}
              valueDefault={this.state.addressByCEP.uf || this.props.defaultValue['state']}
              isDisabled={this.props.formIsWorking || this.state.isSearchingForAddress}
            />
          </Grid.Col>
          <Grid.Col>
            <FormFieldText
              {...fieldProps(this.props, 'address.neighborhood')}
              label="Bairro"
              noValidate={this.props.noValidate}
              valueDefault={this.state.addressByCEP.bairro || this.props.defaultValue['neighborhood']}
              isDisabled={this.props.formIsWorking || this.state.isSearchingForAddress}
            />
          </Grid.Col>
        </Grid>
        <Grid>
          <Grid.Col>
            <FormFieldText
              {...fieldProps(this.props, 'address.additional_address')}
              valueDefault={this.props.defaultValue['additional_address']}
              label="Complemento (Opcional)"
              noValidate
            />
          </Grid.Col>
        </Grid>
      </React.Fragment>
    );
  }

  handleCEP(value, field) {
    if (!field.wasFocused) return;
    this.setState({ addressByCEP: {} });
    if (!value || value.length < 9) return;
    const searchForCEPDelay = timeDelay(1000);
    this.setState({ isSearchingForAddress: true });
    jsonp('https://viacep.com.br/ws/' + value.replace('-', '') + '/json', null, (error, response) => {
      if (response) {
        searchForCEPDelay(() => {
          this.props.handleFieldChanges('address.zip_code', value, field);
          this.setState({
            addressByCEP: response || {},
            isSearchingForAddress: false
          });
        });
        return;
      }
      this.props.handleFieldChanges('address.zip_code', value, field);
      this.setState({
        addressByCEP: {
          erro: true
        },
        isSearchingForAddress: false
      });
    });
  }

};

FullAddress.defaultProps = {
  defaultValue: {}
};
