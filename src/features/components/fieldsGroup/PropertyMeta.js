import React from 'react';

// Components
import {
  FormFieldSelect,
  FormFieldText
} from 'components/lib/FormFields';
import Grid from 'components/lib/Grid';
import { fieldProps } from '../formCreator';

export default class PropertyMeta extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      selectedType: this.props.defaultValue && this.props.defaultValue.type ? this.props.defaultValue.type.id : null
    };
  }

  render() {

    const defaultType = this.props.defaultValue && this.props.defaultValue.type ? this.props.defaultValue.type.id : null;

    const type = this.state.selectedType || defaultType;

    const condoFee = (type == 2 || type == 3 || type == 4);
    const parking = (type == 2 || type == 4);
    const hobbyBoxes = (type == 2 || type == 4);

    return (
      <React.Fragment>
        <Grid>
          <Grid.Col>
            <FormFieldSelect
              {...fieldProps(this.props, 'type')}
              label="Tipo de imóvel"
              onChange={this.handleTypeChange.bind(this)}
              value={type}
              valueKey="id"
              options={[
                { id: 1, code: 'TYPE_HOUSE', name: 'Casa' },
                { id: 2, code: 'TYPE_APARTMENT', name: 'Apartamento' },
                { id: 3, code: 'TYPE_CONDO_HOUSE', name: 'Casa em condomínio' },
                { id: 4, code: 'TYPE_PENTHOUSE', name: 'Cobertura' }
              ]}
            />
          </Grid.Col>
          <Grid.Col>
            <FormFieldText
              {...fieldProps(this.props, 'area')}
              label="Área privativa (m²)"
              mask={['onlyNumbers']}
            />
          </Grid.Col>
          <Grid.Col>
            <FormFieldText
              {...fieldProps(this.props, 'iptu')}
              label="IPTU (Aproximado)"
              mask={['number']}
            />
          </Grid.Col>
          <Grid.Col>
            {condoFee &&
              <FormFieldText
                {...fieldProps(this.props, 'condo_fee')}
                label="Condomínio (Mensal aproximado)"
                mask={['number']}
              />
            }
          </Grid.Col>
        </Grid>
        <Grid>
          <Grid.Col>
            {parking &&
              <FormFieldText
                {...fieldProps(this.props, 'parking_spaces')}
                label="Vaga de garagem"
              />
            }
          </Grid.Col>
          <Grid.Col>
            {hobbyBoxes &&
              <FormFieldText
                {...fieldProps(this.props, 'hobby_boxes')}
                label="Hobby box"
              />
            }
          </Grid.Col>
        </Grid>
      </React.Fragment>
    );
  }

  handleTypeChange(value, field) {
    if (!field.wasFocused) return;
    this.setState({ selectedType: value });
    this.props.handleFieldChanges('type_id', value, field);
  }

};
