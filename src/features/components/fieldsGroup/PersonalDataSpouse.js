import React from 'react';
import SectionWrapper from '../../../components/SectionWrapper';
import {
  FormFieldText,
  FormFieldDate,
  FormFieldSelect,
  FormFieldSwitch
} from 'components/lib/FormFields';
import Grid from 'components/lib/Grid';
import { fieldProps } from '../formCreator';

class PersonalDataSpouse extends React.Component {
  constructor(props) {
    super(props);
    this.handleSpouseStatus = this.handleSpouseStatus.bind(this);
    this.state = {
      spouse_status: false
    }
  }
  componentDidUpdate() { 
    const { defaultValue, fields } = this.props;
    // check spouse_status
    const spouse_status = fields.spouse_cpf ? fields.spouse_cpf.value : defaultValue.spouse_cpf ? true : false;
    if(!this.state.spouse_status && spouse_status) {
      this.setState({ spouse_status: spouse_status })
    }
  }
  handleSpouseStatus() {
    const { defaultValue, fields } = this.props;
    const { spouse_status } = this.state;
    this.setState({ spouse_status: !spouse_status })
    if(spouse_status) {
      this.props.updateFieldValues([
        { fieldName: 'spouse_cpf', field: { value: "", isValid: true } },
        { fieldName: 'spouse_birth_date', field: { value: "", isValid: true } },
        { fieldName: 'spouse_email', field: { value: "", isValid: true } },
        { fieldName: 'spouse_identity', field: { value: "", isValid: true } },
        { fieldName: 'spouse_identity_issuing_body', field: { value: "", isValid: true } },
        { fieldName: 'spouse_identity_type', field: { value: "", isValid: true } },
        { fieldName: 'spouse_name', field: { value: "", isValid: true } },
        { fieldName: 'spouse_nationality', field: { value: "", isValid: true } },
        { fieldName: 'spouse_occupation', field: { value: "", isValid: true } },
        { fieldName: 'spouse_phone', field: { value: "", isValid: true } },
      ]);
    }
  }
  render() {
    const { spouse_status } = this.state;
    const props = this.props;
    const { fields } = this.props;
    return (
      <React.Fragment>
        <FormFieldSwitch
          type="checkbox"
          description="Tem cônjuge?"
          checked={spouse_status}
          onClick={this.handleSpouseStatus ? this.handleSpouseStatus : null}
        />
        { spouse_status ? (
          <React.Fragment>
            <Grid>
              <Grid.Col>
                <FormFieldText
                  {...fieldProps(this.props, 'spouse_name')}
                  label="Nome"
                  noValidate
                />
              </Grid.Col>
              <Grid.Col>
                <FormFieldText
                  {...fieldProps(this.props, 'spouse_email')}
                  label="Email"
                  noValidate
                />
              </Grid.Col>
              <Grid.Col isNarrow>
                <FormFieldText
                  {...fieldProps(this.props, 'spouse_phone')}
                  label="Telefone"
                  mask={['phone']}
                  style={{ width: 150 }}
                  noValidate
                />
              </Grid.Col>
            </Grid>
            <Grid>
              <Grid.Col isNarrow>
                <FormFieldText
                  {...fieldProps(this.props, 'spouse_cpf')}
                  label="CPF"
                  mask={['cpf']}
                  noValidate
                />
              </Grid.Col>
              <Grid.Col>
                <FormFieldSelect
                  {...fieldProps(this.props, 'spouse_identity_type')}
                  label="Documento"
                  valueKey="name"
                  options={[{
                    id: 1,
                    code: 'DOCUMENT_TYPE_RG',
                    name: 'RG'
                  }, {
                    id: 2,
                    code: 'DOCUMENT_TYPE_CNH',
                    name: 'CNH'
                  }, {
                    id: 3,
                    code: 'DOCUMENT_TYPE_PASSPORT',
                    name: 'Passaporte'
                  }, {
                    id: 4,
                    code: 'DOCUMENT_TYPE_OTHER',
                    name: 'Outro documento'
                  }]}
                  noValidate
                />
              </Grid.Col>
              <Grid.Col>
                <FormFieldText
                  {...fieldProps(this.props, 'spouse_identity')}
                  label="Número do documento"
                  mask={[{ maxLength: 15 }]}
                  noValidate
                />
              </Grid.Col>
              <Grid.Col>
                <FormFieldText
                  {...fieldProps(this.props, 'spouse_identity_issuing_body')}
                  label="Orgão emissor"
                  noValidate
                />
              </Grid.Col>
            </Grid>
            <Grid>
              <Grid.Col isNarrow>
                <FormFieldDate
                  {...fieldProps(this.props, 'spouse_birth_date')}
                  label="Data de nascimento"
                  noValidate
                />
              </Grid.Col>
              <Grid.Col>
                <FormFieldText
                  {...fieldProps(this.props, 'spouse_nationality')}
                  label="Nacionalidade"
                  noValidate
                />
              </Grid.Col>
              <Grid.Col>
                <FormFieldText
                  {...fieldProps(this.props, 'spouse_occupation')}
                  label="Ocupação"
                  noValidate
                />
              </Grid.Col>
            </Grid>
          </React.Fragment>
        ) : null }
      </React.Fragment>
    )
  };
}

export default PersonalDataSpouse;