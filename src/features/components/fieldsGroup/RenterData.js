import React from 'react';

// Components
import {
  FormFieldText,
  FormFieldDate,
  FormFieldSelect
} from 'components/lib/FormFields';
import { fieldProps } from '../formCreator';
import Grid from 'components/lib/Grid';

const PersonalData = (props) => (
  <React.Fragment>
    <Grid>
      <Grid.Col>
        <FormFieldText
          {...fieldProps(props, 'name')}
          label="Nome"
        />
      </Grid.Col>
      <Grid.Col>
        <FormFieldText
          {...fieldProps(props, 'email')}
          label="Email"
        />
      </Grid.Col>
      <Grid.Col isNarrow>
        <FormFieldText
          {...fieldProps(props, 'phone')}
          label="Telefone"
          noValidate
          mask={['phone']}
          style={{ width: 150 }}
        />
      </Grid.Col>
    </Grid>
    <Grid>
      <Grid.Col>
        <FormFieldText
          {...fieldProps(props, 'cpf')}
          label="CPF"
          mask={['cpf']}
        />
      </Grid.Col>
      <Grid.Col>
        <FormFieldText
          {...fieldProps(props, 'income')}
          mask={['number']}
          label="Renda"
        />
      </Grid.Col>
      <Grid.Col>
        <FormFieldText
          {...fieldProps(props, 'estimated_rent_value')}
          mask={['number']}
          label="Valor estimado do aluguel"
        />
      </Grid.Col>
    </Grid>
  </React.Fragment>
);

export default PersonalData;
