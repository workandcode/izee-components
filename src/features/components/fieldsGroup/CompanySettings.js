import React from 'react';

// Components
import { FormFieldText, FormFieldSwitch } from 'components/lib/FormFields';
import { fieldProps } from '../formCreator';
import Grid from 'components/lib/Grid';

const CompanySettings = (props) => (
  <Grid>
    <Grid.Col>

      <FormFieldSwitch
        {...fieldProps(props, 'signs_contract')}
        description="A imobiliária assina o contrato no lugar do proprietário"
        defaultChecked={props.defaultValue['signs_contract']}
        isLoading={props.formIsWorking}
      />

      <FormFieldSwitch
        {...fieldProps(props, 'chooses_guarantee')}
        description="A imobiliária escolhe a garantia no lugar do locatário"
        defaultChecked={props.defaultValue['chooses_guarantee']}
        isLoading={props.formIsWorking}
      />

    </Grid.Col>
  </Grid>
);

export default CompanySettings;
