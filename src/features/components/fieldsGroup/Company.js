import React from 'react';

// Components
import {
  FormFieldCNPJ,
  FormFieldText,
  FormFieldSwitch
} from 'components/lib/FormFields';
import { fieldProps } from '../formCreator';
import Grid from 'components/lib/Grid';

class Company extends React.Component {
  constructor(props) {
    super(props);
    this.handleCompanyStatus = this.handleCompanyStatus.bind(this);
    this.state = {
      company_status: false,
    }
  }
  componentDidUpdate() { 
    const { defaultValue, fields } = this.props;
    // check company_status
    const company_status = fields.cnpj ? fields.cnpj.value : defaultValue.cnpj ? true : false;
    if(!this.state.company_status && company_status) {
      this.setState({ company_status: company_status })
    }
  }
  handleCompanyStatus() {
    const { defaultValue, fields } = this.props;
    const { company_status } = this.state;
    this.setState({ company_status: !company_status })
    if(company_status) {
      this.props.updateFieldValues([
        { fieldName: 'cnpj', field: { value: "", isValid: true } },
        { fieldName: 'trading_name', field: { value: "", isValid: true } },
        { fieldName: 'state_enrollment', field: { value: "", isValid: true } },
        { fieldName: 'municipal_enrollment', field: { value: "", isValid: true } },
      ]);
    }
  }
  render() {
    const { company_status } = this.state;
    const props = this.props;
    const { fields } = props;
    return (
      <React.Fragment>
        { !props.prefix ? (
          <FormFieldSwitch
            checked={company_status}
            onClick={this.handleCompanyStatus ? this.handleCompanyStatus : null}
            description="Tem empresa?"
            type="checkbox"
          />
        ) : null }
        { props.prefix || company_status ? (
          <React.Fragment>
            <Grid>
              <Grid.Col>
                <FormFieldText
                  {...fieldProps(props, props.prefix ? 'name' : 'trading_name')}
                  label="Nome da empresa"
                />
              </Grid.Col>
              <Grid.Col isOneThird>
                <FormFieldText
                  {...fieldProps(props, 'cnpj')}
                  label="CNPJ"
                  mask={['cnpj']}
                  noValidate={fields.noValidate}
                />
              </Grid.Col>
            </Grid>
            <Grid>
              <Grid.Col>
                <FormFieldText
                  {...fieldProps(props, 'trading_name')}
                  label="Razão Social"
                  noValidate
                />
              </Grid.Col>
              <Grid.Col>
                <FormFieldText
                  {...fieldProps(props, 'municipal_enrollment')}
                  label="Inscrição Municipal"
                  noValidate
                />
              </Grid.Col>
              <Grid.Col>
                <FormFieldText
                  {...fieldProps(props, 'state_enrollment')}
                  label="Inscrição Estadual"
                  noValidate
                />
              </Grid.Col>
              { props.prefix ? 
                <Grid.Col isNarrow>
                  <FormFieldText
                    {...fieldProps(props, 'prefix')}
                    label="Prefixo"
                    info="Este código aparece antes do ID da proposta"
                    mask={[{ 'maxLength': 3 }]}
                    validator={[{ 'minLength': 2 }]}
                    style={{ maxWidth: 58 }}
                    noValidate
                  />
                </Grid.Col>
             : null }
            </Grid>
          </React.Fragment>
        ) : null }
      </React.Fragment>
    )
  }
}

export default Company;
