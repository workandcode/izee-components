import React from 'react';
import ReactQuill, { Quill } from 'react-quill';
import 'react-quill/dist/quill.snow.css';

// Components
import {
  FormFieldText,
  FormFieldTextArea,
  FormFieldSelect
} from 'components/lib/FormFields';
import Grid from 'components/lib/Grid';
import Text from 'components/lib/Text';
import { fieldProps } from '../formCreator';

export default class ContractEditor extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      editorState: this.props.defaultValue['template'] || ''
    };
    this.editorRef = null;

    const AlignStyle = Quill.import('attributors/style/align');
    const BackgroundStyle = Quill.import('attributors/style/background');
    const ColorStyle = Quill.import('attributors/style/color');
    const DirectionStyle = Quill.import('attributors/style/direction');
    const FontStyle = Quill.import('attributors/style/font');
    const SizeStyle = Quill.import('attributors/style/size');

    Quill.register(AlignStyle, true);
    Quill.register(BackgroundStyle, true);
    Quill.register(ColorStyle, true);
    Quill.register(DirectionStyle, true);
    Quill.register(FontStyle, true);
    Quill.register(SizeStyle, true);

    this.modules = {
      toolbar: [
        ['bold', 'italic', 'underline'],
        [{ 'background': [] }],
        [{ 'header': 1 }, { 'header': 2 }],
        [{ 'indent': '-1' }, { 'indent': '+1' }],
        [{ 'align': [] }],
        [{ 'size': ['small', false, 'large'] }],
        [{ 'font': [] }]
      ]
    };
    this.formats = [
      'header', 'font', 'background', 'color', 'size',
      'bold', 'italic', 'underline', 'strike',
      'list', 'bullet', 'indent', 'script', 'align'
    ];
  }

  componentDidMount() {
    this.props.handleFieldChanges('template', this.state.editorState, {
      isValid: this.state.editorState.length > 0,
      value: this.state.editorState
    });
  }

  render() {
    return (
      <React.Fragment>
        <Grid>
          <Grid.Col>
            <FormFieldText
              {...fieldProps(this.props, 'name')}
              label="Modelo"
              info="Dê um nome ao modelo do contrato"
            />
          </Grid.Col>
        </Grid>
        <Grid>
          <Grid.Col>
            <ReactQuill
              theme="snow"
              tabIndex={0}
              ref={(editor) => this.editorRef = editor}
              value={this.state.editorState}
              placeholder={''}
              onChange={this.handleEditorChanges.bind(this)}
              modules={this.modules}
              formats={this.formats}
            />
          </Grid.Col>
        </Grid>

        <Grid>
          <Grid.Col>
            <FormFieldSelect
              {...fieldProps(this.props, 'guarantee_id')}
              label="Garantia associada"
              info="Seleciona o tipo de garantia em que este contrato deverá ser utilizado"
              valueKey={'id'}
              options={this.props.activeCompany.guarantees}
            />
          </Grid.Col>
        </Grid>

        <Grid>
          <Grid.Col>
            <FormFieldTextArea
              label="Tokens"
              info="Copie e cole os tokens para substituir por dados reais no final do processo de locação"
              valueDefault={`{!! $PROPRIETARIOS !!}
{!! $LOCATARIOS !!}
{!! $FIADORES !!}
{!! $IMOVEL !!}
{!! $DATA_POSSE !!}
{!! $DATA_POSSE_EXT !!}
{!! $DATA_FIM_VIGENCIA !!}
{!! $DATA_FIM_VIGENCIA_EXT !!}
{!! $VALOR_ALUGUEL !!}
{!! $VALOR_ALUGUEL_EXT !!}
{!! $VALOR_CAPITALIZACAO !!}
{!! $VALOR_CAPITALIZACAO_EXT !!}
{!! $PROTOCOLO_CAPITALIZACAO !!}
{!! $CLAUSULA_ADICIONAL !!}
{!! $ASSINATURAS !!}`}
              readOnly
              isDisabled
              style={{ height: 335 }}
            />
          </Grid.Col>
        </Grid>
      </React.Fragment>
    );
  }

  handleEditorChanges(editorState) {
    this.setState({ editorState });
    this.props.handleFieldChanges('template', editorState, {
      isValid: editorState.length > 0,
      value: editorState
    });
  }

};
