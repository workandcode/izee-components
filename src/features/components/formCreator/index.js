import React from 'react';
import ReactTimeout from 'react-timeout';
import PropTypes from 'prop-types';
import { each } from 'lodash';

// Redux
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from './actions';

// Components
import Form from 'components/lib/Form';
import { invalidFields } from 'components/lib/FormFields';

// Utils
import parseApiErrors from 'utils/parseApiErrors';
import { getValues } from 'components/lib/FormFields';
import { deepKey, setFormInitialFocus } from 'utils/utils';

const formCreator = (Component, options = {}) => {

  class FormWrapper extends React.Component {

    constructor(props) {
      super(props);
      this.state = {
        fields: {},
        fieldErrors: {},
        formIsWorking: false,
        formFocus: false
      };
      this.form = null;
      this.resetAfterSuccess = false;
    }

    getSnapshotBeforeUpdate(prevProps) {
      if (prevProps.actionKey !== this.props.actionKey && this.state.formIsWorking) {
        this.setState({ formIsWorking: false });
        this.props.setTimeout(() => {
          this.setState({
            formIsWorking: false,
            fieldErrors: parseApiErrors(this.props.errors)
          });
          if (this.props.onSuccess) {
            if (this.resetAfterSuccess && this.props.isSuccess) this.resetForm();
            this.props.onSuccess({
              isSuccess: this.props.isSuccess,
              actionMessage: this.props.actionMessage,
              actionKey: this.props.actionKey,
              isProcessing: this.props.isProcessing,
              errors: this.props.errors,
              data: this.props.data
            });
          }
        }, 300);
      }
      return null;
    }

    componentDidUpdate() { }

    render() {

      const formIsWorking = this.state.formIsWorking;
      const formIsReady = invalidFields(this.state.fields).length === 0;
      console.log("INVALID", invalidFields(this.state.fields))

      return (
        <React.Fragment>

          <Form
            getRef={(ref) => this.form = ref}
            onSubmit={this.submitForm.bind(this)}
            onFocus={this.handleFieldFocus.bind(this)}
            autoFocus={!this.props.disabledAutoFocus}>

            <Component
              formIsWorking={formIsWorking}
              formIsReady={formIsReady}
              defaultValue={this.props.defaultValue || {}}
              handleFieldChanges={this.handleFieldChanges.bind(this)}
              updateFieldValues={this.updateFieldValues.bind(this)}
              goto={this.goto.bind(this)}
              submitForm={this.submitForm.bind(this)}
              createRequest={this.createRequest.bind(this)}
              {...this.props}
              {...this.state}
            />

          </Form>

        </React.Fragment>
      );
    }

    updateFieldValues(newFields, cb, cbParams) {
      let { fields } = this.state
      each(newFields, (it) => {
        fields[it.fieldName] = it.field;
      })
      this.setState({
        fields: fields,
        hideAlert: true,
        formFocus: true
      }, typeof cb === 'function' ? cb(cbParams) : null)
    }

    handleFieldChanges(name, value, field) {
      let fields = this.state.fields;
      fields[name] = field;
      let fieldError = this.state.fieldErrors;
      delete fieldError[name];
      this.setState({
        fields: fields,
        hideAlert: true,
        formFocus: true,
        fieldErrors: fieldError
      });
      if (options.submitOnChange) this.submitForm();
    }

    handleFieldFocus({ target }) {
      if (!this.state.formFocus) {
        this.setState({
          formFocus: true,
          formElement: target
        });
      }
    }

    goto(path) {
      this.props.history.push(path);
    }

    createRequest(request, resetAfterSuccess) {
      this.request = request;
      this.resetAfterSuccess = resetAfterSuccess;
    }

    submitForm() {
      if (this.state.formFocus) {
        this.setState({ formIsWorking: true, formFocus: false });
        if (this.state.formElement) this.state.formElement.blur();
        if (typeof this.request === 'function') this.request(getValues(this.state.fields), this.state.fields);
        if (typeof this.props.onSubmit === 'function') this.props.onSubmit(getValues(this.state.fields), this.state.fields);
      }
    }

    resetForm() {
      this.form ? this.form.reset() : null;
      this.setState({
        fields: {},
        fieldErrors: {},
        formIsWorking: false,
        formFocus: false
      });
      setFormInitialFocus(this.form);
    }

  };

  FormWrapper.displayName = `FormWrapper(${getDisplayName(Component)})`;

  function mapStateToProps(store) {
    return {
      isSuccess: store.features.isSuccess,
      actionMessage: store.features.actionMessage,
      actionKey: store.features.actionKey,
      isProcessing: store.features.isProcessing,
      errors: store.features.errors,
      data: store.features.data
    };
  }

  function mapDispatchToProps(dispatch) {
    return { ...bindActionCreators(actions, dispatch) };
  }

  return connect(mapStateToProps, mapDispatchToProps)(ReactTimeout(FormWrapper));

};

function getDisplayName(Component) {
  return Component.displayName || Component.name || 'Component';
};

export const fieldProps = (props, fieldName) => (
  {
    name: fieldName,
    valueDefault: props.defaultValue ? deepKey(props.defaultValue, fieldName) : '',
    value: props.fields ? (deepKey(props.fields, `${fieldName}.value`) || '') : '',
    checkedDefault: props.defaultValue ? (deepKey(props.defaultValue, fieldName) || false) : false,
    checked: props.fields ? (deepKey(props.fields, `${fieldName}.value`) || false) : false,
    isDisabled: props.formIsWorking || props.isDisabled,
    forceInvalid: props.fieldErrors ? deepKey(props.fieldErrors, fieldName) : null,
    errorMessage: props.fieldErrors ? deepKey(props.fieldErrors, fieldName) : null,
    onChange: props.handleFieldChanges && props.handleFieldChanges.bind(null, fieldName)
  }
);

export default formCreator;
