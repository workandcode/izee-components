import React from 'react';
import moment from 'moment';
import Grid from 'components/lib/Grid';
import SubmitButton from 'components/SubmitButton';
import SectionWrapper from 'components/SectionWrapper';
import Text from 'components/lib/Text';
import Icon from 'components/lib/Icon';
import formCreator from '../components/formCreator';
import { FormFieldSwitch, FormFieldSelect } from 'components/lib/FormFields';
import Company from '../components/fieldsGroup/Company';
import FullAddress from '../components/fieldsGroup/FullAddress';
import PersonalDataRenter from '../components/fieldsGroup/PersonalDataRenter';
import PersonalDataSpouse from 'features/components/fieldsGroup/PersonalDataSpouse'
import RenterRelation from 'features/components/fieldsGroup/RenterRelation';
import { goodObject, rawNumber } from 'utils/utils';
import { LEGAL_PERSON,  NATURAL_PERSON } from '../constants/PersonTypes';

export class UpdateFullRenter extends React.Component {

  componentDidMount() {
    this.props.createRequest(this.createRequest);
    // console.log(this.props);
  }

  componentDidUpdate() {}

  render() {

    const { 
      defaultValue,
      fields, 
      formIsReady, 
      formIsWorking, 
      hideSubmitButton, 
      showRelation,
    } = this.props;
    const user = defaultValue;
    const guarantees = (user.guarantees || []).filter((item) => item.is_active && item);

    return (
      <React.Fragment>
        
        {user.is_verified && (
          <Grid>
            <Grid.Col>
              <div>
                <Text isDefault>
                  {user.is_verified && <Icon name="fas fa-shield-check" style={{ marginLeft: -4 }} isSuccess />}
                  Garantias pré-aprovadas pela <strong>Izee</strong>: {guarantees.map((guarantee) => guarantee.name).join(', ')} {guarantees.length === 0 && 'Todas'}
                </Text>
                <hr />
              </div>
            </Grid.Col>
          </Grid>
        )}

        <SectionWrapper header="Dados do(a) locatário(a)">
          <PersonalDataRenter {...this.props} />
        </SectionWrapper>

        <SectionWrapper header="Dados do(a) cônjuge">
          <PersonalDataSpouse {...this.props} />
        </SectionWrapper>
        
        <SectionWrapper header="Endereço do(a) locatário(a)">
          <FullAddress {...this.props} noValidate />
        </SectionWrapper>
        
        <SectionWrapper header="Dados da empresa">
          <Company {...this.props} noValidate />
        </SectionWrapper>
        
        <SectionWrapper>
        {showRelation ? (
          <Grid>
            <Grid.Col>
              <RenterRelation
                onSelect={this.selectedRentersHandler.bind(this)}
                {...this.props}
              />
            </Grid.Col>
          </Grid>
        ) : null}
        </SectionWrapper>
        
        <SectionWrapper>
        {!hideSubmitButton && (
          <SubmitButton
            label="Salvar"
            isLoading={formIsWorking}
            isDisabled={!formIsReady || formIsWorking}
          />
        )}
        </SectionWrapper>

      </React.Fragment>
    );
  }

  fieldsToClear() {
    const { fields } = this.props;
    return [
      { fieldName: 'name', field: { ...fields.name } },
      { fieldName: 'phone', field: { ...fields.phone, value: "" } },
      { fieldName: 'identity_type', field: { ...fields.identity_type, value: "" } },
      { fieldName: 'identity', field: { ...fields.identity, value: "" } },
      { fieldName: 'identity_expedition_date', field: { ...fields.identity_expedition_date, value: "" } },
      { fieldName: 'identity_issuing_body', field: { ...fields.identity_issuing_body, value: "" } },
      { fieldName: 'birth_date', field: { ...fields.birth_date, value: "" } },
      { fieldName: 'occupation', field: { ...fields.occupation, value: "" } },
      { fieldName: 'nationality', field: { ...fields.nationality, value: "" } },
      { fieldName: 'marital_status', field: { ...fields.marital_status, value: "" } },
      { fieldName: 'prefix', field: { ...fields.prefix, value: "" } },
      { fieldName: 'trading_name', field: { ...fields.trading_name, value: "" } },
      { fieldName: 'municipal_enrollment', field: { ...fields.municipal_enrollment, value: "" } },
      { fieldName: 'state_enrollment', field: { ...fields.state_enrollment, value: "" } },
      { fieldName: 'income', field: { ...fields.income, value: "" } },
      { fieldName: 'spouse_phone', field: { ...fields.spouse_phone, value: "" } },
      { fieldName: 'spouse_birth_date', field: { ...fields.spouse_birth_date, value: "" } },
      { fieldName: 'spouse_occupation', field: { ...fields.spouse_occupation, value: "" } },
      { fieldName: 'spouse_nationality', field: { ...fields.spouse_nationality, value: "" } },
      { fieldName: 'spouse_identity_type', field: { ...fields.spouse_identity_type, value: "" } },
      { fieldName: 'spouse_identity', field: { ...fields.spouse_identity, value: "" } },
      { fieldName: 'spouse_identity_issuing_body', field: { ...fields.spouse_identity_issuing_body, value: "" } },
      { fieldName: 'spouse_cpf', field: { ...fields.spouse_cpf, value: "" } }
    ]
  }

  selectedRentersHandler(renters) {
    this.setState({ renters });
  }

  createRequest(fields) {
    let goodFields = goodObject(fields, {
      'birth_date': {
        path: 'birth_date',
        format: (value) => moment(value, 'DD/MM/YYYY').format('YYYY-MM-DD')
      },
      'identity_expedition_date': {
        path: 'identity_expedition_date',
        format: (value) => moment(value, 'DD/MM/YYYY').format('YYYY-MM-DD')
      },
      'income': {
        path: 'income',
        format: (value) => rawNumber(value)
      },
      'estimated_rent_value': {
        path: 'estimated_rent_value',
        format: (value) => rawNumber(value)
      },
      'marital_status_id': {
        path: 'marital_status',
        format: (value) => parseInt(value)
      },
      'spouse_birth_date': {
        path: 'spouse_birth_date',
        format: (value) => moment(value, 'DD/MM/YYYY').format('YYYY-MM-DD')
      },
    });
    this.props.submit(`{company}/renter/${this.props.renterId}`, goodFields, 'put');
  }

};

export default formCreator(UpdateFullRenter);
