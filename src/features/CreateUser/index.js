import React from 'react';
import PropTypes from 'prop-types';

// Components
import SubmitButton from 'components/SubmitButton';

import formCreator from '../components/formCreator';
import UserInvite from '../components/fieldsGroup/UserInvite';
import ProfileSelection from '../components/fieldsGroup/ProfileSelection';

export class CreateUser extends React.Component {

  componentDidMount() {
    this.props.createRequest(this.createRequest, true);
  }

  render() {

    const { formIsWorking, formIsReady } = this.props;

    return (
      <React.Fragment>

        <UserInvite {...this.props} />

        <ProfileSelection {...this.props} />

        <SubmitButton
          label="Salvar e continuar"
          isLoading={formIsWorking}
          isDisabled={!formIsReady || formIsWorking}
        />

      </React.Fragment>
    );
  }

  createRequest(fields) {
    fields['return_to'] = location.origin + '/auth/register';
    this.props.submit('{company}/grant-profile', fields);
  }

};

CreateUser.defaultProps = {
  fields: {},
  activeCompany: {}
};

CreateUser.propTypes = {
  activeCompany: PropTypes.object.isRequired
};

export default formCreator(CreateUser);
