import React from 'react';
import PropTypes from 'prop-types';

// Components
import SubmitButton from 'components/SubmitButton';

import formCreator from '../components/formCreator';
import ContractEditor from '../components/fieldsGroup/ContractEditor';

export class UpdateContract extends React.Component {

  componentDidMount() {
    this.props.createRequest(this.createRequest);
  }

  render() {

    const { formIsWorking, formIsReady } = this.props;

    return (
      <React.Fragment>

        <ContractEditor {...this.props} />

        <SubmitButton
          label="Salvar"
          isLoading={formIsWorking}
          isDisabled={!formIsReady || formIsWorking}
        />

      </React.Fragment>
    );
  }

  createRequest(fields) {
    this.props.submit(`company/${
      this.props.activeCompany.id
    }/contract/${
      this.props.defaultValue.id
    }`, fields, 'put');
  }

};

UpdateContract.defaultProps = {
  fields: {},
  defaultValue: {},
  activeCompany: {}
};

UpdateContract.propTypes = {
  activeCompany: PropTypes.object.isRequired
};

export default formCreator(UpdateContract);
