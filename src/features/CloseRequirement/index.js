import React from 'react';
import PropTypes from 'prop-types';

// Components
import {
  FormFieldCheckbox
} from 'components/lib/FormFields';
import Grid from 'components/lib/Grid';
import formCreator from '../components/formCreator';
import Button from 'components/lib/Button';

export class CloseRequirement extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      requirementIsClosed: false
    };
  }

  componentDidMount() {
    this.props.createRequest(this.createRequest, true);
  }

  render() {

    const { formIsWorking, formIsReady, buttonLabel, description } = this.props;
    const submitIsDisabled = (!formIsReady || formIsWorking) || !this.state.requirementIsClosed;

    return (
      <Grid>
        <Grid.Col>
          <FormFieldCheckbox
            description={description || 'Declaro que esta etapa está concluída e desejo encerrá-la.'}
            onChange={this.closeRequirement.bind(this)}
          />
        </Grid.Col>
        <Grid.Col isRight>
          <Button
            hasThemeColor
            isLoading={formIsWorking}
            isDisabled={submitIsDisabled}
            submit>
            {buttonLabel || 'Concluir'}
          </Button>
        </Grid.Col>
      </Grid>
    );
  }

  closeRequirement(value, field) {
    this.setState({
      requirementIsClosed: value
    });
  }

  createRequest() {
    const { processId, requirement } = this.props;
    this.props.submit(`{company}/process/${processId}/${requirement}/close-requirement`);
  }

};

CloseRequirement.defaultProps = {};

CloseRequirement.propTypes = {};

export default formCreator(CloseRequirement);
