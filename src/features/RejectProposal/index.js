import React from 'react';
import PropTypes from 'prop-types';

// Components
import {
  FormFieldText,
  FormFieldTextArea
} from 'components/lib/FormFields';
import Grid from 'components/lib/Grid';
import Text from 'components/lib/Text';
import SubmitButton from 'components/SubmitButton';
import formCreator, { fieldProps } from '../components/formCreator';

export class RejectProposal extends React.Component {

  componentDidMount() {
    this.props.createRequest(this.createRequest);
  }

  render() {

    const { formIsWorking, formIsReady } = this.props;

    return (
      <React.Fragment>

        <Grid>
          <Grid.Col>
            <Text>Tem certeza que deseja recusar esta proposta?</Text>
          </Grid.Col>
        </Grid>
        <Grid>
          <Grid.Col>
            <FormFieldTextArea
              {...fieldProps(this.props, 'denial_comment')}
              label="Motivo da recusa"
            />
          </Grid.Col>
        </Grid>

        <SubmitButton
          label="Recusar"
          isDanger
          isLoading={formIsWorking}
          isDisabled={!formIsReady || formIsWorking}
        />

      </React.Fragment>
    );
  }

  createRequest(fields) {
    this.props.submit(this.props.path, fields, 'put');
  }

};

RejectProposal.defaultProps = {
  display: null,
  isSafe: true
};

RejectProposal.propTypes = {
  path: PropTypes.string.isRequired,
  display: PropTypes.string,
  isSafe: PropTypes.bool
};

export default formCreator(RejectProposal);
