import React from 'react';

// Components
import SubmitButton from 'components/SubmitButton';

import formCreator from '../components/formCreator';
import CompanySettings from '../components/fieldsGroup/CompanySettings';

export class UpdateCompanySettings extends React.Component {

  componentDidMount() {
    this.props.createRequest(this.createRequest);
  }

  render() {

    return (
      <React.Fragment>

        <CompanySettings {...this.props} />

      </React.Fragment>
    );
  }

  createRequest(fields) {
    let { defaultValue } = this.props;
    defaultValue = defaultValue || {};
    this.props.submit(`company/${defaultValue.id}/settings`, fields, 'put');
  }

};

export default formCreator(UpdateCompanySettings, { submitOnChange: true });
