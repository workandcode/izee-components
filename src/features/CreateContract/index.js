import React from 'react';
import PropTypes from 'prop-types';

// Components
import SubmitButton from 'components/SubmitButton';

import formCreator from '../components/formCreator';
import ContractEditor from '../components/fieldsGroup/ContractEditor';

export class CreateContract extends React.Component {

  componentDidMount() {
    this.props.createRequest(this.createRequest);
  }

  render() {

    const { formIsWorking, formIsReady } = this.props;

    return (
      <React.Fragment>

        <ContractEditor {...this.props} />

        <SubmitButton
          label="Salvar"
          isLoading={formIsWorking}
          isDisabled={!formIsReady || formIsWorking}
        />

      </React.Fragment>
    );
  }

  createRequest(fields) {
    this.props.submit('{company}/contract', fields);
  }

};

CreateContract.defaultProps = {
  fields: {},
  activeCompany: {}
};

CreateContract.propTypes = {
  activeCompany: PropTypes.object.isRequired
};

export default formCreator(CreateContract);
