import React from 'react';
import PropTypes from 'prop-types';

// Components
import {
  FormFieldText,
  FormFieldFile
} from 'components/lib/FormFields';
import Grid from 'components/lib/Grid';
import Icon from 'components/lib/Icon';
import SubmitButton from 'components/SubmitButton';
import formCreator, { fieldProps } from '../components/formCreator';
import Button from 'components/lib/Button';
import UserSelection from 'features/components/fieldsGroup/UserSelection';

export class SendFile extends React.Component {

  componentDidMount() {
    this.props.createRequest(this.createRequest, true);
  }

  render() {

    const { formIsWorking, formIsReady } = this.props;

    return (
      <React.Fragment>

        <Grid>
          <Grid.Col>
            <FormFieldFile
              {...fieldProps(this.props, 'file')}
              label="Importar CSV"
              accept=".csv"
            />
          </Grid.Col>
          <Grid.Col isNarrow isFlexBottom>
            <Button
              hasThemeColor
              isLoading={formIsWorking}
              isDisabled={!formIsReady || formIsWorking}
              submit>
              Importar CSV
            </Button>
          </Grid.Col>
        </Grid>

      </React.Fragment>
    );
  }

  createRequest(fields) {
    fields.file[0].preview().then((content) => {
      this.props.submit('{company}/import-csv-data', {
        file: content
      });
    });
  }

};

SendFile.defaultProps = {};

SendFile.propTypes = {};

export default formCreator(SendFile);
