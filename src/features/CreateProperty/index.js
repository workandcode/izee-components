import React from 'react';

// Components
import SubmitButton from 'components/SubmitButton';
import Grid from 'components/lib/Grid';

import formCreator from '../components/formCreator';

import {
  FormFieldText,
  FormFieldCEP
} from 'components/lib/FormFields';
import { fieldProps } from '../components/formCreator';
import FullAddress from '../components/fieldsGroup/FullAddress';
import PropertyOwnerSelection from '../components/fieldsGroup/PropertyOwnerSelection';

export class CreateProperty extends React.Component {

  componentDidMount() {
    this.props.createRequest(this.createRequest);
  }

  render() {

    const { formIsWorking, formIsReady } = this.props;

    return (
      <React.Fragment>

        <Grid>
          <Grid.Col isNarrow>
            <FormFieldText
              {...fieldProps(this.props, 'code')}
              label="Código"
              noValidate
              style={{ width: 100 }}
            />
          </Grid.Col>
          <Grid.Col>
            <FormFieldText
              {...fieldProps(this.props, 'name')}
              label="Apelido"
              noValidate
            />
          </Grid.Col>
        </Grid>

        <FullAddress {...this.props} />

        {!this.props.ownerId && <PropertyOwnerSelection {...this.props} />}

        <SubmitButton
          label="Salvar"
          isLoading={formIsWorking}
          isDisabled={!formIsReady || formIsWorking}
        />

      </React.Fragment>
    );
  }

  createRequest(fields) {
    const ids = [];
    if (this.props.ownerId) {
      ids.push(this.props.ownerId);
      fields['property_owner_ids'] = ids;
    }
    this.props.submit('{company}/property', fields);
  }

};

export default formCreator(CreateProperty);
