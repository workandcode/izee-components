import React from 'react';

// Components
import SubmitButton from 'components/SubmitButton';
import Grid from 'components/lib/Grid';

import formCreator from '../components/formCreator';

import {
  FormFieldText
} from 'components/lib/FormFields';
import { fieldProps } from '../components/formCreator';

export class CreatePropertyOwner extends React.Component {

  componentDidMount() {
    this.props.createRequest(this.createRequest, true);
  }

  render() {

    const { formIsWorking, formIsReady } = this.props;

    return (
      <React.Fragment>

        <Grid>
          <Grid.Col>
            <FormFieldText
              {...fieldProps(this.props, 'name')}
              label="Nome"
            />
          </Grid.Col>
          <Grid.Col>
            <FormFieldText
              {...fieldProps(this.props, 'email')}
              label="Email"
            />
          </Grid.Col>
        </Grid>

        <SubmitButton
          label="Salvar e continuar"
          isLoading={formIsWorking}
          isDisabled={!formIsReady || formIsWorking}
        />

      </React.Fragment>
    );
  }

  createRequest(fields) {
    this.props.submit('{company}/property-owner', fields);
  }

};

export default formCreator(CreatePropertyOwner);
