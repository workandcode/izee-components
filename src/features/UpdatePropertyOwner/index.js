import React from 'react';
import moment from 'moment';
import SubmitButton from 'components/SubmitButton';

import SectionWrapper from 'components/SectionWrapper';
import Grid from 'components/lib/Grid';
import { FormFieldSelect, FormFieldSwitch } from 'components/lib/FormFields';
import formCreator from '../components/formCreator';
import PersonalData from '../components/fieldsGroup/PersonalData';
import PersonalDataRenter from '../components/fieldsGroup/PersonalDataRenter';
import PersonalDataSpouse from 'features/components/fieldsGroup/PersonalDataSpouse'
import Company from '../components/fieldsGroup/Company';
import FullAddress from '../components/fieldsGroup/FullAddress';
import { goodObject } from 'utils/utils';
import { LEGAL_PERSON, NATURAL_PERSON } from '../constants/PersonTypes';

export class UpdatePropertyOwner extends React.Component {

  constructor(props) {
    super(props);
    // const { proposal } = this.props;
    // const signMethod = deepKey(proposal, 'contract.sign_method');
    this.chooseSpouse = this.chooseSpouse.bind(this);
    this.state = {
      spouseStatus: false
    };
  }
  componentDidMount() {
    this.props.createRequest(this.createRequest);
  }
  componentDidUpdate() { }

  render() {
    const { formIsWorking, formIsReady } = this.props;
    const { spouseStatus } = this.state;
    // console.log(spouseStatus, "rerender");
    return (
      <React.Fragment>
        <SectionWrapper header="Dados do(a) proprietário(a)">
          <PersonalDataRenter {...this.props} />
        </SectionWrapper>
        <SectionWrapper header="Dados do(a) cônjuge">
          <PersonalDataSpouse {...this.props} />
        </SectionWrapper>

        <SectionWrapper header="Endereço do(a) proprietário(a)">
          <FullAddress {...this.props} noValidate />
        </SectionWrapper>

        <SectionWrapper header="Dados da empresa proprietário(a)">
          <Company {...this.props} noValidate />
        </SectionWrapper>

        <SectionWrapper header="">
          <SubmitButton
            label="Salvar"
            isLoading={formIsWorking}
            isDisabled={!formIsReady || formIsWorking}
          />
        </SectionWrapper>
      </React.Fragment>
    );
  }
  chooseSpouse() {
    // const { spouseStatus } = this.state;
    // this.setState({ spouseStatus: !spouseStatus });
  }
  fieldsToClear() {
    const { fields } = this.props;
    return [
      { fieldName: 'phone', field: { ...fields.phone, value: "" } },
      { fieldName: 'identity_type', field: { ...fields.identity_type, value: "" } },
      { fieldName: 'identity', field: { ...fields.identity, value: "" } },
      { fieldName: 'identity_expedition_date', field: { ...fields.identity_expedition_date, value: "" } },
      { fieldName: 'identity_issuing_body', field: { ...fields.identity_issuing_body, value: "" } },
      { fieldName: 'birth_date', field: { ...fields.birth_date, value: "" } },
      { fieldName: 'occupation', field: { ...fields.occupation, value: "" } },
      { fieldName: 'nationality', field: { ...fields.nationality, value: "" } },
      { fieldName: 'marital_status', field: { ...fields.marital_status, value: "" } },
      { fieldName: 'prefix', field: { ...fields.prefix, value: "" } },
      { fieldName: 'trading_name', field: { ...fields.trading_name, value: "" } },
      { fieldName: 'municipal_enrollment', field: { ...fields.municipal_enrollment, value: "" } },
      { fieldName: 'state_enrollment', field: { ...fields.state_enrollment, value: "" } },
      { fieldName: 'spouse_phone', field: { ...fields.spouse_phone, value: "" } },
      { fieldName: 'spouse_birth_date', field: { ...fields.spouse_birth_date, value: "" } },
      { fieldName: 'spouse_occupation', field: { ...fields.spouse_occupation, value: "" } },
      { fieldName: 'spouse_nationality', field: { ...fields.spouse_nationality, value: "" } },
      { fieldName: 'spouse_identity_type', field: { ...fields.spouse_identity_type, value: "" } },
      { fieldName: 'spouse_identity', field: { ...fields.spouse_identity, value: "" } },
      { fieldName: 'spouse_identity_issuing_body', field: { ...fields.spouse_identity_issuing_body, value: "" } },
      { fieldName: 'spouse_cpf', field: { ...fields.spouse_cpf, value: "" } }
    ]
  }

  createRequest(fields) {
    let goodFields = goodObject(fields, {
      'birth_date': {
        path: 'birth_date',
        format: (value) => moment(value, 'DD/MM/YYYY').format('YYYY-MM-DD')
      },
      'identity_expedition_date': {
        path: 'identity_expedition_date',
        format: (value) => moment(value, 'DD/MM/YYYY').format('YYYY-MM-DD')
      },
      'marital_status_id': {
        path: 'marital_status',
        format: (value) => parseInt(value)
      },
      'spouse_birth_date': {
        path: 'spouse_birth_date',
        format: (value) => moment(value, 'DD/MM/YYYY').format('YYYY-MM-DD')
      },
    });
    this.props.submit(`{company}/property-owner/${this.props.propertyOwnerId}`, goodFields, 'put');
  }

};

export default formCreator(UpdatePropertyOwner);
